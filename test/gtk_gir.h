
#pragma once

typedef char gchar;
typedef short gshort;
typedef long glong;
typedef int gint;
typedef gint gboolean;

typedef unsigned char guchar;
typedef unsigned short gushort;
typedef unsigned long gulong;
typedef unsigned int guint;

typedef float gfloat;
typedef double gdouble;

#include <atk/atk.h>

#include <gdk/gdk.h>

typedef GdkRectangle GtkAllocation;

typedef char *GtkStock;

typedef struct _GtkAboutDialogClass GtkAboutDialogClass;

struct _GtkAboutDialogClass {
  GtkDialogClass parent_class;
  gboolean (*activate_link)(GtkAboutDialog *dialog, const gchar *uri);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkAboutDialogPrivate GtkAboutDialogPrivate;

enum GtkAccelFlags {
  GTK_ACCEL_VISIBLE = 1,
  GTK_ACCEL_LOCKED = 2,
  GTK_ACCEL_MASK = 7
};
gboolean (*AccelGroupActivate)(GtkAccelGroup *accel_group,
                               GObject *acceleratable, guint keyval,
                               GdkModifierType modifier);

typedef struct _GtkAccelGroupClass GtkAccelGroupClass;

struct _GtkAccelGroupClass {
  GObjectClass parent_class;
  void (*accel_changed)(GtkAccelGroup *accel_group, guint keyval,
                        GdkModifierType modifier, GClosure *accel_closure);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkAccelGroupEntry GtkAccelGroupEntry;

struct _GtkAccelGroupEntry {
  GtkAccelKey key;
  GClosure *closure;
  GQuark accel_path_quark;
};
gboolean (*AccelGroupFindFunc)(GtkAccelKey *key, GClosure *closure,
                               gpointer data);

typedef struct _GtkAccelGroupPrivate GtkAccelGroupPrivate;

typedef struct _GtkAccelKey GtkAccelKey;

struct _GtkAccelKey {
  guint accel_key;
  GdkModifierType accel_mods;
  guint accel_flags;
};

typedef struct _GtkAccelLabelClass GtkAccelLabelClass;

struct _GtkAccelLabelClass {
  GtkLabelClass parent_class;
  gchar *signal_quote1;
  gchar *signal_quote2;
  gchar *mod_name_shift;
  gchar *mod_name_control;
  gchar *mod_name_alt;
  gchar *mod_separator;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkAccelLabelPrivate GtkAccelLabelPrivate;

typedef struct _GtkAccelMapClass GtkAccelMapClass;
void (*AccelMapForeach)(gpointer data, const gchar *accel_path, guint accel_key,
                        GdkModifierType accel_mods, gboolean changed);

typedef struct _GtkAccessibleClass GtkAccessibleClass;

struct _GtkAccessibleClass {
  AtkObjectClass parent_class;
  void (*connect_widget_destroyed)(GtkAccessible *accessible);
  void (*widget_set)(GtkAccessible *accessible);
  void (*widget_unset)(GtkAccessible *accessible);
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkAccessiblePrivate GtkAccessiblePrivate;

typedef struct _GtkActionBarClass GtkActionBarClass;

struct _GtkActionBarClass {
  GtkBinClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkActionBarPrivate GtkActionBarPrivate;

typedef struct _GtkActionClass GtkActionClass;

struct _GtkActionClass {
  GObjectClass parent_class;
  void (*activate)(GtkAction *action);
  GType menu_item_type;
  GType toolbar_item_type;
  GtkWidget *(*create_menu_item)(GtkAction *action);
  GtkWidget *(*create_tool_item)(GtkAction *action);
  void (*connect_proxy)(GtkAction *action, GtkWidget *proxy);
  void (*disconnect_proxy)(GtkAction *action, GtkWidget *proxy);
  GtkWidget *(*create_menu)(GtkAction *action);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkActionEntry GtkActionEntry;

struct _GtkActionEntry {
  const gchar *name;
  const gchar *stock_id;
  const gchar *label;
  const gchar *accelerator;
  const gchar *tooltip;
  GCallback callback;
};

typedef struct _GtkActionGroupClass GtkActionGroupClass;

struct _GtkActionGroupClass {
  GObjectClass parent_class;
  GtkAction *(*get_action)(GtkActionGroup *action_group,
                           const gchar *action_name);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkActionGroupPrivate GtkActionGroupPrivate;

typedef struct _GtkActionPrivate GtkActionPrivate;

typedef struct _GtkActionableInterface GtkActionableInterface;

struct _GtkActionableInterface {
  GTypeInterface g_iface;
  const gchar *(*get_action_name)(GtkActionable *actionable);
  void (*set_action_name)(GtkActionable *actionable, const gchar *action_name);
  GVariant *(*get_action_target_value)(GtkActionable *actionable);
  void (*set_action_target_value)(GtkActionable *actionable,
                                  GVariant *target_value);
};

typedef struct _GtkActivatableIface GtkActivatableIface;

struct _GtkActivatableIface {
  GTypeInterface g_iface;
  void (*update)(GtkActivatable *activatable, GtkAction *action,
                 const gchar *property_name);
  void (*sync_action_properties)(GtkActivatable *activatable,
                                 GtkAction *action);
};

typedef struct _GtkAdjustmentClass GtkAdjustmentClass;

struct _GtkAdjustmentClass {
  GInitiallyUnownedClass parent_class;
  void (*changed)(GtkAdjustment *adjustment);
  void (*value_changed)(GtkAdjustment *adjustment);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkAdjustmentPrivate GtkAdjustmentPrivate;

enum GtkAlign {
  GTK_ALIGN_FILL = 0,
  GTK_ALIGN_START = 1,
  GTK_ALIGN_END = 2,
  GTK_ALIGN_CENTER = 3,
  GTK_ALIGN_BASELINE = 4
};

typedef struct _GtkAlignmentClass GtkAlignmentClass;

struct _GtkAlignmentClass {
  GtkBinClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkAlignmentPrivate GtkAlignmentPrivate;

typedef struct _GtkAppChooserButtonClass GtkAppChooserButtonClass;

struct _GtkAppChooserButtonClass {
  GtkComboBoxClass parent_class;
  void (*custom_item_activated)(GtkAppChooserButton *self,
                                const gchar *item_name);
  gpointer padding[16];
};

typedef struct _GtkAppChooserButtonPrivate GtkAppChooserButtonPrivate;

typedef struct _GtkAppChooserDialogClass GtkAppChooserDialogClass;

struct _GtkAppChooserDialogClass {
  GtkDialogClass parent_class;
  gpointer padding[16];
};

typedef struct _GtkAppChooserDialogPrivate GtkAppChooserDialogPrivate;

typedef struct _GtkAppChooserWidgetClass GtkAppChooserWidgetClass;

struct _GtkAppChooserWidgetClass {
  GtkBoxClass parent_class;
  void (*application_selected)(GtkAppChooserWidget *self, GAppInfo *app_info);
  void (*application_activated)(GtkAppChooserWidget *self, GAppInfo *app_info);
  void (*populate_popup)(GtkAppChooserWidget *self, GtkMenu *menu,
                         GAppInfo *app_info);
  gpointer padding[16];
};

typedef struct _GtkAppChooserWidgetPrivate GtkAppChooserWidgetPrivate;

typedef struct _GtkApplicationClass GtkApplicationClass;

struct _GtkApplicationClass {
  GApplicationClass parent_class;
  void (*window_added)(GtkApplication *application, GtkWindow *window);
  void (*window_removed)(GtkApplication *application, GtkWindow *window);
  gpointer padding[12];
};

enum GtkApplicationInhibitFlags {
  GTK_APPLICATION_INHIBIT_LOGOUT = 1,
  GTK_APPLICATION_INHIBIT_SWITCH = 2,
  GTK_APPLICATION_INHIBIT_SUSPEND = 4,
  GTK_APPLICATION_INHIBIT_IDLE = 8
};

typedef struct _GtkApplicationPrivate GtkApplicationPrivate;

typedef struct _GtkApplicationWindowClass GtkApplicationWindowClass;

struct _GtkApplicationWindowClass {
  GtkWindowClass parent_class;
  gpointer padding[14];
};

typedef struct _GtkApplicationWindowPrivate GtkApplicationWindowPrivate;

typedef struct _GtkArrowAccessibleClass GtkArrowAccessibleClass;

struct _GtkArrowAccessibleClass {
  GtkWidgetAccessibleClass parent_class;
};

typedef struct _GtkArrowAccessiblePrivate GtkArrowAccessiblePrivate;

typedef struct _GtkArrowClass GtkArrowClass;

struct _GtkArrowClass {
  GtkMiscClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkArrowPlacement {
  GTK_ARROWS_BOTH = 0,
  GTK_ARROWS_START = 1,
  GTK_ARROWS_END = 2
};

typedef struct _GtkArrowPrivate GtkArrowPrivate;

enum GtkArrowType {
  GTK_ARROW_UP = 0,
  GTK_ARROW_DOWN = 1,
  GTK_ARROW_LEFT = 2,
  GTK_ARROW_RIGHT = 3,
  GTK_ARROW_NONE = 4
};

typedef struct _GtkAspectFrameClass GtkAspectFrameClass;

struct _GtkAspectFrameClass {
  GtkFrameClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkAspectFramePrivate GtkAspectFramePrivate;

typedef struct _GtkAssistantClass GtkAssistantClass;

struct _GtkAssistantClass {
  GtkWindowClass parent_class;
  void (*prepare)(GtkAssistant *assistant, GtkWidget *page);
  void (*apply)(GtkAssistant *assistant);
  void (*close)(GtkAssistant *assistant);
  void (*cancel)(GtkAssistant *assistant);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
};
gint (*AssistantPageFunc)(gint current_page, gpointer data);

enum GtkAssistantPageType {
  GTK_ASSISTANT_PAGE_CONTENT = 0,
  GTK_ASSISTANT_PAGE_INTRO = 1,
  GTK_ASSISTANT_PAGE_CONFIRM = 2,
  GTK_ASSISTANT_PAGE_SUMMARY = 3,
  GTK_ASSISTANT_PAGE_PROGRESS = 4,
  GTK_ASSISTANT_PAGE_CUSTOM = 5
};

typedef struct _GtkAssistantPrivate GtkAssistantPrivate;

enum GtkAttachOptions { GTK_EXPAND = 1, GTK_SHRINK = 2, GTK_FILL = 4 };

const gint GTK_BINARY_AGE = 2230;

enum GtkBaselinePosition {
  GTK_BASELINE_POSITION_TOP = 0,
  GTK_BASELINE_POSITION_CENTER = 1,
  GTK_BASELINE_POSITION_BOTTOM = 2
};

typedef struct _GtkBinClass GtkBinClass;

struct _GtkBinClass {
  GtkContainerClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkBinPrivate GtkBinPrivate;

typedef struct _GtkBindingArg GtkBindingArg;

struct _GtkBindingArg {
  GType arg_type;
};

typedef struct _GtkBindingEntry GtkBindingEntry;

struct _GtkBindingEntry {
  guint keyval;
  GdkModifierType modifiers;
  GtkBindingSet *binding_set;
  guint destroyed;
  guint in_emission;
  guint marks_unbound;
  GtkBindingEntry *set_next;
  GtkBindingEntry *hash_next;
  GtkBindingSignal *signals;
};

typedef struct _GtkBindingSet GtkBindingSet;

struct _GtkBindingSet {
  gchar *set_name;
  gint priority;
  GSList *widget_path_pspecs;
  GSList *widget_class_pspecs;
  GSList *class_branch_pspecs;
  GtkBindingEntry *entries;
  GtkBindingEntry *current;
  guint parsed;
};
gboolean gtk_binding_set_activate(GtkBindingSet *binding_set, guint keyval,
                                  GdkModifierType modifiers, GObject *object);
void gtk_binding_set_add_path(GtkBindingSet *binding_set, GtkPathType path_type,
                              const gchar *path_pattern,
                              GtkPathPriorityType priority);

typedef struct _GtkBindingSignal GtkBindingSignal;

struct _GtkBindingSignal {
  GtkBindingSignal *next;
  gchar *signal_name;
  guint n_args;
  GtkBindingArg args[];
};

typedef struct _GtkBooleanCellAccessibleClass GtkBooleanCellAccessibleClass;

struct _GtkBooleanCellAccessibleClass {
  GtkRendererCellAccessibleClass parent_class;
};

typedef struct _GtkBooleanCellAccessiblePrivate GtkBooleanCellAccessiblePrivate;

typedef struct _GtkBorder GtkBorder;

struct _GtkBorder {
  gint16 left;
  gint16 right;
  gint16 top;
  gint16 bottom;
};
GtkBorder *gtk_border_copy(const GtkBorder *border_);
void gtk_border_free(GtkBorder *border_);

enum GtkBorderStyle {
  GTK_BORDER_STYLE_NONE = 0,
  GTK_BORDER_STYLE_SOLID = 1,
  GTK_BORDER_STYLE_INSET = 2,
  GTK_BORDER_STYLE_OUTSET = 3,
  GTK_BORDER_STYLE_HIDDEN = 4,
  GTK_BORDER_STYLE_DOTTED = 5,
  GTK_BORDER_STYLE_DASHED = 6,
  GTK_BORDER_STYLE_DOUBLE = 7,
  GTK_BORDER_STYLE_GROOVE = 8,
  GTK_BORDER_STYLE_RIDGE = 9
};

typedef struct _GtkBoxClass GtkBoxClass;

struct _GtkBoxClass {
  GtkContainerClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkBoxPrivate GtkBoxPrivate;

typedef struct _GtkBuildableIface GtkBuildableIface;

struct _GtkBuildableIface {
  GTypeInterface g_iface;
  void (*set_name)(GtkBuildable *buildable, const gchar *name);
  const gchar *(*get_name)(GtkBuildable *buildable);
  void (*add_child)(GtkBuildable *buildable, GtkBuilder *builder,
                    GObject *child, const gchar *type);
  void (*set_buildable_property)(GtkBuildable *buildable, GtkBuilder *builder,
                                 const gchar *name, const GValue *value);
  GObject *(*construct_child)(GtkBuildable *buildable, GtkBuilder *builder,
                              const gchar *name);
  gboolean (*custom_tag_start)(GtkBuildable *buildable, GtkBuilder *builder,
                               GObject *child, const gchar *tagname,
                               GMarkupParser *parser, gpointer *data);
  void (*custom_tag_end)(GtkBuildable *buildable, GtkBuilder *builder,
                         GObject *child, const gchar *tagname, gpointer *data);
  void (*custom_finished)(GtkBuildable *buildable, GtkBuilder *builder,
                          GObject *child, const gchar *tagname, gpointer data);
  void (*parser_finished)(GtkBuildable *buildable, GtkBuilder *builder);
  GObject *(*get_internal_child)(GtkBuildable *buildable, GtkBuilder *builder,
                                 const gchar *childname);
};

typedef struct _GtkBuilderClass GtkBuilderClass;

struct _GtkBuilderClass {
  GObjectClass parent_class;
  GType (*get_type_from_name)(GtkBuilder *builder, const char *type_name);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};
void (*BuilderConnectFunc)(GtkBuilder *builder, GObject *object,
                           const gchar *signal_name, const gchar *handler_name,
                           GObject *connect_object, GConnectFlags flags,
                           gpointer user_data);

enum GtkBuilderError {
  GTK_BUILDER_ERROR_INVALID_TYPE_FUNCTION = 0,
  GTK_BUILDER_ERROR_UNHANDLED_TAG = 1,
  GTK_BUILDER_ERROR_MISSING_ATTRIBUTE = 2,
  GTK_BUILDER_ERROR_INVALID_ATTRIBUTE = 3,
  GTK_BUILDER_ERROR_INVALID_TAG = 4,
  GTK_BUILDER_ERROR_MISSING_PROPERTY_VALUE = 5,
  GTK_BUILDER_ERROR_INVALID_VALUE = 6,
  GTK_BUILDER_ERROR_VERSION_MISMATCH = 7,
  GTK_BUILDER_ERROR_DUPLICATE_ID = 8,
  GTK_BUILDER_ERROR_OBJECT_TYPE_REFUSED = 9,
  GTK_BUILDER_ERROR_TEMPLATE_MISMATCH = 10,
  GTK_BUILDER_ERROR_INVALID_PROPERTY = 11,
  GTK_BUILDER_ERROR_INVALID_SIGNAL = 12,
  GTK_BUILDER_ERROR_INVALID_ID = 13
};

typedef struct _GtkBuilderPrivate GtkBuilderPrivate;

typedef struct _GtkButtonAccessibleClass GtkButtonAccessibleClass;

struct _GtkButtonAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkButtonAccessiblePrivate GtkButtonAccessiblePrivate;

typedef struct _GtkButtonBoxClass GtkButtonBoxClass;

struct _GtkButtonBoxClass {
  GtkBoxClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkButtonBoxPrivate GtkButtonBoxPrivate;

enum GtkButtonBoxStyle {
  GTK_BUTTONBOX_SPREAD = 1,
  GTK_BUTTONBOX_EDGE = 2,
  GTK_BUTTONBOX_START = 3,
  GTK_BUTTONBOX_END = 4,
  GTK_BUTTONBOX_CENTER = 5,
  GTK_BUTTONBOX_EXPAND = 6
};

typedef struct _GtkButtonClass GtkButtonClass;

struct _GtkButtonClass {
  GtkBinClass parent_class;
  void (*pressed)(GtkButton *button);
  void (*released)(GtkButton *button);
  void (*clicked)(GtkButton *button);
  void (*enter)(GtkButton *button);
  void (*leave)(GtkButton *button);
  void (*activate)(GtkButton *button);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkButtonPrivate GtkButtonPrivate;

enum GtkButtonRole {
  GTK_BUTTON_ROLE_NORMAL = 0,
  GTK_BUTTON_ROLE_CHECK = 1,
  GTK_BUTTON_ROLE_RADIO = 2
};

enum GtkButtonsType {
  GTK_BUTTONS_NONE = 0,
  GTK_BUTTONS_OK = 1,
  GTK_BUTTONS_CLOSE = 2,
  GTK_BUTTONS_CANCEL = 3,
  GTK_BUTTONS_YES_NO = 4,
  GTK_BUTTONS_OK_CANCEL = 5
};

typedef struct _GtkCalendarClass GtkCalendarClass;

struct _GtkCalendarClass {
  GtkWidgetClass parent_class;
  void (*month_changed)(GtkCalendar *calendar);
  void (*day_selected)(GtkCalendar *calendar);
  void (*day_selected_double_click)(GtkCalendar *calendar);
  void (*prev_month)(GtkCalendar *calendar);
  void (*next_month)(GtkCalendar *calendar);
  void (*prev_year)(GtkCalendar *calendar);
  void (*next_year)(GtkCalendar *calendar);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};
gchar *(*CalendarDetailFunc)(GtkCalendar *calendar, guint year, guint month,
                             guint day, gpointer user_data);

enum GtkCalendarDisplayOptions {
  GTK_CALENDAR_SHOW_HEADING = 1,
  GTK_CALENDAR_SHOW_DAY_NAMES = 2,
  GTK_CALENDAR_NO_MONTH_CHANGE = 4,
  GTK_CALENDAR_SHOW_WEEK_NUMBERS = 8,
  GTK_CALENDAR_SHOW_DETAILS = 32
};

typedef struct _GtkCalendarPrivate GtkCalendarPrivate;
void (*Callback)(GtkWidget *widget, gpointer data);

typedef struct _GtkCellAccessibleClass GtkCellAccessibleClass;

struct _GtkCellAccessibleClass {
  GtkAccessibleClass parent_class;
  void (*update_cache)(GtkCellAccessible *cell, gboolean emit_signal);
};

typedef struct _GtkCellAccessibleParentIface GtkCellAccessibleParentIface;

struct _GtkCellAccessibleParentIface {
  GTypeInterface parent;
  void (*get_cell_extents)(GtkCellAccessibleParent *parent,
                           GtkCellAccessible *cell, gint *x, gint *y,
                           gint *width, gint *height, AtkCoordType coord_type);
  void (*get_cell_area)(GtkCellAccessibleParent *parent,
                        GtkCellAccessible *cell, GdkRectangle *cell_rect);
  gboolean (*grab_focus)(GtkCellAccessibleParent *parent,
                         GtkCellAccessible *cell);
  int (*get_child_index)(GtkCellAccessibleParent *parent,
                         GtkCellAccessible *cell);
  GtkCellRendererState (*get_renderer_state)(GtkCellAccessibleParent *parent,
                                             GtkCellAccessible *cell);
  void (*expand_collapse)(GtkCellAccessibleParent *parent,
                          GtkCellAccessible *cell);
  void (*activate)(GtkCellAccessibleParent *parent, GtkCellAccessible *cell);
  void (*edit)(GtkCellAccessibleParent *parent, GtkCellAccessible *cell);
  void (*update_relationset)(GtkCellAccessibleParent *parent,
                             GtkCellAccessible *cell,
                             AtkRelationSet *relationset);
};

typedef struct _GtkCellAccessiblePrivate GtkCellAccessiblePrivate;
gboolean (*CellAllocCallback)(GtkCellRenderer *renderer,
                              const GdkRectangle *cell_area,
                              const GdkRectangle *cell_background,
                              gpointer data);

typedef struct _GtkCellAreaBoxClass GtkCellAreaBoxClass;

struct _GtkCellAreaBoxClass {
  GtkCellAreaClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCellAreaBoxPrivate GtkCellAreaBoxPrivate;

typedef struct _GtkCellAreaClass GtkCellAreaClass;

struct _GtkCellAreaClass {
  GInitiallyUnownedClass parent_class;
  void (*add)(GtkCellArea *area, GtkCellRenderer *renderer);
  void (*remove)(GtkCellArea *area, GtkCellRenderer *renderer);
  void (*foreach)(GtkCellArea *area, GtkCellCallback callback,
                  gpointer callback_data);
  void (*foreach_alloc)(GtkCellArea *area, GtkCellAreaContext *context,
                        GtkWidget *widget, const GdkRectangle *cell_area,
                        const GdkRectangle *background_area,
                        GtkCellAllocCallback callback, gpointer callback_data);
  gint (*event)(GtkCellArea *area, GtkCellAreaContext *context,
                GtkWidget *widget, GdkEvent *event,
                const GdkRectangle *cell_area, GtkCellRendererState flags);
  void (*render)(GtkCellArea *area, GtkCellAreaContext *context,
                 GtkWidget *widget, cairo_t *cr,
                 const GdkRectangle *background_area,
                 const GdkRectangle *cell_area, GtkCellRendererState flags,
                 gboolean paint_focus);
  void (*apply_attributes)(GtkCellArea *area, GtkTreeModel *tree_model,
                           GtkTreeIter *iter, gboolean is_expander,
                           gboolean is_expanded);
  GtkCellAreaContext *(*create_context)(GtkCellArea *area);
  GtkCellAreaContext *(*copy_context)(GtkCellArea *area,
                                      GtkCellAreaContext *context);
  GtkSizeRequestMode (*get_request_mode)(GtkCellArea *area);
  void (*get_preferred_width)(GtkCellArea *area, GtkCellAreaContext *context,
                              GtkWidget *widget, gint *minimum_width,
                              gint *natural_width);
  void (*get_preferred_height_for_width)(GtkCellArea *area,
                                         GtkCellAreaContext *context,
                                         GtkWidget *widget, gint width,
                                         gint *minimum_height,
                                         gint *natural_height);
  void (*get_preferred_height)(GtkCellArea *area, GtkCellAreaContext *context,
                               GtkWidget *widget, gint *minimum_height,
                               gint *natural_height);
  void (*get_preferred_width_for_height)(GtkCellArea *area,
                                         GtkCellAreaContext *context,
                                         GtkWidget *widget, gint height,
                                         gint *minimum_width,
                                         gint *natural_width);
  void (*set_cell_property)(GtkCellArea *area, GtkCellRenderer *renderer,
                            guint property_id, const GValue *value,
                            GParamSpec *pspec);
  void (*get_cell_property)(GtkCellArea *area, GtkCellRenderer *renderer,
                            guint property_id, GValue *value,
                            GParamSpec *pspec);
  gboolean (*focus)(GtkCellArea *area, GtkDirectionType direction);
  gboolean (*is_activatable)(GtkCellArea *area);
  gboolean (*activate)(GtkCellArea *area, GtkCellAreaContext *context,
                       GtkWidget *widget, const GdkRectangle *cell_area,
                       GtkCellRendererState flags, gboolean edit_only);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};
GParamSpec *gtk_cell_area_class_find_cell_property(GtkCellAreaClass *aclass,
                                                   const gchar *property_name);
void gtk_cell_area_class_install_cell_property(GtkCellAreaClass *aclass,
                                               guint property_id,
                                               GParamSpec *pspec);
gtk_cell_area_class_list_cell_properties(GtkCellAreaClass *aclass,
                                         guint *n_properties);

typedef struct _GtkCellAreaContextClass GtkCellAreaContextClass;

struct _GtkCellAreaContextClass {
  GObjectClass parent_class;
  void (*allocate)(GtkCellAreaContext *context, gint width, gint height);
  void (*reset)(GtkCellAreaContext *context);
  void (*get_preferred_height_for_width)(GtkCellAreaContext *context,
                                         gint width, gint *minimum_height,
                                         gint *natural_height);
  void (*get_preferred_width_for_height)(GtkCellAreaContext *context,
                                         gint height, gint *minimum_width,
                                         gint *natural_width);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
};

typedef struct _GtkCellAreaContextPrivate GtkCellAreaContextPrivate;

typedef struct _GtkCellAreaPrivate GtkCellAreaPrivate;
gboolean (*CellCallback)(GtkCellRenderer *renderer, gpointer data);

typedef struct _GtkCellEditableIface GtkCellEditableIface;

struct _GtkCellEditableIface {
  GTypeInterface g_iface;
  void (*editing_done)(GtkCellEditable *cell_editable);
  void (*remove_widget)(GtkCellEditable *cell_editable);
  void (*start_editing)(GtkCellEditable *cell_editable, GdkEvent *event);
};
void (*CellLayoutDataFunc)(GtkCellLayout *cell_layout, GtkCellRenderer *cell,
                           GtkTreeModel *tree_model, GtkTreeIter *iter,
                           gpointer data);

typedef struct _GtkCellLayoutIface GtkCellLayoutIface;

struct _GtkCellLayoutIface {
  GTypeInterface g_iface;
  void (*pack_start)(GtkCellLayout *cell_layout, GtkCellRenderer *cell,
                     gboolean expand);
  void (*pack_end)(GtkCellLayout *cell_layout, GtkCellRenderer *cell,
                   gboolean expand);
  void (*clear)(GtkCellLayout *cell_layout);
  void (*add_attribute)(GtkCellLayout *cell_layout, GtkCellRenderer *cell,
                        const gchar *attribute, gint column);
  void (*set_cell_data_func)(GtkCellLayout *cell_layout, GtkCellRenderer *cell,
                             GtkCellLayoutDataFunc func, gpointer func_data,
                             GDestroyNotify destroy);
  void (*clear_attributes)(GtkCellLayout *cell_layout, GtkCellRenderer *cell);
  void (*reorder)(GtkCellLayout *cell_layout, GtkCellRenderer *cell,
                  gint position);
  GList *(*get_cells)(GtkCellLayout *cell_layout);
  GtkCellArea *(*get_area)(GtkCellLayout *cell_layout);
};

typedef struct _GtkCellRendererAccelClass GtkCellRendererAccelClass;

struct _GtkCellRendererAccelClass {
  GtkCellRendererTextClass parent_class;
  void (*accel_edited)(GtkCellRendererAccel *accel, const gchar *path_string,
                       guint accel_key, GdkModifierType accel_mods,
                       guint hardware_keycode);
  void (*accel_cleared)(GtkCellRendererAccel *accel, const gchar *path_string);
  void (*_gtk_reserved0)();
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkCellRendererAccelMode {
  GTK_CELL_RENDERER_ACCEL_MODE_GTK = 0,
  GTK_CELL_RENDERER_ACCEL_MODE_OTHER = 1
};

typedef struct _GtkCellRendererAccelPrivate GtkCellRendererAccelPrivate;

typedef struct _GtkCellRendererClass GtkCellRendererClass;

struct _GtkCellRendererClass {
  GInitiallyUnownedClass parent_class;
  GtkSizeRequestMode (*get_request_mode)(GtkCellRenderer *cell);
  void (*get_preferred_width)(GtkCellRenderer *cell, GtkWidget *widget,
                              gint *minimum_size, gint *natural_size);
  void (*get_preferred_height_for_width)(GtkCellRenderer *cell,
                                         GtkWidget *widget, gint width,
                                         gint *minimum_height,
                                         gint *natural_height);
  void (*get_preferred_height)(GtkCellRenderer *cell, GtkWidget *widget,
                               gint *minimum_size, gint *natural_size);
  void (*get_preferred_width_for_height)(GtkCellRenderer *cell,
                                         GtkWidget *widget, gint height,
                                         gint *minimum_width,
                                         gint *natural_width);
  void (*get_aligned_area)(GtkCellRenderer *cell, GtkWidget *widget,
                           GtkCellRendererState flags,
                           const GdkRectangle *cell_area,
                           GdkRectangle *aligned_area);
  void (*get_size)(GtkCellRenderer *cell, GtkWidget *widget,
                   const GdkRectangle *cell_area, gint *x_offset,
                   gint *y_offset, gint *width, gint *height);
  void (*render)(GtkCellRenderer *cell, cairo_t *cr, GtkWidget *widget,
                 const GdkRectangle *background_area,
                 const GdkRectangle *cell_area, GtkCellRendererState flags);
  gboolean (*activate)(GtkCellRenderer *cell, GdkEvent *event,
                       GtkWidget *widget, const gchar *path,
                       const GdkRectangle *background_area,
                       const GdkRectangle *cell_area,
                       GtkCellRendererState flags);
  GtkCellEditable *(*start_editing)(GtkCellRenderer *cell, GdkEvent *event,
                                    GtkWidget *widget, const gchar *path,
                                    const GdkRectangle *background_area,
                                    const GdkRectangle *cell_area,
                                    GtkCellRendererState flags);
  void (*editing_canceled)(GtkCellRenderer *cell);
  void (*editing_started)(GtkCellRenderer *cell, GtkCellEditable *editable,
                          const gchar *path);
  GtkCellRendererClassPrivate *priv;
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};
void gtk_cell_renderer_class_set_accessible_type(
    GtkCellRendererClass *renderer_class, GType type);

typedef struct _GtkCellRendererClassPrivate GtkCellRendererClassPrivate;

typedef struct _GtkCellRendererComboClass GtkCellRendererComboClass;

struct _GtkCellRendererComboClass {
  GtkCellRendererTextClass parent;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCellRendererComboPrivate GtkCellRendererComboPrivate;

enum GtkCellRendererMode {
  GTK_CELL_RENDERER_MODE_INERT = 0,
  GTK_CELL_RENDERER_MODE_ACTIVATABLE = 1,
  GTK_CELL_RENDERER_MODE_EDITABLE = 2
};

typedef struct _GtkCellRendererPixbufClass GtkCellRendererPixbufClass;

struct _GtkCellRendererPixbufClass {
  GtkCellRendererClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCellRendererPixbufPrivate GtkCellRendererPixbufPrivate;

typedef struct _GtkCellRendererPrivate GtkCellRendererPrivate;

typedef struct _GtkCellRendererProgressClass GtkCellRendererProgressClass;

struct _GtkCellRendererProgressClass {
  GtkCellRendererClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCellRendererProgressPrivate GtkCellRendererProgressPrivate;

typedef struct _GtkCellRendererSpinClass GtkCellRendererSpinClass;

struct _GtkCellRendererSpinClass {
  GtkCellRendererTextClass parent;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCellRendererSpinPrivate GtkCellRendererSpinPrivate;

typedef struct _GtkCellRendererSpinnerClass GtkCellRendererSpinnerClass;

struct _GtkCellRendererSpinnerClass {
  GtkCellRendererClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCellRendererSpinnerPrivate GtkCellRendererSpinnerPrivate;

enum GtkCellRendererState {
  GTK_CELL_RENDERER_SELECTED = 1,
  GTK_CELL_RENDERER_PRELIT = 2,
  GTK_CELL_RENDERER_INSENSITIVE = 4,
  GTK_CELL_RENDERER_SORTED = 8,
  GTK_CELL_RENDERER_FOCUSED = 16,
  GTK_CELL_RENDERER_EXPANDABLE = 32,
  GTK_CELL_RENDERER_EXPANDED = 64
};

typedef struct _GtkCellRendererTextClass GtkCellRendererTextClass;

struct _GtkCellRendererTextClass {
  GtkCellRendererClass parent_class;
  void (*edited)(GtkCellRendererText *cell_renderer_text, const gchar *path,
                 const gchar *new_text);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCellRendererTextPrivate GtkCellRendererTextPrivate;

typedef struct _GtkCellRendererToggleClass GtkCellRendererToggleClass;

struct _GtkCellRendererToggleClass {
  GtkCellRendererClass parent_class;
  void (*toggled)(GtkCellRendererToggle *cell_renderer_toggle,
                  const gchar *path);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCellRendererTogglePrivate GtkCellRendererTogglePrivate;

typedef struct _GtkCellViewClass GtkCellViewClass;

struct _GtkCellViewClass {
  GtkWidgetClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCellViewPrivate GtkCellViewPrivate;

typedef struct _GtkCheckButtonClass GtkCheckButtonClass;

struct _GtkCheckButtonClass {
  GtkToggleButtonClass parent_class;
  void (*draw_indicator)(GtkCheckButton *check_button, cairo_t *cr);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCheckMenuItemAccessibleClass GtkCheckMenuItemAccessibleClass;

struct _GtkCheckMenuItemAccessibleClass {
  GtkMenuItemAccessibleClass parent_class;
};

typedef struct _GtkCheckMenuItemAccessiblePrivate
    GtkCheckMenuItemAccessiblePrivate;

typedef struct _GtkCheckMenuItemClass GtkCheckMenuItemClass;

struct _GtkCheckMenuItemClass {
  GtkMenuItemClass parent_class;
  void (*toggled)(GtkCheckMenuItem *check_menu_item);
  void (*draw_indicator)(GtkCheckMenuItem *check_menu_item, cairo_t *cr);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkCheckMenuItemPrivate GtkCheckMenuItemPrivate;
void (*ClipboardClearFunc)(GtkClipboard *clipboard,
                           gpointer user_data_or_owner);
void (*ClipboardGetFunc)(GtkClipboard *clipboard,
                         GtkSelectionData *selection_data, guint info,
                         gpointer user_data_or_owner);
void (*ClipboardImageReceivedFunc)(GtkClipboard *clipboard, GdkPixbuf *pixbuf,
                                   gpointer data);
void (*ClipboardReceivedFunc)(GtkClipboard *clipboard,
                              GtkSelectionData *selection_data, gpointer data);
void (*ClipboardRichTextReceivedFunc)(GtkClipboard *clipboard, GdkAtom format,
                                      const guint8 *text, gsize length,
                                      gpointer data);
void (*ClipboardTargetsReceivedFunc)(GtkClipboard *clipboard, atoms,
                                     gint n_atoms, gpointer data);
void (*ClipboardTextReceivedFunc)(GtkClipboard *clipboard, const gchar *text,
                                  gpointer data);
void (*ClipboardURIReceivedFunc)(GtkClipboard *clipboard, uris, gpointer data);

typedef struct _GtkColorButtonClass GtkColorButtonClass;

struct _GtkColorButtonClass {
  GtkButtonClass parent_class;
  void (*color_set)(GtkColorButton *cp);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkColorButtonPrivate GtkColorButtonPrivate;

typedef struct _GtkColorChooserDialogClass GtkColorChooserDialogClass;

struct _GtkColorChooserDialogClass {
  GtkDialogClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkColorChooserDialogPrivate GtkColorChooserDialogPrivate;

typedef struct _GtkColorChooserInterface GtkColorChooserInterface;

struct _GtkColorChooserInterface {
  GTypeInterface base_interface;
  void (*get_rgba)(GtkColorChooser *chooser, GdkRGBA *color);
  void (*set_rgba)(GtkColorChooser *chooser, const GdkRGBA *color);
  void (*add_palette)(GtkColorChooser *chooser, GtkOrientation orientation,
                      gint colors_per_line, gint n_colors, colors);
  void (*color_activated)(GtkColorChooser *chooser, const GdkRGBA *color);
  gpointer padding[12];
};

typedef struct _GtkColorChooserWidgetClass GtkColorChooserWidgetClass;

struct _GtkColorChooserWidgetClass {
  GtkBoxClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};

typedef struct _GtkColorChooserWidgetPrivate GtkColorChooserWidgetPrivate;
void (*ColorSelectionChangePaletteFunc)(colors, gint n_colors);
void (*ColorSelectionChangePaletteWithScreenFunc)(GdkScreen *screen, colors,
                                                  gint n_colors);

typedef struct _GtkColorSelectionClass GtkColorSelectionClass;

struct _GtkColorSelectionClass {
  GtkBoxClass parent_class;
  void (*color_changed)(GtkColorSelection *color_selection);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkColorSelectionDialogClass GtkColorSelectionDialogClass;

struct _GtkColorSelectionDialogClass {
  GtkDialogClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkColorSelectionDialogPrivate GtkColorSelectionDialogPrivate;

typedef struct _GtkColorSelectionPrivate GtkColorSelectionPrivate;

typedef struct _GtkComboBoxAccessibleClass GtkComboBoxAccessibleClass;

struct _GtkComboBoxAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkComboBoxAccessiblePrivate GtkComboBoxAccessiblePrivate;

typedef struct _GtkComboBoxClass GtkComboBoxClass;

struct _GtkComboBoxClass {
  GtkBinClass parent_class;
  void (*changed)(GtkComboBox *combo_box);
  gchar *(*format_entry_text)(GtkComboBox *combo_box, const gchar *path);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
};

typedef struct _GtkComboBoxPrivate GtkComboBoxPrivate;

typedef struct _GtkComboBoxTextClass GtkComboBoxTextClass;

struct _GtkComboBoxTextClass {
  GtkComboBoxClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkComboBoxTextPrivate GtkComboBoxTextPrivate;

typedef struct _GtkContainerAccessibleClass GtkContainerAccessibleClass;

struct _GtkContainerAccessibleClass {
  GtkWidgetAccessibleClass parent_class;
  gint (*add_gtk)(GtkContainer *container, GtkWidget *widget, gpointer data);
  gint (*remove_gtk)(GtkContainer *container, GtkWidget *widget, gpointer data);
};

typedef struct _GtkContainerAccessiblePrivate GtkContainerAccessiblePrivate;

typedef struct _GtkContainerCellAccessibleClass GtkContainerCellAccessibleClass;

struct _GtkContainerCellAccessibleClass {
  GtkCellAccessibleClass parent_class;
};

typedef struct _GtkContainerCellAccessiblePrivate
    GtkContainerCellAccessiblePrivate;

typedef struct _GtkContainerClass GtkContainerClass;

struct _GtkContainerClass {
  GtkWidgetClass parent_class;
  void (*add)(GtkContainer *container, GtkWidget *widget);
  void (*remove)(GtkContainer *container, GtkWidget *widget);
  void (*check_resize)(GtkContainer *container);
  void (*forall)(GtkContainer *container, gboolean include_internals,
                 GtkCallback callback, gpointer callback_data);
  void (*set_focus_child)(GtkContainer *container, GtkWidget *child);
  GType (*child_type)(GtkContainer *container);
  gchar *(*composite_name)(GtkContainer *container, GtkWidget *child);
  void (*set_child_property)(GtkContainer *container, GtkWidget *child,
                             guint property_id, const GValue *value,
                             GParamSpec *pspec);
  void (*get_child_property)(GtkContainer *container, GtkWidget *child,
                             guint property_id, GValue *value,
                             GParamSpec *pspec);
  GtkWidgetPath *(*get_path_for_child)(GtkContainer *container,
                                       GtkWidget *child);
  unsigned _handle_border_width;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};
GParamSpec *gtk_container_class_find_child_property(GObjectClass *cclass,
                                                    const gchar *property_name);
void gtk_container_class_handle_border_width(GtkContainerClass *klass);
void gtk_container_class_install_child_properties(GtkContainerClass *cclass,
                                                  guint n_pspecs, pspecs);
void gtk_container_class_install_child_property(GtkContainerClass *cclass,
                                                guint property_id,
                                                GParamSpec *pspec);
gtk_container_class_list_child_properties(GObjectClass *cclass,
                                          guint *n_properties);

typedef struct _GtkContainerPrivate GtkContainerPrivate;

enum GtkCornerType {
  GTK_CORNER_TOP_LEFT = 0,
  GTK_CORNER_BOTTOM_LEFT = 1,
  GTK_CORNER_TOP_RIGHT = 2,
  GTK_CORNER_BOTTOM_RIGHT = 3
};

typedef struct _GtkCssProviderClass GtkCssProviderClass;

struct _GtkCssProviderClass {
  GObjectClass parent_class;
  void (*parsing_error)(GtkCssProvider *provider, GtkCssSection *section,
                        const GError *error);
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkCssProviderError {
  GTK_CSS_PROVIDER_ERROR_FAILED = 0,
  GTK_CSS_PROVIDER_ERROR_SYNTAX = 1,
  GTK_CSS_PROVIDER_ERROR_IMPORT = 2,
  GTK_CSS_PROVIDER_ERROR_NAME = 3,
  GTK_CSS_PROVIDER_ERROR_DEPRECATED = 4,
  GTK_CSS_PROVIDER_ERROR_UNKNOWN_VALUE = 5
};

typedef struct _GtkCssProviderPrivate GtkCssProviderPrivate;

typedef struct _GtkCssSection GtkCssSection;
guint gtk_css_section_get_end_line(const GtkCssSection *section);
guint gtk_css_section_get_end_position(const GtkCssSection *section);
GFile *gtk_css_section_get_file(const GtkCssSection *section);
GtkCssSection *gtk_css_section_get_parent(const GtkCssSection *section);
GtkCssSectionType
gtk_css_section_get_section_type(const GtkCssSection *section);
guint gtk_css_section_get_start_line(const GtkCssSection *section);
guint gtk_css_section_get_start_position(const GtkCssSection *section);
GtkCssSection *gtk_css_section_ref(GtkCssSection *section);
void gtk_css_section_unref(GtkCssSection *section);

enum GtkCssSectionType {
  GTK_CSS_SECTION_DOCUMENT = 0,
  GTK_CSS_SECTION_IMPORT = 1,
  GTK_CSS_SECTION_COLOR_DEFINITION = 2,
  GTK_CSS_SECTION_BINDING_SET = 3,
  GTK_CSS_SECTION_RULESET = 4,
  GTK_CSS_SECTION_SELECTOR = 5,
  GTK_CSS_SECTION_DECLARATION = 6,
  GTK_CSS_SECTION_VALUE = 7,
  GTK_CSS_SECTION_KEYFRAMES = 8
};

enum GtkDebugFlag {
  GTK_DEBUG_MISC = 1,
  GTK_DEBUG_PLUGSOCKET = 2,
  GTK_DEBUG_TEXT = 4,
  GTK_DEBUG_TREE = 8,
  GTK_DEBUG_UPDATES = 16,
  GTK_DEBUG_KEYBINDINGS = 32,
  GTK_DEBUG_MULTIHEAD = 64,
  GTK_DEBUG_MODULES = 128,
  GTK_DEBUG_GEOMETRY = 256,
  GTK_DEBUG_ICONTHEME = 512,
  GTK_DEBUG_PRINTING = 1024,
  GTK_DEBUG_BUILDER = 2048,
  GTK_DEBUG_SIZE_REQUEST = 4096,
  GTK_DEBUG_NO_CSS_CACHE = 8192,
  GTK_DEBUG_BASELINES = 16384,
  GTK_DEBUG_PIXEL_CACHE = 32768,
  GTK_DEBUG_NO_PIXEL_CACHE = 65536,
  GTK_DEBUG_INTERACTIVE = 131072,
  GTK_DEBUG_TOUCHSCREEN = 262144,
  GTK_DEBUG_ACTIONS = 524288,
  GTK_DEBUG_RESIZE = 1048576,
  GTK_DEBUG_LAYOUT = 2097152
};

enum GtkDeleteType {
  GTK_DELETE_CHARS = 0,
  GTK_DELETE_WORD_ENDS = 1,
  GTK_DELETE_WORDS = 2,
  GTK_DELETE_DISPLAY_LINES = 3,
  GTK_DELETE_DISPLAY_LINE_ENDS = 4,
  GTK_DELETE_PARAGRAPH_ENDS = 5,
  GTK_DELETE_PARAGRAPHS = 6,
  GTK_DELETE_WHITESPACE = 7
};

enum GtkDestDefaults {
  GTK_DEST_DEFAULT_MOTION = 1,
  GTK_DEST_DEFAULT_HIGHLIGHT = 2,
  GTK_DEST_DEFAULT_DROP = 4,
  GTK_DEST_DEFAULT_ALL = 7
};

typedef struct _GtkDialogClass GtkDialogClass;

struct _GtkDialogClass {
  GtkWindowClass parent_class;
  void (*response)(GtkDialog *dialog, gint response_id);
  void (*close)(GtkDialog *dialog);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkDialogFlags {
  GTK_DIALOG_MODAL = 1,
  GTK_DIALOG_DESTROY_WITH_PARENT = 2,
  GTK_DIALOG_USE_HEADER_BAR = 4
};

typedef struct _GtkDialogPrivate GtkDialogPrivate;

enum GtkDirectionType {
  GTK_DIR_TAB_FORWARD = 0,
  GTK_DIR_TAB_BACKWARD = 1,
  GTK_DIR_UP = 2,
  GTK_DIR_DOWN = 3,
  GTK_DIR_LEFT = 4,
  GTK_DIR_RIGHT = 5
};

enum GtkDragResult {
  GTK_DRAG_RESULT_SUCCESS = 0,
  GTK_DRAG_RESULT_NO_TARGET = 1,
  GTK_DRAG_RESULT_USER_CANCELLED = 2,
  GTK_DRAG_RESULT_TIMEOUT_EXPIRED = 3,
  GTK_DRAG_RESULT_GRAB_BROKEN = 4,
  GTK_DRAG_RESULT_ERROR = 5
};

typedef struct _GtkDrawingAreaClass GtkDrawingAreaClass;

struct _GtkDrawingAreaClass {
  GtkWidgetClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkEditableInterface GtkEditableInterface;

struct _GtkEditableInterface {
  GTypeInterface base_iface;
  void (*insert_text)(GtkEditable *editable, const gchar *new_text,
                      gint new_text_length, gint *position);
  void (*delete_text)(GtkEditable *editable, gint start_pos, gint end_pos);
  void (*changed)(GtkEditable *editable);
  void (*do_insert_text)(GtkEditable *editable, const gchar *new_text,
                         gint new_text_length, gint *position);
  void (*do_delete_text)(GtkEditable *editable, gint start_pos, gint end_pos);
  gchar *(*get_chars)(GtkEditable *editable, gint start_pos, gint end_pos);
  void (*set_selection_bounds)(GtkEditable *editable, gint start_pos,
                               gint end_pos);
  gboolean (*get_selection_bounds)(GtkEditable *editable, gint *start_pos,
                                   gint *end_pos);
  void (*set_position)(GtkEditable *editable, gint position);
  gint (*get_position)(GtkEditable *editable);
};

typedef struct _GtkEntryAccessibleClass GtkEntryAccessibleClass;

struct _GtkEntryAccessibleClass {
  GtkWidgetAccessibleClass parent_class;
};

typedef struct _GtkEntryAccessiblePrivate GtkEntryAccessiblePrivate;

typedef struct _GtkEntryBufferClass GtkEntryBufferClass;

struct _GtkEntryBufferClass {
  GObjectClass parent_class;
  void (*inserted_text)(GtkEntryBuffer *buffer, guint position,
                        const gchar *chars, guint n_chars);
  void (*deleted_text)(GtkEntryBuffer *buffer, guint position, guint n_chars);
  const gchar *(*get_text)(GtkEntryBuffer *buffer, gsize *n_bytes);
  guint (*get_length)(GtkEntryBuffer *buffer);
  guint (*insert_text)(GtkEntryBuffer *buffer, guint position,
                       const gchar *chars, guint n_chars);
  guint (*delete_text)(GtkEntryBuffer *buffer, guint position, guint n_chars);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};

typedef struct _GtkEntryBufferPrivate GtkEntryBufferPrivate;

typedef struct _GtkEntryClass GtkEntryClass;

struct _GtkEntryClass {
  GtkWidgetClass parent_class;
  void (*populate_popup)(GtkEntry *entry, GtkWidget *popup);
  void (*activate)(GtkEntry *entry);
  void (*move_cursor)(GtkEntry *entry, GtkMovementStep step, gint count,
                      gboolean extend_selection);
  void (*insert_at_cursor)(GtkEntry *entry, const gchar *str);
  void (*delete_from_cursor)(GtkEntry *entry, GtkDeleteType type, gint count);
  void (*backspace)(GtkEntry *entry);
  void (*cut_clipboard)(GtkEntry *entry);
  void (*copy_clipboard)(GtkEntry *entry);
  void (*paste_clipboard)(GtkEntry *entry);
  void (*toggle_overwrite)(GtkEntry *entry);
  void (*get_text_area_size)(GtkEntry *entry, gint *x, gint *y, gint *width,
                             gint *height);
  void (*get_frame_size)(GtkEntry *entry, gint *x, gint *y, gint *width,
                         gint *height);
  void (*insert_emoji)(GtkEntry *entry);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
};

typedef struct _GtkEntryCompletionClass GtkEntryCompletionClass;

struct _GtkEntryCompletionClass {
  GObjectClass parent_class;
  gboolean (*match_selected)(GtkEntryCompletion *completion,
                             GtkTreeModel *model, GtkTreeIter *iter);
  void (*action_activated)(GtkEntryCompletion *completion, gint index_);
  gboolean (*insert_prefix)(GtkEntryCompletion *completion,
                            const gchar *prefix);
  gboolean (*cursor_on_match)(GtkEntryCompletion *completion,
                              GtkTreeModel *model, GtkTreeIter *iter);
  void (*no_matches)(GtkEntryCompletion *completion);
  void (*_gtk_reserved0)();
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
};
gboolean (*EntryCompletionMatchFunc)(GtkEntryCompletion *completion,
                                     const gchar *key, GtkTreeIter *iter,
                                     gpointer user_data);

typedef struct _GtkEntryCompletionPrivate GtkEntryCompletionPrivate;

enum GtkEntryIconPosition {
  GTK_ENTRY_ICON_PRIMARY = 0,
  GTK_ENTRY_ICON_SECONDARY = 1
};

typedef struct _GtkEntryPrivate GtkEntryPrivate;

typedef struct _GtkEventBoxClass GtkEventBoxClass;

struct _GtkEventBoxClass {
  GtkBinClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkEventBoxPrivate GtkEventBoxPrivate;

typedef struct _GtkEventControllerClass GtkEventControllerClass;

enum GtkEventSequenceState {
  GTK_EVENT_SEQUENCE_NONE = 0,
  GTK_EVENT_SEQUENCE_CLAIMED = 1,
  GTK_EVENT_SEQUENCE_DENIED = 2
};

typedef struct _GtkExpanderAccessibleClass GtkExpanderAccessibleClass;

struct _GtkExpanderAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkExpanderAccessiblePrivate GtkExpanderAccessiblePrivate;

typedef struct _GtkExpanderClass GtkExpanderClass;

struct _GtkExpanderClass {
  GtkBinClass parent_class;
  void (*activate)(GtkExpander *expander);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkExpanderPrivate GtkExpanderPrivate;

enum GtkExpanderStyle {
  GTK_EXPANDER_COLLAPSED = 0,
  GTK_EXPANDER_SEMI_COLLAPSED = 1,
  GTK_EXPANDER_SEMI_EXPANDED = 2,
  GTK_EXPANDER_EXPANDED = 3
};

enum GtkFileChooserAction {
  GTK_FILE_CHOOSER_ACTION_OPEN = 0,
  GTK_FILE_CHOOSER_ACTION_SAVE = 1,
  GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER = 2,
  GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER = 3
};

typedef struct _GtkFileChooserButtonClass GtkFileChooserButtonClass;

struct _GtkFileChooserButtonClass {
  GtkBoxClass parent_class;
  void (*file_set)(GtkFileChooserButton *fc);
  void *__gtk_reserved1;
  void *__gtk_reserved2;
  void *__gtk_reserved3;
  void *__gtk_reserved4;
};

typedef struct _GtkFileChooserButtonPrivate GtkFileChooserButtonPrivate;

enum GtkFileChooserConfirmation {
  GTK_FILE_CHOOSER_CONFIRMATION_CONFIRM = 0,
  GTK_FILE_CHOOSER_CONFIRMATION_ACCEPT_FILENAME = 1,
  GTK_FILE_CHOOSER_CONFIRMATION_SELECT_AGAIN = 2
};

typedef struct _GtkFileChooserDialogClass GtkFileChooserDialogClass;

struct _GtkFileChooserDialogClass {
  GtkDialogClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkFileChooserDialogPrivate GtkFileChooserDialogPrivate;

enum GtkFileChooserError {
  GTK_FILE_CHOOSER_ERROR_NONEXISTENT = 0,
  GTK_FILE_CHOOSER_ERROR_BAD_FILENAME = 1,
  GTK_FILE_CHOOSER_ERROR_ALREADY_EXISTS = 2,
  GTK_FILE_CHOOSER_ERROR_INCOMPLETE_HOSTNAME = 3
};

typedef struct _GtkFileChooserNativeClass GtkFileChooserNativeClass;

struct _GtkFileChooserNativeClass {
  GtkNativeDialogClass parent_class;
};

typedef struct _GtkFileChooserWidgetClass GtkFileChooserWidgetClass;

struct _GtkFileChooserWidgetClass {
  GtkBoxClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkFileChooserWidgetPrivate GtkFileChooserWidgetPrivate;

enum GtkFileFilterFlags {
  GTK_FILE_FILTER_FILENAME = 1,
  GTK_FILE_FILTER_URI = 2,
  GTK_FILE_FILTER_DISPLAY_NAME = 4,
  GTK_FILE_FILTER_MIME_TYPE = 8
};
gboolean (*FileFilterFunc)(const GtkFileFilterInfo *filter_info, gpointer data);

typedef struct _GtkFileFilterInfo GtkFileFilterInfo;

struct _GtkFileFilterInfo {
  GtkFileFilterFlags contains;
  const gchar *filename;
  const gchar *uri;
  const gchar *display_name;
  const gchar *mime_type;
};

typedef struct _GtkFixedChild GtkFixedChild;

struct _GtkFixedChild {
  GtkWidget *widget;
  gint x;
  gint y;
};

typedef struct _GtkFixedClass GtkFixedClass;

struct _GtkFixedClass {
  GtkContainerClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkFixedPrivate GtkFixedPrivate;

typedef struct _GtkFlowBoxAccessibleClass GtkFlowBoxAccessibleClass;

struct _GtkFlowBoxAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkFlowBoxAccessiblePrivate GtkFlowBoxAccessiblePrivate;

typedef struct _GtkFlowBoxChildAccessibleClass GtkFlowBoxChildAccessibleClass;

struct _GtkFlowBoxChildAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkFlowBoxChildClass GtkFlowBoxChildClass;

struct _GtkFlowBoxChildClass {
  GtkBinClass parent_class;
  void (*activate)(GtkFlowBoxChild *child);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
};

typedef struct _GtkFlowBoxClass GtkFlowBoxClass;

struct _GtkFlowBoxClass {
  GtkContainerClass parent_class;
  void (*child_activated)(GtkFlowBox *box, GtkFlowBoxChild *child);
  void (*selected_children_changed)(GtkFlowBox *box);
  void (*activate_cursor_child)(GtkFlowBox *box);
  void (*toggle_cursor_child)(GtkFlowBox *box);
  gboolean (*move_cursor)(GtkFlowBox *box, GtkMovementStep step, gint count);
  void (*select_all)(GtkFlowBox *box);
  void (*unselect_all)(GtkFlowBox *box);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
};
GtkWidget *(*FlowBoxCreateWidgetFunc)(gpointer item, gpointer user_data);
gboolean (*FlowBoxFilterFunc)(GtkFlowBoxChild *child, gpointer user_data);
void (*FlowBoxForeachFunc)(GtkFlowBox *box, GtkFlowBoxChild *child,
                           gpointer user_data);
gint (*FlowBoxSortFunc)(GtkFlowBoxChild *child1, GtkFlowBoxChild *child2,
                        gpointer user_data);

typedef struct _GtkFontButtonClass GtkFontButtonClass;

struct _GtkFontButtonClass {
  GtkButtonClass parent_class;
  void (*font_set)(GtkFontButton *gfp);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkFontButtonPrivate GtkFontButtonPrivate;

typedef struct _GtkFontChooserDialogClass GtkFontChooserDialogClass;

struct _GtkFontChooserDialogClass {
  GtkDialogClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkFontChooserDialogPrivate GtkFontChooserDialogPrivate;

typedef struct _GtkFontChooserIface GtkFontChooserIface;

struct _GtkFontChooserIface {
  GTypeInterface base_iface;
  PangoFontFamily *(*get_font_family)(GtkFontChooser *fontchooser);
  PangoFontFace *(*get_font_face)(GtkFontChooser *fontchooser);
  gint (*get_font_size)(GtkFontChooser *fontchooser);
  void (*set_filter_func)(GtkFontChooser *fontchooser, GtkFontFilterFunc filter,
                          gpointer user_data, GDestroyNotify destroy);
  void (*font_activated)(GtkFontChooser *chooser, const gchar *fontname);
  void (*set_font_map)(GtkFontChooser *fontchooser, PangoFontMap *fontmap);
  PangoFontMap *(*get_font_map)(GtkFontChooser *fontchooser);
  gpointer padding[10];
};

typedef struct _GtkFontChooserWidgetClass GtkFontChooserWidgetClass;

struct _GtkFontChooserWidgetClass {
  GtkBoxClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};

typedef struct _GtkFontChooserWidgetPrivate GtkFontChooserWidgetPrivate;
gboolean (*FontFilterFunc)(const PangoFontFamily *family,
                           const PangoFontFace *face, gpointer data);

typedef struct _GtkFontSelectionClass GtkFontSelectionClass;

struct _GtkFontSelectionClass {
  GtkBoxClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkFontSelectionDialogClass GtkFontSelectionDialogClass;

struct _GtkFontSelectionDialogClass {
  GtkDialogClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkFontSelectionDialogPrivate GtkFontSelectionDialogPrivate;

typedef struct _GtkFontSelectionPrivate GtkFontSelectionPrivate;

typedef struct _GtkFrameAccessibleClass GtkFrameAccessibleClass;

struct _GtkFrameAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkFrameAccessiblePrivate GtkFrameAccessiblePrivate;

typedef struct _GtkFrameClass GtkFrameClass;

struct _GtkFrameClass {
  GtkBinClass parent_class;
  void (*compute_child_allocation)(GtkFrame *frame, GtkAllocation *allocation);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkFramePrivate GtkFramePrivate;

typedef struct _GtkGLAreaClass GtkGLAreaClass;

struct _GtkGLAreaClass {
  GtkWidgetClass parent_class;
  gboolean (*render)(GtkGLArea *area, GdkGLContext *context);
  void (*resize)(GtkGLArea *area, int width, int height);
  GdkGLContext *(*create_context)(GtkGLArea *area);
  gpointer _padding[6];
};

typedef struct _GtkGestureClass GtkGestureClass;

typedef struct _GtkGestureDragClass GtkGestureDragClass;

typedef struct _GtkGestureLongPressClass GtkGestureLongPressClass;

typedef struct _GtkGestureMultiPressClass GtkGestureMultiPressClass;

typedef struct _GtkGesturePanClass GtkGesturePanClass;

typedef struct _GtkGestureRotateClass GtkGestureRotateClass;

typedef struct _GtkGestureSingleClass GtkGestureSingleClass;

typedef struct _GtkGestureSwipeClass GtkGestureSwipeClass;

typedef struct _GtkGestureZoomClass GtkGestureZoomClass;

typedef struct _GtkGradient GtkGradient;
void gtk_gradient_add_color_stop(GtkGradient *gradient, gdouble offset,
                                 GtkSymbolicColor *color);
GtkGradient *gtk_gradient_ref(GtkGradient *gradient);
gboolean gtk_gradient_resolve(GtkGradient *gradient, GtkStyleProperties *props,
                              cairo_pattern_t **resolved_gradient);
cairo_pattern_t *gtk_gradient_resolve_for_context(GtkGradient *gradient,
                                                  GtkStyleContext *context);
char *gtk_gradient_to_string(GtkGradient *gradient);
void gtk_gradient_unref(GtkGradient *gradient);

typedef struct _GtkGridClass GtkGridClass;

struct _GtkGridClass {
  GtkContainerClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};

typedef struct _GtkGridPrivate GtkGridPrivate;

typedef struct _GtkHBoxClass GtkHBoxClass;

struct _GtkHBoxClass {
  GtkBoxClass parent_class;
};

typedef struct _GtkHButtonBoxClass GtkHButtonBoxClass;

struct _GtkHButtonBoxClass {
  GtkButtonBoxClass parent_class;
};

typedef struct _GtkHPanedClass GtkHPanedClass;

struct _GtkHPanedClass {
  GtkPanedClass parent_class;
};

typedef struct _GtkHSVClass GtkHSVClass;

struct _GtkHSVClass {
  GtkWidgetClass parent_class;
  void (*changed)(GtkHSV *hsv);
  void (*move)(GtkHSV *hsv, GtkDirectionType type);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkHSVPrivate GtkHSVPrivate;

typedef struct _GtkHScaleClass GtkHScaleClass;

struct _GtkHScaleClass {
  GtkScaleClass parent_class;
};

typedef struct _GtkHScrollbarClass GtkHScrollbarClass;

struct _GtkHScrollbarClass {
  GtkScrollbarClass parent_class;
};

typedef struct _GtkHSeparatorClass GtkHSeparatorClass;

struct _GtkHSeparatorClass {
  GtkSeparatorClass parent_class;
};

typedef struct _GtkHandleBoxClass GtkHandleBoxClass;

struct _GtkHandleBoxClass {
  GtkBinClass parent_class;
  void (*child_attached)(GtkHandleBox *handle_box, GtkWidget *child);
  void (*child_detached)(GtkHandleBox *handle_box, GtkWidget *child);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkHandleBoxPrivate GtkHandleBoxPrivate;

typedef struct _GtkHeaderBarClass GtkHeaderBarClass;

struct _GtkHeaderBarClass {
  GtkContainerClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkHeaderBarPrivate GtkHeaderBarPrivate;

typedef struct _GtkIMContextClass GtkIMContextClass;

struct _GtkIMContextClass {
  GObjectClass parent_class;
  void (*preedit_start)(GtkIMContext *context);
  void (*preedit_end)(GtkIMContext *context);
  void (*preedit_changed)(GtkIMContext *context);
  void (*commit)(GtkIMContext *context, const gchar *str);
  gboolean (*retrieve_surrounding)(GtkIMContext *context);
  gboolean (*delete_surrounding)(GtkIMContext *context, gint offset,
                                 gint n_chars);
  void (*set_client_window)(GtkIMContext *context, GdkWindow *window);
  void (*get_preedit_string)(GtkIMContext *context, gchar **str,
                             PangoAttrList **attrs, gint *cursor_pos);
  gboolean (*filter_keypress)(GtkIMContext *context, GdkEventKey *event);
  void (*focus_in)(GtkIMContext *context);
  void (*focus_out)(GtkIMContext *context);
  void (*reset)(GtkIMContext *context);
  void (*set_cursor_location)(GtkIMContext *context, GdkRectangle *area);
  void (*set_use_preedit)(GtkIMContext *context, gboolean use_preedit);
  void (*set_surrounding)(GtkIMContext *context, const gchar *text, gint len,
                          gint cursor_index);
  gboolean (*get_surrounding)(GtkIMContext *context, gchar **text,
                              gint *cursor_index);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
};

typedef struct _GtkIMContextInfo GtkIMContextInfo;

struct _GtkIMContextInfo {
  const gchar *context_id;
  const gchar *context_name;
  const gchar *domain;
  const gchar *domain_dirname;
  const gchar *default_locales;
};

typedef struct _GtkIMContextSimpleClass GtkIMContextSimpleClass;

struct _GtkIMContextSimpleClass {
  GtkIMContextClass parent_class;
};

typedef struct _GtkIMContextSimplePrivate GtkIMContextSimplePrivate;

typedef struct _GtkIMMulticontextClass GtkIMMulticontextClass;

struct _GtkIMMulticontextClass {
  GtkIMContextClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkIMMulticontextPrivate GtkIMMulticontextPrivate;

enum GtkIMPreeditStyle {
  GTK_IM_PREEDIT_NOTHING = 0,
  GTK_IM_PREEDIT_CALLBACK = 1,
  GTK_IM_PREEDIT_NONE = 2
};

enum GtkIMStatusStyle {
  GTK_IM_STATUS_NOTHING = 0,
  GTK_IM_STATUS_CALLBACK = 1,
  GTK_IM_STATUS_NONE = 2
};

const gint GTK_INPUT_ERROR = -1;

const gint GTK_INTERFACE_AGE = 30;

typedef struct _GtkIconFactoryClass GtkIconFactoryClass;

struct _GtkIconFactoryClass {
  GObjectClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkIconFactoryPrivate GtkIconFactoryPrivate;

typedef struct _GtkIconInfoClass GtkIconInfoClass;

enum GtkIconLookupFlags {
  GTK_ICON_LOOKUP_NO_SVG = 1,
  GTK_ICON_LOOKUP_FORCE_SVG = 2,
  GTK_ICON_LOOKUP_USE_BUILTIN = 4,
  GTK_ICON_LOOKUP_GENERIC_FALLBACK = 8,
  GTK_ICON_LOOKUP_FORCE_SIZE = 16,
  GTK_ICON_LOOKUP_FORCE_REGULAR = 32,
  GTK_ICON_LOOKUP_FORCE_SYMBOLIC = 64,
  GTK_ICON_LOOKUP_DIR_LTR = 128,
  GTK_ICON_LOOKUP_DIR_RTL = 256
};

typedef struct _GtkIconSet GtkIconSet;
void gtk_icon_set_add_source(GtkIconSet *icon_set, const GtkIconSource *source);
GtkIconSet *gtk_icon_set_copy(GtkIconSet *icon_set);
void gtk_icon_set_get_sizes(GtkIconSet *icon_set, sizes, gint *n_sizes);
GtkIconSet *gtk_icon_set_ref(GtkIconSet *icon_set);
GdkPixbuf *gtk_icon_set_render_icon(GtkIconSet *icon_set, GtkStyle *style,
                                    GtkTextDirection direction,
                                    GtkStateType state, GtkIconSize size,
                                    GtkWidget *widget, const gchar *detail);
GdkPixbuf *gtk_icon_set_render_icon_pixbuf(GtkIconSet *icon_set,
                                           GtkStyleContext *context,
                                           GtkIconSize size);
cairo_surface_t *gtk_icon_set_render_icon_surface(GtkIconSet *icon_set,
                                                  GtkStyleContext *context,
                                                  GtkIconSize size, int scale,
                                                  GdkWindow *for_window);
void gtk_icon_set_unref(GtkIconSet *icon_set);

enum GtkIconSize {
  GTK_ICON_SIZE_INVALID = 0,
  GTK_ICON_SIZE_MENU = 1,
  GTK_ICON_SIZE_SMALL_TOOLBAR = 2,
  GTK_ICON_SIZE_LARGE_TOOLBAR = 3,
  GTK_ICON_SIZE_BUTTON = 4,
  GTK_ICON_SIZE_DND = 5,
  GTK_ICON_SIZE_DIALOG = 6
};

typedef struct _GtkIconSource GtkIconSource;
GtkIconSource *gtk_icon_source_copy(const GtkIconSource *source);
void gtk_icon_source_free(GtkIconSource *source);
GtkTextDirection gtk_icon_source_get_direction(const GtkIconSource *source);
gboolean gtk_icon_source_get_direction_wildcarded(const GtkIconSource *source);
const gchar *gtk_icon_source_get_filename(const GtkIconSource *source);
const gchar *gtk_icon_source_get_icon_name(const GtkIconSource *source);
GdkPixbuf *gtk_icon_source_get_pixbuf(const GtkIconSource *source);
GtkIconSize gtk_icon_source_get_size(const GtkIconSource *source);
gboolean gtk_icon_source_get_size_wildcarded(const GtkIconSource *source);
GtkStateType gtk_icon_source_get_state(const GtkIconSource *source);
gboolean gtk_icon_source_get_state_wildcarded(const GtkIconSource *source);
void gtk_icon_source_set_direction(GtkIconSource *source,
                                   GtkTextDirection direction);
void gtk_icon_source_set_direction_wildcarded(GtkIconSource *source,
                                              gboolean setting);
void gtk_icon_source_set_filename(GtkIconSource *source, const gchar *filename);
void gtk_icon_source_set_icon_name(GtkIconSource *source,
                                   const gchar *icon_name);
void gtk_icon_source_set_pixbuf(GtkIconSource *source, GdkPixbuf *pixbuf);
void gtk_icon_source_set_size(GtkIconSource *source, GtkIconSize size);
void gtk_icon_source_set_size_wildcarded(GtkIconSource *source,
                                         gboolean setting);
void gtk_icon_source_set_state(GtkIconSource *source, GtkStateType state);
void gtk_icon_source_set_state_wildcarded(GtkIconSource *source,
                                          gboolean setting);

typedef struct _GtkIconThemeClass GtkIconThemeClass;

struct _GtkIconThemeClass {
  GObjectClass parent_class;
  void (*changed)(GtkIconTheme *icon_theme);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkIconThemeError {
  GTK_ICON_THEME_NOT_FOUND = 0,
  GTK_ICON_THEME_FAILED = 1
};

typedef struct _GtkIconThemePrivate GtkIconThemePrivate;

typedef struct _GtkIconViewAccessibleClass GtkIconViewAccessibleClass;

struct _GtkIconViewAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkIconViewAccessiblePrivate GtkIconViewAccessiblePrivate;

typedef struct _GtkIconViewClass GtkIconViewClass;

struct _GtkIconViewClass {
  GtkContainerClass parent_class;
  void (*item_activated)(GtkIconView *icon_view, GtkTreePath *path);
  void (*selection_changed)(GtkIconView *icon_view);
  void (*select_all)(GtkIconView *icon_view);
  void (*unselect_all)(GtkIconView *icon_view);
  void (*select_cursor_item)(GtkIconView *icon_view);
  void (*toggle_cursor_item)(GtkIconView *icon_view);
  gboolean (*move_cursor)(GtkIconView *icon_view, GtkMovementStep step,
                          gint count);
  gboolean (*activate_cursor_item)(GtkIconView *icon_view);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkIconViewDropPosition {
  GTK_ICON_VIEW_NO_DROP = 0,
  GTK_ICON_VIEW_DROP_INTO = 1,
  GTK_ICON_VIEW_DROP_LEFT = 2,
  GTK_ICON_VIEW_DROP_RIGHT = 3,
  GTK_ICON_VIEW_DROP_ABOVE = 4,
  GTK_ICON_VIEW_DROP_BELOW = 5
};
void (*IconViewForeachFunc)(GtkIconView *icon_view, GtkTreePath *path,
                            gpointer data);

typedef struct _GtkIconViewPrivate GtkIconViewPrivate;

typedef struct _GtkImageAccessibleClass GtkImageAccessibleClass;

struct _GtkImageAccessibleClass {
  GtkWidgetAccessibleClass parent_class;
};

typedef struct _GtkImageAccessiblePrivate GtkImageAccessiblePrivate;

typedef struct _GtkImageCellAccessibleClass GtkImageCellAccessibleClass;

struct _GtkImageCellAccessibleClass {
  GtkRendererCellAccessibleClass parent_class;
};

typedef struct _GtkImageCellAccessiblePrivate GtkImageCellAccessiblePrivate;

typedef struct _GtkImageClass GtkImageClass;

struct _GtkImageClass {
  GtkMiscClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkImageMenuItemClass GtkImageMenuItemClass;

struct _GtkImageMenuItemClass {
  GtkMenuItemClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkImageMenuItemPrivate GtkImageMenuItemPrivate;

typedef struct _GtkImagePrivate GtkImagePrivate;

enum GtkImageType {
  GTK_IMAGE_EMPTY = 0,
  GTK_IMAGE_PIXBUF = 1,
  GTK_IMAGE_STOCK = 2,
  GTK_IMAGE_ICON_SET = 3,
  GTK_IMAGE_ANIMATION = 4,
  GTK_IMAGE_ICON_NAME = 5,
  GTK_IMAGE_GICON = 6,
  GTK_IMAGE_SURFACE = 7
};

typedef struct _GtkInfoBarClass GtkInfoBarClass;

struct _GtkInfoBarClass {
  GtkBoxClass parent_class;
  void (*response)(GtkInfoBar *info_bar, gint response_id);
  void (*close)(GtkInfoBar *info_bar);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkInfoBarPrivate GtkInfoBarPrivate;

enum GtkInputHints {
  GTK_INPUT_HINT_NONE = 0,
  GTK_INPUT_HINT_SPELLCHECK = 1,
  GTK_INPUT_HINT_NO_SPELLCHECK = 2,
  GTK_INPUT_HINT_WORD_COMPLETION = 4,
  GTK_INPUT_HINT_LOWERCASE = 8,
  GTK_INPUT_HINT_UPPERCASE_CHARS = 16,
  GTK_INPUT_HINT_UPPERCASE_WORDS = 32,
  GTK_INPUT_HINT_UPPERCASE_SENTENCES = 64,
  GTK_INPUT_HINT_INHIBIT_OSK = 128,
  GTK_INPUT_HINT_VERTICAL_WRITING = 256,
  GTK_INPUT_HINT_EMOJI = 512,
  GTK_INPUT_HINT_NO_EMOJI = 1024
};

enum GtkInputPurpose {
  GTK_INPUT_PURPOSE_FREE_FORM = 0,
  GTK_INPUT_PURPOSE_ALPHA = 1,
  GTK_INPUT_PURPOSE_DIGITS = 2,
  GTK_INPUT_PURPOSE_NUMBER = 3,
  GTK_INPUT_PURPOSE_PHONE = 4,
  GTK_INPUT_PURPOSE_URL = 5,
  GTK_INPUT_PURPOSE_EMAIL = 6,
  GTK_INPUT_PURPOSE_NAME = 7,
  GTK_INPUT_PURPOSE_PASSWORD = 8,
  GTK_INPUT_PURPOSE_PIN = 9
};

typedef struct _GtkInvisibleClass GtkInvisibleClass;

struct _GtkInvisibleClass {
  GtkWidgetClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkInvisiblePrivate GtkInvisiblePrivate;

enum GtkJunctionSides {
  GTK_JUNCTION_NONE = 0,
  GTK_JUNCTION_CORNER_TOPLEFT = 1,
  GTK_JUNCTION_CORNER_TOPRIGHT = 2,
  GTK_JUNCTION_CORNER_BOTTOMLEFT = 4,
  GTK_JUNCTION_CORNER_BOTTOMRIGHT = 8,
  GTK_JUNCTION_TOP = 3,
  GTK_JUNCTION_BOTTOM = 12,
  GTK_JUNCTION_LEFT = 5,
  GTK_JUNCTION_RIGHT = 10
};

enum GtkJustification {
  GTK_JUSTIFY_LEFT = 0,
  GTK_JUSTIFY_RIGHT = 1,
  GTK_JUSTIFY_CENTER = 2,
  GTK_JUSTIFY_FILL = 3
};
gint (*KeySnoopFunc)(GtkWidget *grab_widget, GdkEventKey *event,
                     gpointer func_data);

const gchar *GTK_LEVEL_BAR_OFFSET_FULL = "full";

const gchar *GTK_LEVEL_BAR_OFFSET_HIGH = "high";

const gchar *GTK_LEVEL_BAR_OFFSET_LOW = "low";

typedef struct _GtkLabelAccessibleClass GtkLabelAccessibleClass;

struct _GtkLabelAccessibleClass {
  GtkWidgetAccessibleClass parent_class;
};

typedef struct _GtkLabelAccessiblePrivate GtkLabelAccessiblePrivate;

typedef struct _GtkLabelClass GtkLabelClass;

struct _GtkLabelClass {
  GtkMiscClass parent_class;
  void (*move_cursor)(GtkLabel *label, GtkMovementStep step, gint count,
                      gboolean extend_selection);
  void (*copy_clipboard)(GtkLabel *label);
  void (*populate_popup)(GtkLabel *label, GtkMenu *menu);
  gboolean (*activate_link)(GtkLabel *label, const gchar *uri);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};

typedef struct _GtkLabelPrivate GtkLabelPrivate;

typedef struct _GtkLabelSelectionInfo GtkLabelSelectionInfo;

typedef struct _GtkLayoutClass GtkLayoutClass;

struct _GtkLayoutClass {
  GtkContainerClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkLayoutPrivate GtkLayoutPrivate;

typedef struct _GtkLevelBarAccessibleClass GtkLevelBarAccessibleClass;

struct _GtkLevelBarAccessibleClass {
  GtkWidgetAccessibleClass parent_class;
};

typedef struct _GtkLevelBarAccessiblePrivate GtkLevelBarAccessiblePrivate;

typedef struct _GtkLevelBarClass GtkLevelBarClass;

struct _GtkLevelBarClass {
  GtkWidgetClass parent_class;
  void (*offset_changed)(GtkLevelBar *self, const gchar *name);
  gpointer padding[16];
};

enum GtkLevelBarMode {
  GTK_LEVEL_BAR_MODE_CONTINUOUS = 0,
  GTK_LEVEL_BAR_MODE_DISCRETE = 1
};

typedef struct _GtkLevelBarPrivate GtkLevelBarPrivate;

enum GtkLicense {
  GTK_LICENSE_UNKNOWN = 0,
  GTK_LICENSE_CUSTOM = 1,
  GTK_LICENSE_GPL_2_0 = 2,
  GTK_LICENSE_GPL_3_0 = 3,
  GTK_LICENSE_LGPL_2_1 = 4,
  GTK_LICENSE_LGPL_3_0 = 5,
  GTK_LICENSE_BSD = 6,
  GTK_LICENSE_MIT_X11 = 7,
  GTK_LICENSE_ARTISTIC = 8,
  GTK_LICENSE_GPL_2_0_ONLY = 9,
  GTK_LICENSE_GPL_3_0_ONLY = 10,
  GTK_LICENSE_LGPL_2_1_ONLY = 11,
  GTK_LICENSE_LGPL_3_0_ONLY = 12,
  GTK_LICENSE_AGPL_3_0 = 13,
  GTK_LICENSE_AGPL_3_0_ONLY = 14
};

typedef struct _GtkLinkButtonAccessibleClass GtkLinkButtonAccessibleClass;

struct _GtkLinkButtonAccessibleClass {
  GtkButtonAccessibleClass parent_class;
};

typedef struct _GtkLinkButtonAccessiblePrivate GtkLinkButtonAccessiblePrivate;

typedef struct _GtkLinkButtonClass GtkLinkButtonClass;

struct _GtkLinkButtonClass {
  GtkButtonClass parent_class;
  gboolean (*activate_link)(GtkLinkButton *button);
  void (*_gtk_padding1)();
  void (*_gtk_padding2)();
  void (*_gtk_padding3)();
  void (*_gtk_padding4)();
};

typedef struct _GtkLinkButtonPrivate GtkLinkButtonPrivate;

typedef struct _GtkListBoxAccessibleClass GtkListBoxAccessibleClass;

struct _GtkListBoxAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkListBoxAccessiblePrivate GtkListBoxAccessiblePrivate;

typedef struct _GtkListBoxClass GtkListBoxClass;

struct _GtkListBoxClass {
  GtkContainerClass parent_class;
  void (*row_selected)(GtkListBox *box, GtkListBoxRow *row);
  void (*row_activated)(GtkListBox *box, GtkListBoxRow *row);
  void (*activate_cursor_row)(GtkListBox *box);
  void (*toggle_cursor_row)(GtkListBox *box);
  void (*move_cursor)(GtkListBox *box, GtkMovementStep step, gint count);
  void (*selected_rows_changed)(GtkListBox *box);
  void (*select_all)(GtkListBox *box);
  void (*unselect_all)(GtkListBox *box);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
};
GtkWidget *(*ListBoxCreateWidgetFunc)(gpointer item, gpointer user_data);
gboolean (*ListBoxFilterFunc)(GtkListBoxRow *row, gpointer user_data);
void (*ListBoxForeachFunc)(GtkListBox *box, GtkListBoxRow *row,
                           gpointer user_data);

typedef struct _GtkListBoxRowAccessibleClass GtkListBoxRowAccessibleClass;

struct _GtkListBoxRowAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkListBoxRowClass GtkListBoxRowClass;

struct _GtkListBoxRowClass {
  GtkBinClass parent_class;
  void (*activate)(GtkListBoxRow *row);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
};
gint (*ListBoxSortFunc)(GtkListBoxRow *row1, GtkListBoxRow *row2,
                        gpointer user_data);
void (*ListBoxUpdateHeaderFunc)(GtkListBoxRow *row, GtkListBoxRow *before,
                                gpointer user_data);

typedef struct _GtkListStoreClass GtkListStoreClass;

struct _GtkListStoreClass {
  GObjectClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkListStorePrivate GtkListStorePrivate;

typedef struct _GtkLockButtonAccessibleClass GtkLockButtonAccessibleClass;

struct _GtkLockButtonAccessibleClass {
  GtkButtonAccessibleClass parent_class;
};

typedef struct _GtkLockButtonAccessiblePrivate GtkLockButtonAccessiblePrivate;

typedef struct _GtkLockButtonClass GtkLockButtonClass;

struct _GtkLockButtonClass {
  GtkButtonClass parent_class;
  void (*reserved0)();
  void (*reserved1)();
  void (*reserved2)();
  void (*reserved3)();
  void (*reserved4)();
  void (*reserved5)();
  void (*reserved6)();
  void (*reserved7)();
};

typedef struct _GtkLockButtonPrivate GtkLockButtonPrivate;

const gint GTK_MAJOR_VERSION = 3;

const gint GTK_MAX_COMPOSE_LEN = 7;

const gint GTK_MICRO_VERSION = 30;

const gint GTK_MINOR_VERSION = 22;

typedef struct _GtkMenuAccessibleClass GtkMenuAccessibleClass;

struct _GtkMenuAccessibleClass {
  GtkMenuShellAccessibleClass parent_class;
};

typedef struct _GtkMenuAccessiblePrivate GtkMenuAccessiblePrivate;

typedef struct _GtkMenuBarClass GtkMenuBarClass;

struct _GtkMenuBarClass {
  GtkMenuShellClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkMenuBarPrivate GtkMenuBarPrivate;

typedef struct _GtkMenuButtonAccessibleClass GtkMenuButtonAccessibleClass;

struct _GtkMenuButtonAccessibleClass {
  GtkToggleButtonAccessibleClass parent_class;
};

typedef struct _GtkMenuButtonAccessiblePrivate GtkMenuButtonAccessiblePrivate;

typedef struct _GtkMenuButtonClass GtkMenuButtonClass;

struct _GtkMenuButtonClass {
  GtkToggleButtonClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkMenuButtonPrivate GtkMenuButtonPrivate;

typedef struct _GtkMenuClass GtkMenuClass;

struct _GtkMenuClass {
  GtkMenuShellClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};
void (*MenuDetachFunc)(GtkWidget *attach_widget, GtkMenu *menu);

enum GtkMenuDirectionType {
  GTK_MENU_DIR_PARENT = 0,
  GTK_MENU_DIR_CHILD = 1,
  GTK_MENU_DIR_NEXT = 2,
  GTK_MENU_DIR_PREV = 3
};

typedef struct _GtkMenuItemAccessibleClass GtkMenuItemAccessibleClass;

struct _GtkMenuItemAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkMenuItemAccessiblePrivate GtkMenuItemAccessiblePrivate;

typedef struct _GtkMenuItemClass GtkMenuItemClass;

struct _GtkMenuItemClass {
  GtkBinClass parent_class;
  guint hide_on_activate;
  void (*activate)(GtkMenuItem *menu_item);
  void (*activate_item)(GtkMenuItem *menu_item);
  void (*toggle_size_request)(GtkMenuItem *menu_item, gint *requisition);
  void (*toggle_size_allocate)(GtkMenuItem *menu_item, gint allocation);
  void (*set_label)(GtkMenuItem *menu_item, const gchar *label);
  const gchar *(*get_label)(GtkMenuItem *menu_item);
  void (*select)(GtkMenuItem *menu_item);
  void (*deselect)(GtkMenuItem *menu_item);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkMenuItemPrivate GtkMenuItemPrivate;
void (*MenuPositionFunc)(GtkMenu *menu, gint *x, gint *y, gboolean *push_in,
                         gpointer user_data);

typedef struct _GtkMenuPrivate GtkMenuPrivate;

typedef struct _GtkMenuShellAccessibleClass GtkMenuShellAccessibleClass;

struct _GtkMenuShellAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkMenuShellAccessiblePrivate GtkMenuShellAccessiblePrivate;

typedef struct _GtkMenuShellClass GtkMenuShellClass;

struct _GtkMenuShellClass {
  GtkContainerClass parent_class;
  guint submenu_placement;
  void (*deactivate)(GtkMenuShell *menu_shell);
  void (*selection_done)(GtkMenuShell *menu_shell);
  void (*move_current)(GtkMenuShell *menu_shell,
                       GtkMenuDirectionType direction);
  void (*activate_current)(GtkMenuShell *menu_shell, gboolean force_hide);
  void (*cancel)(GtkMenuShell *menu_shell);
  void (*select_item)(GtkMenuShell *menu_shell, GtkWidget *menu_item);
  void (*insert)(GtkMenuShell *menu_shell, GtkWidget *child, gint position);
  gint (*get_popup_delay)(GtkMenuShell *menu_shell);
  gboolean (*move_selected)(GtkMenuShell *menu_shell, gint distance);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkMenuShellPrivate GtkMenuShellPrivate;

typedef struct _GtkMenuToolButtonClass GtkMenuToolButtonClass;

struct _GtkMenuToolButtonClass {
  GtkToolButtonClass parent_class;
  void (*show_menu)(GtkMenuToolButton *button);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkMenuToolButtonPrivate GtkMenuToolButtonPrivate;

typedef struct _GtkMessageDialogClass GtkMessageDialogClass;

struct _GtkMessageDialogClass {
  GtkDialogClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkMessageDialogPrivate GtkMessageDialogPrivate;

enum GtkMessageType {
  GTK_MESSAGE_INFO = 0,
  GTK_MESSAGE_WARNING = 1,
  GTK_MESSAGE_QUESTION = 2,
  GTK_MESSAGE_ERROR = 3,
  GTK_MESSAGE_OTHER = 4
};

typedef struct _GtkMiscClass GtkMiscClass;

struct _GtkMiscClass {
  GtkWidgetClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkMiscPrivate GtkMiscPrivate;
void (*ModuleDisplayInitFunc)(GdkDisplay *display);
void (*ModuleInitFunc)(gint *argc, argv);

typedef struct _GtkMountOperationClass GtkMountOperationClass;

struct _GtkMountOperationClass {
  GMountOperationClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkMountOperationPrivate GtkMountOperationPrivate;

enum GtkMovementStep {
  GTK_MOVEMENT_LOGICAL_POSITIONS = 0,
  GTK_MOVEMENT_VISUAL_POSITIONS = 1,
  GTK_MOVEMENT_WORDS = 2,
  GTK_MOVEMENT_DISPLAY_LINES = 3,
  GTK_MOVEMENT_DISPLAY_LINE_ENDS = 4,
  GTK_MOVEMENT_PARAGRAPHS = 5,
  GTK_MOVEMENT_PARAGRAPH_ENDS = 6,
  GTK_MOVEMENT_PAGES = 7,
  GTK_MOVEMENT_BUFFER_ENDS = 8,
  GTK_MOVEMENT_HORIZONTAL_PAGES = 9
};

typedef struct _GtkNativeDialogClass GtkNativeDialogClass;

struct _GtkNativeDialogClass {
  GObjectClass parent_class;
  void (*response)(GtkNativeDialog *self, gint response_id);
  void (*show)(GtkNativeDialog *self);
  void (*hide)(GtkNativeDialog *self);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkNotebookAccessibleClass GtkNotebookAccessibleClass;

struct _GtkNotebookAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkNotebookAccessiblePrivate GtkNotebookAccessiblePrivate;

typedef struct _GtkNotebookClass GtkNotebookClass;

struct _GtkNotebookClass {
  GtkContainerClass parent_class;
  void (*switch_page)(GtkNotebook *notebook, GtkWidget *page, guint page_num);
  gboolean (*select_page)(GtkNotebook *notebook, gboolean move_focus);
  gboolean (*focus_tab)(GtkNotebook *notebook, GtkNotebookTab type);
  gboolean (*change_current_page)(GtkNotebook *notebook, gint offset);
  void (*move_focus_out)(GtkNotebook *notebook, GtkDirectionType direction);
  gboolean (*reorder_tab)(GtkNotebook *notebook, GtkDirectionType direction,
                          gboolean move_to_last);
  gint (*insert_page)(GtkNotebook *notebook, GtkWidget *child,
                      GtkWidget *tab_label, GtkWidget *menu_label,
                      gint position);
  GtkNotebook *(*create_window)(GtkNotebook *notebook, GtkWidget *page, gint x,
                                gint y);
  void (*page_reordered)(GtkNotebook *notebook, GtkWidget *child,
                         guint page_num);
  void (*page_removed)(GtkNotebook *notebook, GtkWidget *child, guint page_num);
  void (*page_added)(GtkNotebook *notebook, GtkWidget *child, guint page_num);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};

typedef struct _GtkNotebookPageAccessibleClass GtkNotebookPageAccessibleClass;

struct _GtkNotebookPageAccessibleClass {
  AtkObjectClass parent_class;
};

typedef struct _GtkNotebookPageAccessiblePrivate
    GtkNotebookPageAccessiblePrivate;

typedef struct _GtkNotebookPrivate GtkNotebookPrivate;

enum GtkNotebookTab { GTK_NOTEBOOK_TAB_FIRST = 0, GTK_NOTEBOOK_TAB_LAST = 1 };

enum GtkNumberUpLayout {
  GTK_NUMBER_UP_LAYOUT_LEFT_TO_RIGHT_TOP_TO_BOTTOM = 0,
  GTK_NUMBER_UP_LAYOUT_LEFT_TO_RIGHT_BOTTOM_TO_TOP = 1,
  GTK_NUMBER_UP_LAYOUT_RIGHT_TO_LEFT_TOP_TO_BOTTOM = 2,
  GTK_NUMBER_UP_LAYOUT_RIGHT_TO_LEFT_BOTTOM_TO_TOP = 3,
  GTK_NUMBER_UP_LAYOUT_TOP_TO_BOTTOM_LEFT_TO_RIGHT = 4,
  GTK_NUMBER_UP_LAYOUT_TOP_TO_BOTTOM_RIGHT_TO_LEFT = 5,
  GTK_NUMBER_UP_LAYOUT_BOTTOM_TO_TOP_LEFT_TO_RIGHT = 6,
  GTK_NUMBER_UP_LAYOUT_BOTTOM_TO_TOP_RIGHT_TO_LEFT = 7
};

typedef struct _GtkNumerableIconClass GtkNumerableIconClass;

struct _GtkNumerableIconClass {
  GEmblemedIconClass parent_class;
  gpointer padding[16];
};

typedef struct _GtkNumerableIconPrivate GtkNumerableIconPrivate;

typedef struct _GtkOffscreenWindowClass GtkOffscreenWindowClass;

struct _GtkOffscreenWindowClass {
  GtkWindowClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkOrientableIface GtkOrientableIface;

struct _GtkOrientableIface {
  GTypeInterface base_iface;
};

enum GtkOrientation {
  GTK_ORIENTATION_HORIZONTAL = 0,
  GTK_ORIENTATION_VERTICAL = 1
};

typedef struct _GtkOverlayClass GtkOverlayClass;

struct _GtkOverlayClass {
  GtkBinClass parent_class;
  gboolean (*get_child_position)(GtkOverlay *overlay, GtkWidget *widget,
                                 GtkAllocation *allocation);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};

typedef struct _GtkOverlayPrivate GtkOverlayPrivate;

const gchar *GTK_PAPER_NAME_A3 = "iso_a3";

const gchar *GTK_PAPER_NAME_A4 = "iso_a4";

const gchar *GTK_PAPER_NAME_A5 = "iso_a5";

const gchar *GTK_PAPER_NAME_B5 = "iso_b5";

const gchar *GTK_PAPER_NAME_EXECUTIVE = "na_executive";

const gchar *GTK_PAPER_NAME_LEGAL = "na_legal";

const gchar *GTK_PAPER_NAME_LETTER = "na_letter";

const gint GTK_PATH_PRIO_MASK = 15;

const gchar *GTK_PRINT_SETTINGS_COLLATE = "collate";

const gchar *GTK_PRINT_SETTINGS_DEFAULT_SOURCE = "default-source";

const gchar *GTK_PRINT_SETTINGS_DITHER = "dither";

const gchar *GTK_PRINT_SETTINGS_DUPLEX = "duplex";

const gchar *GTK_PRINT_SETTINGS_FINISHINGS = "finishings";

const gchar *GTK_PRINT_SETTINGS_MEDIA_TYPE = "media-type";

const gchar *GTK_PRINT_SETTINGS_NUMBER_UP = "number-up";

const gchar *GTK_PRINT_SETTINGS_NUMBER_UP_LAYOUT = "number-up-layout";

const gchar *GTK_PRINT_SETTINGS_N_COPIES = "n-copies";

const gchar *GTK_PRINT_SETTINGS_ORIENTATION = "orientation";

const gchar *GTK_PRINT_SETTINGS_OUTPUT_BASENAME = "output-basename";

const gchar *GTK_PRINT_SETTINGS_OUTPUT_BIN = "output-bin";

const gchar *GTK_PRINT_SETTINGS_OUTPUT_DIR = "output-dir";

const gchar *GTK_PRINT_SETTINGS_OUTPUT_FILE_FORMAT = "output-file-format";

const gchar *GTK_PRINT_SETTINGS_OUTPUT_URI = "output-uri";

const gchar *GTK_PRINT_SETTINGS_PAGE_RANGES = "page-ranges";

const gchar *GTK_PRINT_SETTINGS_PAGE_SET = "page-set";

const gchar *GTK_PRINT_SETTINGS_PAPER_FORMAT = "paper-format";

const gchar *GTK_PRINT_SETTINGS_PAPER_HEIGHT = "paper-height";

const gchar *GTK_PRINT_SETTINGS_PAPER_WIDTH = "paper-width";

const gchar *GTK_PRINT_SETTINGS_PRINTER = "printer";

const gchar *GTK_PRINT_SETTINGS_PRINTER_LPI = "printer-lpi";

const gchar *GTK_PRINT_SETTINGS_PRINT_PAGES = "print-pages";

const gchar *GTK_PRINT_SETTINGS_QUALITY = "quality";

const gchar *GTK_PRINT_SETTINGS_RESOLUTION = "resolution";

const gchar *GTK_PRINT_SETTINGS_RESOLUTION_X = "resolution-x";

const gchar *GTK_PRINT_SETTINGS_RESOLUTION_Y = "resolution-y";

const gchar *GTK_PRINT_SETTINGS_REVERSE = "reverse";

const gchar *GTK_PRINT_SETTINGS_SCALE = "scale";

const gchar *GTK_PRINT_SETTINGS_USE_COLOR = "use-color";

const gchar *GTK_PRINT_SETTINGS_WIN32_DRIVER_EXTRA = "win32-driver-extra";

const gchar *GTK_PRINT_SETTINGS_WIN32_DRIVER_VERSION = "win32-driver-version";

const gint GTK_PRIORITY_RESIZE = 10;

enum GtkPackDirection {
  GTK_PACK_DIRECTION_LTR = 0,
  GTK_PACK_DIRECTION_RTL = 1,
  GTK_PACK_DIRECTION_TTB = 2,
  GTK_PACK_DIRECTION_BTT = 3
};

enum GtkPackType { GTK_PACK_START = 0, GTK_PACK_END = 1 };

typedef struct _GtkPadActionEntry GtkPadActionEntry;

struct _GtkPadActionEntry {
  GtkPadActionType type;
  gint index;
  gint mode;
  gchar *label;
  gchar *action_name;
};

enum GtkPadActionType {
  GTK_PAD_ACTION_BUTTON = 0,
  GTK_PAD_ACTION_RING = 1,
  GTK_PAD_ACTION_STRIP = 2
};

typedef struct _GtkPadControllerClass GtkPadControllerClass;

enum GtkPageOrientation {
  GTK_PAGE_ORIENTATION_PORTRAIT = 0,
  GTK_PAGE_ORIENTATION_LANDSCAPE = 1,
  GTK_PAGE_ORIENTATION_REVERSE_PORTRAIT = 2,
  GTK_PAGE_ORIENTATION_REVERSE_LANDSCAPE = 3
};

typedef struct _GtkPageRange GtkPageRange;

struct _GtkPageRange {
  gint start;
  gint end;
};

enum GtkPageSet {
  GTK_PAGE_SET_ALL = 0,
  GTK_PAGE_SET_EVEN = 1,
  GTK_PAGE_SET_ODD = 2
};
void (*PageSetupDoneFunc)(GtkPageSetup *page_setup, gpointer data);

enum GtkPanDirection {
  GTK_PAN_DIRECTION_LEFT = 0,
  GTK_PAN_DIRECTION_RIGHT = 1,
  GTK_PAN_DIRECTION_UP = 2,
  GTK_PAN_DIRECTION_DOWN = 3
};

typedef struct _GtkPanedAccessibleClass GtkPanedAccessibleClass;

struct _GtkPanedAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkPanedAccessiblePrivate GtkPanedAccessiblePrivate;

typedef struct _GtkPanedClass GtkPanedClass;

struct _GtkPanedClass {
  GtkContainerClass parent_class;
  gboolean (*cycle_child_focus)(GtkPaned *paned, gboolean reverse);
  gboolean (*toggle_handle_focus)(GtkPaned *paned);
  gboolean (*move_handle)(GtkPaned *paned, GtkScrollType scroll);
  gboolean (*cycle_handle_focus)(GtkPaned *paned, gboolean reverse);
  gboolean (*accept_position)(GtkPaned *paned);
  gboolean (*cancel_position)(GtkPaned *paned);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkPanedPrivate GtkPanedPrivate;

typedef struct _GtkPaperSize GtkPaperSize;
GtkPaperSize *gtk_paper_size_copy(GtkPaperSize *other);
void gtk_paper_size_free(GtkPaperSize *size);
gdouble gtk_paper_size_get_default_bottom_margin(GtkPaperSize *size,
                                                 GtkUnit unit);
gdouble gtk_paper_size_get_default_left_margin(GtkPaperSize *size,
                                               GtkUnit unit);
gdouble gtk_paper_size_get_default_right_margin(GtkPaperSize *size,
                                                GtkUnit unit);
gdouble gtk_paper_size_get_default_top_margin(GtkPaperSize *size, GtkUnit unit);
const gchar *gtk_paper_size_get_display_name(GtkPaperSize *size);
gdouble gtk_paper_size_get_height(GtkPaperSize *size, GtkUnit unit);
const gchar *gtk_paper_size_get_name(GtkPaperSize *size);
const gchar *gtk_paper_size_get_ppd_name(GtkPaperSize *size);
gdouble gtk_paper_size_get_width(GtkPaperSize *size, GtkUnit unit);
gboolean gtk_paper_size_is_custom(GtkPaperSize *size);
gboolean gtk_paper_size_is_equal(GtkPaperSize *size1, GtkPaperSize *size2);
gboolean gtk_paper_size_is_ipp(GtkPaperSize *size);
void gtk_paper_size_set_size(GtkPaperSize *size, gdouble width, gdouble height,
                             GtkUnit unit);
GVariant *gtk_paper_size_to_gvariant(GtkPaperSize *paper_size);
void gtk_paper_size_to_key_file(GtkPaperSize *size, GKeyFile *key_file,
                                const gchar *group_name);

enum GtkPathPriorityType {
  GTK_PATH_PRIO_LOWEST = 0,
  GTK_PATH_PRIO_GTK = 4,
  GTK_PATH_PRIO_APPLICATION = 8,
  GTK_PATH_PRIO_THEME = 10,
  GTK_PATH_PRIO_RC = 12,
  GTK_PATH_PRIO_HIGHEST = 15
};

enum GtkPathType {
  GTK_PATH_WIDGET = 0,
  GTK_PATH_WIDGET_CLASS = 1,
  GTK_PATH_CLASS = 2
};

enum GtkPlacesOpenFlags {
  GTK_PLACES_OPEN_NORMAL = 1,
  GTK_PLACES_OPEN_NEW_TAB = 2,
  GTK_PLACES_OPEN_NEW_WINDOW = 4
};

typedef struct _GtkPlacesSidebarClass GtkPlacesSidebarClass;

typedef struct _GtkPlugClass GtkPlugClass;

struct _GtkPlugClass {
  GtkWindowClass parent_class;
  void (*embedded)(GtkPlug *plug);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkPlugPrivate GtkPlugPrivate;

enum GtkPolicyType {
  GTK_POLICY_ALWAYS = 0,
  GTK_POLICY_AUTOMATIC = 1,
  GTK_POLICY_NEVER = 2,
  GTK_POLICY_EXTERNAL = 3
};

typedef struct _GtkPopoverAccessibleClass GtkPopoverAccessibleClass;

struct _GtkPopoverAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkPopoverClass GtkPopoverClass;

struct _GtkPopoverClass {
  GtkBinClass parent_class;
  void (*closed)(GtkPopover *popover);
  gpointer reserved[10];
};

enum GtkPopoverConstraint {
  GTK_POPOVER_CONSTRAINT_NONE = 0,
  GTK_POPOVER_CONSTRAINT_WINDOW = 1
};

typedef struct _GtkPopoverMenuClass GtkPopoverMenuClass;

struct _GtkPopoverMenuClass {
  GtkPopoverClass parent_class;
  gpointer reserved[10];
};

typedef struct _GtkPopoverPrivate GtkPopoverPrivate;

enum GtkPositionType {
  GTK_POS_LEFT = 0,
  GTK_POS_RIGHT = 1,
  GTK_POS_TOP = 2,
  GTK_POS_BOTTOM = 3
};

enum GtkPrintDuplex {
  GTK_PRINT_DUPLEX_SIMPLEX = 0,
  GTK_PRINT_DUPLEX_HORIZONTAL = 1,
  GTK_PRINT_DUPLEX_VERTICAL = 2
};

enum GtkPrintError {
  GTK_PRINT_ERROR_GENERAL = 0,
  GTK_PRINT_ERROR_INTERNAL_ERROR = 1,
  GTK_PRINT_ERROR_NOMEM = 2,
  GTK_PRINT_ERROR_INVALID_FILE = 3
};

enum GtkPrintOperationAction {
  GTK_PRINT_OPERATION_ACTION_PRINT_DIALOG = 0,
  GTK_PRINT_OPERATION_ACTION_PRINT = 1,
  GTK_PRINT_OPERATION_ACTION_PREVIEW = 2,
  GTK_PRINT_OPERATION_ACTION_EXPORT = 3
};

typedef struct _GtkPrintOperationClass GtkPrintOperationClass;

struct _GtkPrintOperationClass {
  GObjectClass parent_class;
  void (*done)(GtkPrintOperation *operation, GtkPrintOperationResult result);
  void (*begin_print)(GtkPrintOperation *operation, GtkPrintContext *context);
  gboolean (*paginate)(GtkPrintOperation *operation, GtkPrintContext *context);
  void (*request_page_setup)(GtkPrintOperation *operation,
                             GtkPrintContext *context, gint page_nr,
                             GtkPageSetup *setup);
  void (*draw_page)(GtkPrintOperation *operation, GtkPrintContext *context,
                    gint page_nr);
  void (*end_print)(GtkPrintOperation *operation, GtkPrintContext *context);
  void (*status_changed)(GtkPrintOperation *operation);
  GtkWidget *(*create_custom_widget)(GtkPrintOperation *operation);
  void (*custom_widget_apply)(GtkPrintOperation *operation, GtkWidget *widget);
  gboolean (*preview)(GtkPrintOperation *operation,
                      GtkPrintOperationPreview *preview,
                      GtkPrintContext *context, GtkWindow *parent);
  void (*update_custom_widget)(GtkPrintOperation *operation, GtkWidget *widget,
                               GtkPageSetup *setup, GtkPrintSettings *settings);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};

typedef struct _GtkPrintOperationPreviewIface GtkPrintOperationPreviewIface;

struct _GtkPrintOperationPreviewIface {
  GTypeInterface g_iface;
  void (*ready)(GtkPrintOperationPreview *preview, GtkPrintContext *context);
  void (*got_page_size)(GtkPrintOperationPreview *preview,
                        GtkPrintContext *context, GtkPageSetup *page_setup);
  void (*render_page)(GtkPrintOperationPreview *preview, gint page_nr);
  gboolean (*is_selected)(GtkPrintOperationPreview *preview, gint page_nr);
  void (*end_preview)(GtkPrintOperationPreview *preview);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};

typedef struct _GtkPrintOperationPrivate GtkPrintOperationPrivate;

enum GtkPrintOperationResult {
  GTK_PRINT_OPERATION_RESULT_ERROR = 0,
  GTK_PRINT_OPERATION_RESULT_APPLY = 1,
  GTK_PRINT_OPERATION_RESULT_CANCEL = 2,
  GTK_PRINT_OPERATION_RESULT_IN_PROGRESS = 3
};

enum GtkPrintPages {
  GTK_PRINT_PAGES_ALL = 0,
  GTK_PRINT_PAGES_CURRENT = 1,
  GTK_PRINT_PAGES_RANGES = 2,
  GTK_PRINT_PAGES_SELECTION = 3
};

enum GtkPrintQuality {
  GTK_PRINT_QUALITY_LOW = 0,
  GTK_PRINT_QUALITY_NORMAL = 1,
  GTK_PRINT_QUALITY_HIGH = 2,
  GTK_PRINT_QUALITY_DRAFT = 3
};
void (*PrintSettingsFunc)(const gchar *key, const gchar *value,
                          gpointer user_data);

enum GtkPrintStatus {
  GTK_PRINT_STATUS_INITIAL = 0,
  GTK_PRINT_STATUS_PREPARING = 1,
  GTK_PRINT_STATUS_GENERATING_DATA = 2,
  GTK_PRINT_STATUS_SENDING_DATA = 3,
  GTK_PRINT_STATUS_PENDING = 4,
  GTK_PRINT_STATUS_PENDING_ISSUE = 5,
  GTK_PRINT_STATUS_PRINTING = 6,
  GTK_PRINT_STATUS_FINISHED = 7,
  GTK_PRINT_STATUS_FINISHED_ABORTED = 8
};

typedef struct _GtkProgressBarAccessibleClass GtkProgressBarAccessibleClass;

struct _GtkProgressBarAccessibleClass {
  GtkWidgetAccessibleClass parent_class;
};

typedef struct _GtkProgressBarAccessiblePrivate GtkProgressBarAccessiblePrivate;

typedef struct _GtkProgressBarClass GtkProgressBarClass;

struct _GtkProgressBarClass {
  GtkWidgetClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkProgressBarPrivate GtkProgressBarPrivate;

enum GtkPropagationPhase {
  GTK_PHASE_NONE = 0,
  GTK_PHASE_CAPTURE = 1,
  GTK_PHASE_BUBBLE = 2,
  GTK_PHASE_TARGET = 3
};

typedef struct _GtkRadioActionClass GtkRadioActionClass;

struct _GtkRadioActionClass {
  GtkToggleActionClass parent_class;
  void (*changed)(GtkRadioAction *action, GtkRadioAction *current);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkRadioActionEntry GtkRadioActionEntry;

struct _GtkRadioActionEntry {
  const gchar *name;
  const gchar *stock_id;
  const gchar *label;
  const gchar *accelerator;
  const gchar *tooltip;
  gint value;
};

typedef struct _GtkRadioActionPrivate GtkRadioActionPrivate;

typedef struct _GtkRadioButtonAccessibleClass GtkRadioButtonAccessibleClass;

struct _GtkRadioButtonAccessibleClass {
  GtkToggleButtonAccessibleClass parent_class;
};

typedef struct _GtkRadioButtonAccessiblePrivate GtkRadioButtonAccessiblePrivate;

typedef struct _GtkRadioButtonClass GtkRadioButtonClass;

struct _GtkRadioButtonClass {
  GtkCheckButtonClass parent_class;
  void (*group_changed)(GtkRadioButton *radio_button);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkRadioButtonPrivate GtkRadioButtonPrivate;

typedef struct _GtkRadioMenuItemAccessibleClass GtkRadioMenuItemAccessibleClass;

struct _GtkRadioMenuItemAccessibleClass {
  GtkCheckMenuItemAccessibleClass parent_class;
};

typedef struct _GtkRadioMenuItemAccessiblePrivate
    GtkRadioMenuItemAccessiblePrivate;

typedef struct _GtkRadioMenuItemClass GtkRadioMenuItemClass;

struct _GtkRadioMenuItemClass {
  GtkCheckMenuItemClass parent_class;
  void (*group_changed)(GtkRadioMenuItem *radio_menu_item);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkRadioMenuItemPrivate GtkRadioMenuItemPrivate;

typedef struct _GtkRadioToolButtonClass GtkRadioToolButtonClass;

struct _GtkRadioToolButtonClass {
  GtkToggleToolButtonClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkRangeAccessibleClass GtkRangeAccessibleClass;

struct _GtkRangeAccessibleClass {
  GtkWidgetAccessibleClass parent_class;
};

typedef struct _GtkRangeAccessiblePrivate GtkRangeAccessiblePrivate;

typedef struct _GtkRangeClass GtkRangeClass;

struct _GtkRangeClass {
  GtkWidgetClass parent_class;
  gchar *slider_detail;
  gchar *stepper_detail;
  void (*value_changed)(GtkRange *range);
  void (*adjust_bounds)(GtkRange *range, gdouble new_value);
  void (*move_slider)(GtkRange *range, GtkScrollType scroll);
  void (*get_range_border)(GtkRange *range, GtkBorder *border_);
  gboolean (*change_value)(GtkRange *range, GtkScrollType scroll,
                           gdouble new_value);
  void (*get_range_size_request)(GtkRange *range, GtkOrientation orientation,
                                 gint *minimum, gint *natural);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
};

typedef struct _GtkRangePrivate GtkRangePrivate;

typedef struct _GtkRcContext GtkRcContext;

enum GtkRcFlags {
  GTK_RC_FG = 1,
  GTK_RC_BG = 2,
  GTK_RC_TEXT = 4,
  GTK_RC_BASE = 8
};

typedef struct _GtkRcProperty GtkRcProperty;

struct _GtkRcProperty {
  GQuark type_name;
  GQuark property_name;
  gchar *origin;
  GValue value;
};
gboolean (*RcPropertyParser)(const GParamSpec *pspec, const GString *rc_string,
                             GValue *property_value);

typedef struct _GtkRcStyleClass GtkRcStyleClass;

struct _GtkRcStyleClass {
  GObjectClass parent_class;
  GtkRcStyle *(*create_rc_style)(GtkRcStyle *rc_style);
  guint (*parse)(GtkRcStyle *rc_style, GtkSettings *settings,
                 GScanner *scanner);
  void (*merge)(GtkRcStyle *dest, GtkRcStyle *src);
  GtkStyle *(*create_style)(GtkRcStyle *rc_style);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkRcTokenType {
  GTK_RC_TOKEN_INVALID = 270,
  GTK_RC_TOKEN_INCLUDE = 271,
  GTK_RC_TOKEN_NORMAL = 272,
  GTK_RC_TOKEN_ACTIVE = 273,
  GTK_RC_TOKEN_PRELIGHT = 274,
  GTK_RC_TOKEN_SELECTED = 275,
  GTK_RC_TOKEN_INSENSITIVE = 276,
  GTK_RC_TOKEN_FG = 277,
  GTK_RC_TOKEN_BG = 278,
  GTK_RC_TOKEN_TEXT = 279,
  GTK_RC_TOKEN_BASE = 280,
  GTK_RC_TOKEN_XTHICKNESS = 281,
  GTK_RC_TOKEN_YTHICKNESS = 282,
  GTK_RC_TOKEN_FONT = 283,
  GTK_RC_TOKEN_FONTSET = 284,
  GTK_RC_TOKEN_FONT_NAME = 285,
  GTK_RC_TOKEN_BG_PIXMAP = 286,
  GTK_RC_TOKEN_PIXMAP_PATH = 287,
  GTK_RC_TOKEN_STYLE = 288,
  GTK_RC_TOKEN_BINDING = 289,
  GTK_RC_TOKEN_BIND = 290,
  GTK_RC_TOKEN_WIDGET = 291,
  GTK_RC_TOKEN_WIDGET_CLASS = 292,
  GTK_RC_TOKEN_CLASS = 293,
  GTK_RC_TOKEN_LOWEST = 294,
  GTK_RC_TOKEN_GTK = 295,
  GTK_RC_TOKEN_APPLICATION = 296,
  GTK_RC_TOKEN_THEME = 297,
  GTK_RC_TOKEN_RC = 298,
  GTK_RC_TOKEN_HIGHEST = 299,
  GTK_RC_TOKEN_ENGINE = 300,
  GTK_RC_TOKEN_MODULE_PATH = 301,
  GTK_RC_TOKEN_IM_MODULE_PATH = 302,
  GTK_RC_TOKEN_IM_MODULE_FILE = 303,
  GTK_RC_TOKEN_STOCK = 304,
  GTK_RC_TOKEN_LTR = 305,
  GTK_RC_TOKEN_RTL = 306,
  GTK_RC_TOKEN_COLOR = 307,
  GTK_RC_TOKEN_UNBIND = 308,
  GTK_RC_TOKEN_LAST = 309
};

typedef struct _GtkRecentActionClass GtkRecentActionClass;

struct _GtkRecentActionClass {
  GtkActionClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkRecentActionPrivate GtkRecentActionPrivate;

typedef struct _GtkRecentChooserDialogClass GtkRecentChooserDialogClass;

struct _GtkRecentChooserDialogClass {
  GtkDialogClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkRecentChooserDialogPrivate GtkRecentChooserDialogPrivate;

enum GtkRecentChooserError {
  GTK_RECENT_CHOOSER_ERROR_NOT_FOUND = 0,
  GTK_RECENT_CHOOSER_ERROR_INVALID_URI = 1
};

typedef struct _GtkRecentChooserIface GtkRecentChooserIface;

struct _GtkRecentChooserIface {
  GTypeInterface base_iface;
  gboolean (*set_current_uri)(GtkRecentChooser *chooser, const gchar *uri);
  gchar *(*get_current_uri)(GtkRecentChooser *chooser);
  gboolean (*select_uri)(GtkRecentChooser *chooser, const gchar *uri);
  void (*unselect_uri)(GtkRecentChooser *chooser, const gchar *uri);
  void (*select_all)(GtkRecentChooser *chooser);
  void (*unselect_all)(GtkRecentChooser *chooser);
  GList *(*get_items)(GtkRecentChooser *chooser);
  GtkRecentManager *(*get_recent_manager)(GtkRecentChooser *chooser);
  void (*add_filter)(GtkRecentChooser *chooser, GtkRecentFilter *filter);
  void (*remove_filter)(GtkRecentChooser *chooser, GtkRecentFilter *filter);
  GSList *(*list_filters)(GtkRecentChooser *chooser);
  void (*set_sort_func)(GtkRecentChooser *chooser, GtkRecentSortFunc sort_func,
                        gpointer sort_data, GDestroyNotify data_destroy);
  void (*item_activated)(GtkRecentChooser *chooser);
  void (*selection_changed)(GtkRecentChooser *chooser);
};

typedef struct _GtkRecentChooserMenuClass GtkRecentChooserMenuClass;

struct _GtkRecentChooserMenuClass {
  GtkMenuClass parent_class;
  void (*gtk_recent1)();
  void (*gtk_recent2)();
  void (*gtk_recent3)();
  void (*gtk_recent4)();
};

typedef struct _GtkRecentChooserMenuPrivate GtkRecentChooserMenuPrivate;

typedef struct _GtkRecentChooserWidgetClass GtkRecentChooserWidgetClass;

struct _GtkRecentChooserWidgetClass {
  GtkBoxClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkRecentChooserWidgetPrivate GtkRecentChooserWidgetPrivate;

typedef struct _GtkRecentData GtkRecentData;

struct _GtkRecentData {
  gchar *display_name;
  gchar *description;
  gchar *mime_type;
  gchar *app_name;
  gchar *app_exec;
  gchar *groups[];
  gboolean is_private;
};

enum GtkRecentFilterFlags {
  GTK_RECENT_FILTER_URI = 1,
  GTK_RECENT_FILTER_DISPLAY_NAME = 2,
  GTK_RECENT_FILTER_MIME_TYPE = 4,
  GTK_RECENT_FILTER_APPLICATION = 8,
  GTK_RECENT_FILTER_GROUP = 16,
  GTK_RECENT_FILTER_AGE = 32
};
gboolean (*RecentFilterFunc)(const GtkRecentFilterInfo *filter_info,
                             gpointer user_data);

typedef struct _GtkRecentFilterInfo GtkRecentFilterInfo;

struct _GtkRecentFilterInfo {
  GtkRecentFilterFlags contains;
  const gchar *uri;
  const gchar *display_name;
  const gchar *mime_type;
  gchar *applications[];
  gchar *groups[];
  gint age;
};

typedef struct _GtkRecentInfo GtkRecentInfo;
GAppInfo *gtk_recent_info_create_app_info(GtkRecentInfo *info,
                                          const gchar *app_name);
gboolean gtk_recent_info_exists(GtkRecentInfo *info);
time_t gtk_recent_info_get_added(GtkRecentInfo *info);
gint gtk_recent_info_get_age(GtkRecentInfo *info);
gboolean gtk_recent_info_get_application_info(GtkRecentInfo *info,
                                              const gchar *app_name,
                                              const gchar **app_exec,
                                              guint *count, time_t *time_);
gtk_recent_info_get_applications(GtkRecentInfo *info, gsize *length);
const gchar *gtk_recent_info_get_description(GtkRecentInfo *info);
const gchar *gtk_recent_info_get_display_name(GtkRecentInfo *info);
GIcon *gtk_recent_info_get_gicon(GtkRecentInfo *info);
gtk_recent_info_get_groups(GtkRecentInfo *info, gsize *length);
GdkPixbuf *gtk_recent_info_get_icon(GtkRecentInfo *info, gint size);
const gchar *gtk_recent_info_get_mime_type(GtkRecentInfo *info);
time_t gtk_recent_info_get_modified(GtkRecentInfo *info);
gboolean gtk_recent_info_get_private_hint(GtkRecentInfo *info);
gchar *gtk_recent_info_get_short_name(GtkRecentInfo *info);
const gchar *gtk_recent_info_get_uri(GtkRecentInfo *info);
gchar *gtk_recent_info_get_uri_display(GtkRecentInfo *info);
time_t gtk_recent_info_get_visited(GtkRecentInfo *info);
gboolean gtk_recent_info_has_application(GtkRecentInfo *info,
                                         const gchar *app_name);
gboolean gtk_recent_info_has_group(GtkRecentInfo *info,
                                   const gchar *group_name);
gboolean gtk_recent_info_is_local(GtkRecentInfo *info);
gchar *gtk_recent_info_last_application(GtkRecentInfo *info);
gboolean gtk_recent_info_match(GtkRecentInfo *info_a, GtkRecentInfo *info_b);
GtkRecentInfo *gtk_recent_info_ref(GtkRecentInfo *info);
void gtk_recent_info_unref(GtkRecentInfo *info);

typedef struct _GtkRecentManagerClass GtkRecentManagerClass;

struct _GtkRecentManagerClass {
  GObjectClass parent_class;
  void (*changed)(GtkRecentManager *manager);
  void (*_gtk_recent1)();
  void (*_gtk_recent2)();
  void (*_gtk_recent3)();
  void (*_gtk_recent4)();
};

enum GtkRecentManagerError {
  GTK_RECENT_MANAGER_ERROR_NOT_FOUND = 0,
  GTK_RECENT_MANAGER_ERROR_INVALID_URI = 1,
  GTK_RECENT_MANAGER_ERROR_INVALID_ENCODING = 2,
  GTK_RECENT_MANAGER_ERROR_NOT_REGISTERED = 3,
  GTK_RECENT_MANAGER_ERROR_READ = 4,
  GTK_RECENT_MANAGER_ERROR_WRITE = 5,
  GTK_RECENT_MANAGER_ERROR_UNKNOWN = 6
};

typedef struct _GtkRecentManagerPrivate GtkRecentManagerPrivate;
gint (*RecentSortFunc)(GtkRecentInfo *a, GtkRecentInfo *b, gpointer user_data);

enum GtkRecentSortType {
  GTK_RECENT_SORT_NONE = 0,
  GTK_RECENT_SORT_MRU = 1,
  GTK_RECENT_SORT_LRU = 2,
  GTK_RECENT_SORT_CUSTOM = 3
};

enum GtkRegionFlags {
  GTK_REGION_EVEN = 1,
  GTK_REGION_ODD = 2,
  GTK_REGION_FIRST = 4,
  GTK_REGION_LAST = 8,
  GTK_REGION_ONLY = 16,
  GTK_REGION_SORTED = 32
};

enum GtkReliefStyle {
  GTK_RELIEF_NORMAL = 0,
  GTK_RELIEF_HALF = 1,
  GTK_RELIEF_NONE = 2
};

typedef struct _GtkRendererCellAccessibleClass GtkRendererCellAccessibleClass;

struct _GtkRendererCellAccessibleClass {
  GtkCellAccessibleClass parent_class;
};

typedef struct _GtkRendererCellAccessiblePrivate
    GtkRendererCellAccessiblePrivate;

typedef struct _GtkRequestedSize GtkRequestedSize;

struct _GtkRequestedSize {
  gpointer data;
  gint minimum_size;
  gint natural_size;
};

typedef struct _GtkRequisition GtkRequisition;

struct _GtkRequisition {
  gint width;
  gint height;
};
GtkRequisition *gtk_requisition_copy(const GtkRequisition *requisition);
void gtk_requisition_free(GtkRequisition *requisition);

enum GtkResizeMode {
  GTK_RESIZE_PARENT = 0,
  GTK_RESIZE_QUEUE = 1,
  GTK_RESIZE_IMMEDIATE = 2
};

enum GtkResponseType {
  GTK_RESPONSE_NONE = -1,
  GTK_RESPONSE_REJECT = -2,
  GTK_RESPONSE_ACCEPT = -3,
  GTK_RESPONSE_DELETE_EVENT = -4,
  GTK_RESPONSE_OK = -5,
  GTK_RESPONSE_CANCEL = -6,
  GTK_RESPONSE_CLOSE = -7,
  GTK_RESPONSE_YES = -8,
  GTK_RESPONSE_NO = -9,
  GTK_RESPONSE_APPLY = -10,
  GTK_RESPONSE_HELP = -11
};

typedef struct _GtkRevealerClass GtkRevealerClass;

struct _GtkRevealerClass {
  GtkBinClass parent_class;
};

enum GtkRevealerTransitionType {
  GTK_REVEALER_TRANSITION_TYPE_NONE = 0,
  GTK_REVEALER_TRANSITION_TYPE_CROSSFADE = 1,
  GTK_REVEALER_TRANSITION_TYPE_SLIDE_RIGHT = 2,
  GTK_REVEALER_TRANSITION_TYPE_SLIDE_LEFT = 3,
  GTK_REVEALER_TRANSITION_TYPE_SLIDE_UP = 4,
  GTK_REVEALER_TRANSITION_TYPE_SLIDE_DOWN = 5
};

const gchar *GTK_STOCK_ABOUT = "gtk-about";

const gchar *GTK_STOCK_ADD = "gtk-add";

const gchar *GTK_STOCK_APPLY = "gtk-apply";

const gchar *GTK_STOCK_BOLD = "gtk-bold";

const gchar *GTK_STOCK_CANCEL = "gtk-cancel";

const gchar *GTK_STOCK_CAPS_LOCK_WARNING = "gtk-caps-lock-warning";

const gchar *GTK_STOCK_CDROM = "gtk-cdrom";

const gchar *GTK_STOCK_CLEAR = "gtk-clear";

const gchar *GTK_STOCK_CLOSE = "gtk-close";

const gchar *GTK_STOCK_COLOR_PICKER = "gtk-color-picker";

const gchar *GTK_STOCK_CONNECT = "gtk-connect";

const gchar *GTK_STOCK_CONVERT = "gtk-convert";

const gchar *GTK_STOCK_COPY = "gtk-copy";

const gchar *GTK_STOCK_CUT = "gtk-cut";

const gchar *GTK_STOCK_DELETE = "gtk-delete";

const gchar *GTK_STOCK_DIALOG_AUTHENTICATION = "gtk-dialog-authentication";

const gchar *GTK_STOCK_DIALOG_ERROR = "gtk-dialog-error";

const gchar *GTK_STOCK_DIALOG_INFO = "gtk-dialog-info";

const gchar *GTK_STOCK_DIALOG_QUESTION = "gtk-dialog-question";

const gchar *GTK_STOCK_DIALOG_WARNING = "gtk-dialog-warning";

const gchar *GTK_STOCK_DIRECTORY = "gtk-directory";

const gchar *GTK_STOCK_DISCARD = "gtk-discard";

const gchar *GTK_STOCK_DISCONNECT = "gtk-disconnect";

const gchar *GTK_STOCK_DND = "gtk-dnd";

const gchar *GTK_STOCK_DND_MULTIPLE = "gtk-dnd-multiple";

const gchar *GTK_STOCK_EDIT = "gtk-edit";

const gchar *GTK_STOCK_EXECUTE = "gtk-execute";

const gchar *GTK_STOCK_FILE = "gtk-file";

const gchar *GTK_STOCK_FIND = "gtk-find";

const gchar *GTK_STOCK_FIND_AND_REPLACE = "gtk-find-and-replace";

const gchar *GTK_STOCK_FLOPPY = "gtk-floppy";

const gchar *GTK_STOCK_FULLSCREEN = "gtk-fullscreen";

const gchar *GTK_STOCK_GOTO_BOTTOM = "gtk-goto-bottom";

const gchar *GTK_STOCK_GOTO_FIRST = "gtk-goto-first";

const gchar *GTK_STOCK_GOTO_LAST = "gtk-goto-last";

const gchar *GTK_STOCK_GOTO_TOP = "gtk-goto-top";

const gchar *GTK_STOCK_GO_BACK = "gtk-go-back";

const gchar *GTK_STOCK_GO_DOWN = "gtk-go-down";

const gchar *GTK_STOCK_GO_FORWARD = "gtk-go-forward";

const gchar *GTK_STOCK_GO_UP = "gtk-go-up";

const gchar *GTK_STOCK_HARDDISK = "gtk-harddisk";

const gchar *GTK_STOCK_HELP = "gtk-help";

const gchar *GTK_STOCK_HOME = "gtk-home";

const gchar *GTK_STOCK_INDENT = "gtk-indent";

const gchar *GTK_STOCK_INDEX = "gtk-index";

const gchar *GTK_STOCK_INFO = "gtk-info";

const gchar *GTK_STOCK_ITALIC = "gtk-italic";

const gchar *GTK_STOCK_JUMP_TO = "gtk-jump-to";

const gchar *GTK_STOCK_JUSTIFY_CENTER = "gtk-justify-center";

const gchar *GTK_STOCK_JUSTIFY_FILL = "gtk-justify-fill";

const gchar *GTK_STOCK_JUSTIFY_LEFT = "gtk-justify-left";

const gchar *GTK_STOCK_JUSTIFY_RIGHT = "gtk-justify-right";

const gchar *GTK_STOCK_LEAVE_FULLSCREEN = "gtk-leave-fullscreen";

const gchar *GTK_STOCK_MEDIA_FORWARD = "gtk-media-forward";

const gchar *GTK_STOCK_MEDIA_NEXT = "gtk-media-next";

const gchar *GTK_STOCK_MEDIA_PAUSE = "gtk-media-pause";

const gchar *GTK_STOCK_MEDIA_PLAY = "gtk-media-play";

const gchar *GTK_STOCK_MEDIA_PREVIOUS = "gtk-media-previous";

const gchar *GTK_STOCK_MEDIA_RECORD = "gtk-media-record";

const gchar *GTK_STOCK_MEDIA_REWIND = "gtk-media-rewind";

const gchar *GTK_STOCK_MEDIA_STOP = "gtk-media-stop";

const gchar *GTK_STOCK_MISSING_IMAGE = "gtk-missing-image";

const gchar *GTK_STOCK_NETWORK = "gtk-network";

const gchar *GTK_STOCK_NEW = "gtk-new";

const gchar *GTK_STOCK_NO = "gtk-no";

const gchar *GTK_STOCK_OK = "gtk-ok";

const gchar *GTK_STOCK_OPEN = "gtk-open";

const gchar *GTK_STOCK_ORIENTATION_LANDSCAPE = "gtk-orientation-landscape";

const gchar *GTK_STOCK_ORIENTATION_PORTRAIT = "gtk-orientation-portrait";

const gchar *GTK_STOCK_ORIENTATION_REVERSE_LANDSCAPE =
    "gtk-orientation-reverse-landscape";

const gchar *GTK_STOCK_ORIENTATION_REVERSE_PORTRAIT =
    "gtk-orientation-reverse-portrait";

const gchar *GTK_STOCK_PAGE_SETUP = "gtk-page-setup";

const gchar *GTK_STOCK_PASTE = "gtk-paste";

const gchar *GTK_STOCK_PREFERENCES = "gtk-preferences";

const gchar *GTK_STOCK_PRINT = "gtk-print";

const gchar *GTK_STOCK_PRINT_ERROR = "gtk-print-error";

const gchar *GTK_STOCK_PRINT_PAUSED = "gtk-print-paused";

const gchar *GTK_STOCK_PRINT_PREVIEW = "gtk-print-preview";

const gchar *GTK_STOCK_PRINT_REPORT = "gtk-print-report";

const gchar *GTK_STOCK_PRINT_WARNING = "gtk-print-warning";

const gchar *GTK_STOCK_PROPERTIES = "gtk-properties";

const gchar *GTK_STOCK_QUIT = "gtk-quit";

const gchar *GTK_STOCK_REDO = "gtk-redo";

const gchar *GTK_STOCK_REFRESH = "gtk-refresh";

const gchar *GTK_STOCK_REMOVE = "gtk-remove";

const gchar *GTK_STOCK_REVERT_TO_SAVED = "gtk-revert-to-saved";

const gchar *GTK_STOCK_SAVE = "gtk-save";

const gchar *GTK_STOCK_SAVE_AS = "gtk-save-as";

const gchar *GTK_STOCK_SELECT_ALL = "gtk-select-all";

const gchar *GTK_STOCK_SELECT_COLOR = "gtk-select-color";

const gchar *GTK_STOCK_SELECT_FONT = "gtk-select-font";

const gchar *GTK_STOCK_SORT_ASCENDING = "gtk-sort-ascending";

const gchar *GTK_STOCK_SORT_DESCENDING = "gtk-sort-descending";

const gchar *GTK_STOCK_SPELL_CHECK = "gtk-spell-check";

const gchar *GTK_STOCK_STOP = "gtk-stop";

const gchar *GTK_STOCK_STRIKETHROUGH = "gtk-strikethrough";

const gchar *GTK_STOCK_UNDELETE = "gtk-undelete";

const gchar *GTK_STOCK_UNDERLINE = "gtk-underline";

const gchar *GTK_STOCK_UNDO = "gtk-undo";

const gchar *GTK_STOCK_UNINDENT = "gtk-unindent";

const gchar *GTK_STOCK_YES = "gtk-yes";

const gchar *GTK_STOCK_ZOOM_100 = "gtk-zoom-100";

const gchar *GTK_STOCK_ZOOM_FIT = "gtk-zoom-fit";

const gchar *GTK_STOCK_ZOOM_IN = "gtk-zoom-in";

const gchar *GTK_STOCK_ZOOM_OUT = "gtk-zoom-out";

const gchar *GTK_STYLE_CLASS_ACCELERATOR = "accelerator";

const gchar *GTK_STYLE_CLASS_ARROW = "arrow";

const gchar *GTK_STYLE_CLASS_BACKGROUND = "background";

const gchar *GTK_STYLE_CLASS_BOTTOM = "bottom";

const gchar *GTK_STYLE_CLASS_BUTTON = "button";

const gchar *GTK_STYLE_CLASS_CALENDAR = "calendar";

const gchar *GTK_STYLE_CLASS_CELL = "cell";

const gchar *GTK_STYLE_CLASS_CHECK = "check";

const gchar *GTK_STYLE_CLASS_COMBOBOX_ENTRY = "combobox-entry";

const gchar *GTK_STYLE_CLASS_CONTEXT_MENU = "context-menu";

const gchar *GTK_STYLE_CLASS_CSD = "csd";

const gchar *GTK_STYLE_CLASS_CURSOR_HANDLE = "cursor-handle";

const gchar *GTK_STYLE_CLASS_DEFAULT = "default";

const gchar *GTK_STYLE_CLASS_DESTRUCTIVE_ACTION = "destructive-action";

const gchar *GTK_STYLE_CLASS_DIM_LABEL = "dim-label";

const gchar *GTK_STYLE_CLASS_DND = "dnd";

const gchar *GTK_STYLE_CLASS_DOCK = "dock";

const gchar *GTK_STYLE_CLASS_ENTRY = "entry";

const gchar *GTK_STYLE_CLASS_ERROR = "error";

const gchar *GTK_STYLE_CLASS_EXPANDER = "expander";

const gchar *GTK_STYLE_CLASS_FLAT = "flat";

const gchar *GTK_STYLE_CLASS_FRAME = "frame";

const gchar *GTK_STYLE_CLASS_GRIP = "grip";

const gchar *GTK_STYLE_CLASS_HEADER = "header";

const gchar *GTK_STYLE_CLASS_HIGHLIGHT = "highlight";

const gchar *GTK_STYLE_CLASS_HORIZONTAL = "horizontal";

const gchar *GTK_STYLE_CLASS_IMAGE = "image";

const gchar *GTK_STYLE_CLASS_INFO = "info";

const gchar *GTK_STYLE_CLASS_INLINE_TOOLBAR = "inline-toolbar";

const gchar *GTK_STYLE_CLASS_INSERTION_CURSOR = "insertion-cursor";

const gchar *GTK_STYLE_CLASS_LABEL = "label";

const gchar *GTK_STYLE_CLASS_LEFT = "left";

const gchar *GTK_STYLE_CLASS_LEVEL_BAR = "level-bar";

const gchar *GTK_STYLE_CLASS_LINKED = "linked";

const gchar *GTK_STYLE_CLASS_LIST = "list";

const gchar *GTK_STYLE_CLASS_LIST_ROW = "list-row";

const gchar *GTK_STYLE_CLASS_MARK = "mark";

const gchar *GTK_STYLE_CLASS_MENU = "menu";

const gchar *GTK_STYLE_CLASS_MENUBAR = "menubar";

const gchar *GTK_STYLE_CLASS_MENUITEM = "menuitem";

const gchar *GTK_STYLE_CLASS_MESSAGE_DIALOG = "message-dialog";

const gchar *GTK_STYLE_CLASS_MONOSPACE = "monospace";

const gchar *GTK_STYLE_CLASS_NEEDS_ATTENTION = "needs-attention";

const gchar *GTK_STYLE_CLASS_NOTEBOOK = "notebook";

const gchar *GTK_STYLE_CLASS_OSD = "osd";

const gchar *GTK_STYLE_CLASS_OVERSHOOT = "overshoot";

const gchar *GTK_STYLE_CLASS_PANE_SEPARATOR = "pane-separator";

const gchar *GTK_STYLE_CLASS_PAPER = "paper";

const gchar *GTK_STYLE_CLASS_POPOVER = "popover";

const gchar *GTK_STYLE_CLASS_POPUP = "popup";

const gchar *GTK_STYLE_CLASS_PRIMARY_TOOLBAR = "primary-toolbar";

const gchar *GTK_STYLE_CLASS_PROGRESSBAR = "progressbar";

const gchar *GTK_STYLE_CLASS_PULSE = "pulse";

const gchar *GTK_STYLE_CLASS_QUESTION = "question";

const gchar *GTK_STYLE_CLASS_RADIO = "radio";

const gchar *GTK_STYLE_CLASS_RAISED = "raised";

const gchar *GTK_STYLE_CLASS_READ_ONLY = "read-only";

const gchar *GTK_STYLE_CLASS_RIGHT = "right";

const gchar *GTK_STYLE_CLASS_RUBBERBAND = "rubberband";

const gchar *GTK_STYLE_CLASS_SCALE = "scale";

const gchar *GTK_STYLE_CLASS_SCALE_HAS_MARKS_ABOVE = "scale-has-marks-above";

const gchar *GTK_STYLE_CLASS_SCALE_HAS_MARKS_BELOW = "scale-has-marks-below";

const gchar *GTK_STYLE_CLASS_SCROLLBAR = "scrollbar";

const gchar *GTK_STYLE_CLASS_SCROLLBARS_JUNCTION = "scrollbars-junction";

const gchar *GTK_STYLE_CLASS_SEPARATOR = "separator";

const gchar *GTK_STYLE_CLASS_SIDEBAR = "sidebar";

const gchar *GTK_STYLE_CLASS_SLIDER = "slider";

const gchar *GTK_STYLE_CLASS_SPINBUTTON = "spinbutton";

const gchar *GTK_STYLE_CLASS_SPINNER = "spinner";

const gchar *GTK_STYLE_CLASS_STATUSBAR = "statusbar";

const gchar *GTK_STYLE_CLASS_SUBTITLE = "subtitle";

const gchar *GTK_STYLE_CLASS_SUGGESTED_ACTION = "suggested-action";

const gchar *GTK_STYLE_CLASS_TITLE = "title";

const gchar *GTK_STYLE_CLASS_TITLEBAR = "titlebar";

const gchar *GTK_STYLE_CLASS_TOOLBAR = "toolbar";

const gchar *GTK_STYLE_CLASS_TOOLTIP = "tooltip";

const gchar *GTK_STYLE_CLASS_TOP = "top";

const gchar *GTK_STYLE_CLASS_TOUCH_SELECTION = "touch-selection";

const gchar *GTK_STYLE_CLASS_TROUGH = "trough";

const gchar *GTK_STYLE_CLASS_UNDERSHOOT = "undershoot";

const gchar *GTK_STYLE_CLASS_VERTICAL = "vertical";

const gchar *GTK_STYLE_CLASS_VIEW = "view";

const gchar *GTK_STYLE_CLASS_WARNING = "warning";

const gchar *GTK_STYLE_CLASS_WIDE = "wide";

const gchar *GTK_STYLE_PROPERTY_BACKGROUND_COLOR = "background-color";

const gchar *GTK_STYLE_PROPERTY_BACKGROUND_IMAGE = "background-image";

const gchar *GTK_STYLE_PROPERTY_BORDER_COLOR = "border-color";

const gchar *GTK_STYLE_PROPERTY_BORDER_RADIUS = "border-radius";

const gchar *GTK_STYLE_PROPERTY_BORDER_STYLE = "border-style";

const gchar *GTK_STYLE_PROPERTY_BORDER_WIDTH = "border-width";

const gchar *GTK_STYLE_PROPERTY_COLOR = "color";

const gchar *GTK_STYLE_PROPERTY_FONT = "font";

const gchar *GTK_STYLE_PROPERTY_MARGIN = "margin";

const gchar *GTK_STYLE_PROPERTY_PADDING = "padding";

const gint GTK_STYLE_PROVIDER_PRIORITY_APPLICATION = 600;

const gint GTK_STYLE_PROVIDER_PRIORITY_FALLBACK = 1;

const gint GTK_STYLE_PROVIDER_PRIORITY_SETTINGS = 400;

const gint GTK_STYLE_PROVIDER_PRIORITY_THEME = 200;

const gint GTK_STYLE_PROVIDER_PRIORITY_USER = 800;

const gchar *GTK_STYLE_REGION_COLUMN = "column";

const gchar *GTK_STYLE_REGION_COLUMN_HEADER = "column-header";

const gchar *GTK_STYLE_REGION_ROW = "row";

const gchar *GTK_STYLE_REGION_TAB = "tab";

typedef struct _GtkScaleAccessibleClass GtkScaleAccessibleClass;

struct _GtkScaleAccessibleClass {
  GtkRangeAccessibleClass parent_class;
};

typedef struct _GtkScaleAccessiblePrivate GtkScaleAccessiblePrivate;

typedef struct _GtkScaleButtonAccessibleClass GtkScaleButtonAccessibleClass;

struct _GtkScaleButtonAccessibleClass {
  GtkButtonAccessibleClass parent_class;
};

typedef struct _GtkScaleButtonAccessiblePrivate GtkScaleButtonAccessiblePrivate;

typedef struct _GtkScaleButtonClass GtkScaleButtonClass;

struct _GtkScaleButtonClass {
  GtkButtonClass parent_class;
  void (*value_changed)(GtkScaleButton *button, gdouble value);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkScaleButtonPrivate GtkScaleButtonPrivate;

typedef struct _GtkScaleClass GtkScaleClass;

struct _GtkScaleClass {
  GtkRangeClass parent_class;
  gchar *(*format_value)(GtkScale *scale, gdouble value);
  void (*draw_value)(GtkScale *scale);
  void (*get_layout_offsets)(GtkScale *scale, gint *x, gint *y);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkScalePrivate GtkScalePrivate;

enum GtkScrollStep {
  GTK_SCROLL_STEPS = 0,
  GTK_SCROLL_PAGES = 1,
  GTK_SCROLL_ENDS = 2,
  GTK_SCROLL_HORIZONTAL_STEPS = 3,
  GTK_SCROLL_HORIZONTAL_PAGES = 4,
  GTK_SCROLL_HORIZONTAL_ENDS = 5
};

enum GtkScrollType {
  GTK_SCROLL_NONE = 0,
  GTK_SCROLL_JUMP = 1,
  GTK_SCROLL_STEP_BACKWARD = 2,
  GTK_SCROLL_STEP_FORWARD = 3,
  GTK_SCROLL_PAGE_BACKWARD = 4,
  GTK_SCROLL_PAGE_FORWARD = 5,
  GTK_SCROLL_STEP_UP = 6,
  GTK_SCROLL_STEP_DOWN = 7,
  GTK_SCROLL_PAGE_UP = 8,
  GTK_SCROLL_PAGE_DOWN = 9,
  GTK_SCROLL_STEP_LEFT = 10,
  GTK_SCROLL_STEP_RIGHT = 11,
  GTK_SCROLL_PAGE_LEFT = 12,
  GTK_SCROLL_PAGE_RIGHT = 13,
  GTK_SCROLL_START = 14,
  GTK_SCROLL_END = 15
};

typedef struct _GtkScrollableInterface GtkScrollableInterface;

struct _GtkScrollableInterface {
  GTypeInterface base_iface;
  gboolean (*get_border)(GtkScrollable *scrollable, GtkBorder *border);
};

enum GtkScrollablePolicy { GTK_SCROLL_MINIMUM = 0, GTK_SCROLL_NATURAL = 1 };

typedef struct _GtkScrollbarClass GtkScrollbarClass;

struct _GtkScrollbarClass {
  GtkRangeClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkScrolledWindowAccessibleClass
    GtkScrolledWindowAccessibleClass;

struct _GtkScrolledWindowAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkScrolledWindowAccessiblePrivate
    GtkScrolledWindowAccessiblePrivate;

typedef struct _GtkScrolledWindowClass GtkScrolledWindowClass;

struct _GtkScrolledWindowClass {
  GtkBinClass parent_class;
  gint scrollbar_spacing;
  gboolean (*scroll_child)(GtkScrolledWindow *scrolled_window,
                           GtkScrollType scroll, gboolean horizontal);
  void (*move_focus_out)(GtkScrolledWindow *scrolled_window,
                         GtkDirectionType direction);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkScrolledWindowPrivate GtkScrolledWindowPrivate;

typedef struct _GtkSearchBarClass GtkSearchBarClass;

struct _GtkSearchBarClass {
  GtkBinClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkSearchEntryClass GtkSearchEntryClass;

struct _GtkSearchEntryClass {
  GtkEntryClass parent_class;
  void (*search_changed)(GtkSearchEntry *entry);
  void (*next_match)(GtkSearchEntry *entry);
  void (*previous_match)(GtkSearchEntry *entry);
  void (*stop_search)(GtkSearchEntry *entry);
};

typedef struct _GtkSelectionData GtkSelectionData;
GtkSelectionData *gtk_selection_data_copy(const GtkSelectionData *data);
void gtk_selection_data_free(GtkSelectionData *data);
gtk_selection_data_get_data(const GtkSelectionData *selection_data);
GdkAtom
gtk_selection_data_get_data_type(const GtkSelectionData *selection_data);
gtk_selection_data_get_data_with_length(const GtkSelectionData *selection_data,
                                        gint *length);
GdkDisplay *
gtk_selection_data_get_display(const GtkSelectionData *selection_data);
gint gtk_selection_data_get_format(const GtkSelectionData *selection_data);
gint gtk_selection_data_get_length(const GtkSelectionData *selection_data);
GdkPixbuf *
gtk_selection_data_get_pixbuf(const GtkSelectionData *selection_data);
GdkAtom
gtk_selection_data_get_selection(const GtkSelectionData *selection_data);
GdkAtom gtk_selection_data_get_target(const GtkSelectionData *selection_data);
gboolean gtk_selection_data_get_targets(const GtkSelectionData *selection_data,
                                        targets, gint *n_atoms);
guchar *gtk_selection_data_get_text(const GtkSelectionData *selection_data);
gtk_selection_data_get_uris(const GtkSelectionData *selection_data);
void gtk_selection_data_set(GtkSelectionData *selection_data, GdkAtom type,
                            gint format, data, gint length);
gboolean gtk_selection_data_set_pixbuf(GtkSelectionData *selection_data,
                                       GdkPixbuf *pixbuf);
gboolean gtk_selection_data_set_text(GtkSelectionData *selection_data,
                                     const gchar *str, gint len);
gboolean gtk_selection_data_set_uris(GtkSelectionData *selection_data, uris);
gboolean
gtk_selection_data_targets_include_image(const GtkSelectionData *selection_data,
                                         gboolean writable);
gboolean gtk_selection_data_targets_include_rich_text(
    const GtkSelectionData *selection_data, GtkTextBuffer *buffer);
gboolean
gtk_selection_data_targets_include_text(const GtkSelectionData *selection_data);
gboolean
gtk_selection_data_targets_include_uri(const GtkSelectionData *selection_data);

enum GtkSelectionMode {
  GTK_SELECTION_NONE = 0,
  GTK_SELECTION_SINGLE = 1,
  GTK_SELECTION_BROWSE = 2,
  GTK_SELECTION_MULTIPLE = 3
};

enum GtkSensitivityType {
  GTK_SENSITIVITY_AUTO = 0,
  GTK_SENSITIVITY_ON = 1,
  GTK_SENSITIVITY_OFF = 2
};

typedef struct _GtkSeparatorClass GtkSeparatorClass;

struct _GtkSeparatorClass {
  GtkWidgetClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkSeparatorMenuItemClass GtkSeparatorMenuItemClass;

struct _GtkSeparatorMenuItemClass {
  GtkMenuItemClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkSeparatorPrivate GtkSeparatorPrivate;

typedef struct _GtkSeparatorToolItemClass GtkSeparatorToolItemClass;

struct _GtkSeparatorToolItemClass {
  GtkToolItemClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkSeparatorToolItemPrivate GtkSeparatorToolItemPrivate;

typedef struct _GtkSettingsClass GtkSettingsClass;

struct _GtkSettingsClass {
  GObjectClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkSettingsPrivate GtkSettingsPrivate;

typedef struct _GtkSettingsValue GtkSettingsValue;

struct _GtkSettingsValue {
  gchar *origin;
  GValue value;
};

enum GtkShadowType {
  GTK_SHADOW_NONE = 0,
  GTK_SHADOW_IN = 1,
  GTK_SHADOW_OUT = 2,
  GTK_SHADOW_ETCHED_IN = 3,
  GTK_SHADOW_ETCHED_OUT = 4
};

typedef struct _GtkShortcutLabelClass GtkShortcutLabelClass;

enum GtkShortcutType {
  GTK_SHORTCUT_ACCELERATOR = 0,
  GTK_SHORTCUT_GESTURE_PINCH = 1,
  GTK_SHORTCUT_GESTURE_STRETCH = 2,
  GTK_SHORTCUT_GESTURE_ROTATE_CLOCKWISE = 3,
  GTK_SHORTCUT_GESTURE_ROTATE_COUNTERCLOCKWISE = 4,
  GTK_SHORTCUT_GESTURE_TWO_FINGER_SWIPE_LEFT = 5,
  GTK_SHORTCUT_GESTURE_TWO_FINGER_SWIPE_RIGHT = 6,
  GTK_SHORTCUT_GESTURE = 7
};

typedef struct _GtkShortcutsGroupClass GtkShortcutsGroupClass;

typedef struct _GtkShortcutsSectionClass GtkShortcutsSectionClass;

typedef struct _GtkShortcutsShortcutClass GtkShortcutsShortcutClass;

typedef struct _GtkShortcutsWindowClass GtkShortcutsWindowClass;

struct _GtkShortcutsWindowClass {
  GtkWindowClass parent_class;
  void (*close)(GtkShortcutsWindow *self);
  void (*search)(GtkShortcutsWindow *self);
};

typedef struct _GtkSizeGroupClass GtkSizeGroupClass;

struct _GtkSizeGroupClass {
  GObjectClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkSizeGroupMode {
  GTK_SIZE_GROUP_NONE = 0,
  GTK_SIZE_GROUP_HORIZONTAL = 1,
  GTK_SIZE_GROUP_VERTICAL = 2,
  GTK_SIZE_GROUP_BOTH = 3
};

typedef struct _GtkSizeGroupPrivate GtkSizeGroupPrivate;

enum GtkSizeRequestMode {
  GTK_SIZE_REQUEST_HEIGHT_FOR_WIDTH = 0,
  GTK_SIZE_REQUEST_WIDTH_FOR_HEIGHT = 1,
  GTK_SIZE_REQUEST_CONSTANT_SIZE = 2
};

typedef struct _GtkSocketClass GtkSocketClass;

struct _GtkSocketClass {
  GtkContainerClass parent_class;
  void (*plug_added)(GtkSocket *socket_);
  gboolean (*plug_removed)(GtkSocket *socket_);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkSocketPrivate GtkSocketPrivate;

enum GtkSortType { GTK_SORT_ASCENDING = 0, GTK_SORT_DESCENDING = 1 };

typedef struct _GtkSpinButtonAccessibleClass GtkSpinButtonAccessibleClass;

struct _GtkSpinButtonAccessibleClass {
  GtkEntryAccessibleClass parent_class;
};

typedef struct _GtkSpinButtonAccessiblePrivate GtkSpinButtonAccessiblePrivate;

typedef struct _GtkSpinButtonClass GtkSpinButtonClass;

struct _GtkSpinButtonClass {
  GtkEntryClass parent_class;
  gint (*input)(GtkSpinButton *spin_button, gdouble *new_value);
  gint (*output)(GtkSpinButton *spin_button);
  void (*value_changed)(GtkSpinButton *spin_button);
  void (*change_value)(GtkSpinButton *spin_button, GtkScrollType scroll);
  void (*wrapped)(GtkSpinButton *spin_button);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkSpinButtonPrivate GtkSpinButtonPrivate;

enum GtkSpinButtonUpdatePolicy {
  GTK_UPDATE_ALWAYS = 0,
  GTK_UPDATE_IF_VALID = 1
};

enum GtkSpinType {
  GTK_SPIN_STEP_FORWARD = 0,
  GTK_SPIN_STEP_BACKWARD = 1,
  GTK_SPIN_PAGE_FORWARD = 2,
  GTK_SPIN_PAGE_BACKWARD = 3,
  GTK_SPIN_HOME = 4,
  GTK_SPIN_END = 5,
  GTK_SPIN_USER_DEFINED = 6
};

typedef struct _GtkSpinnerAccessibleClass GtkSpinnerAccessibleClass;

struct _GtkSpinnerAccessibleClass {
  GtkWidgetAccessibleClass parent_class;
};

typedef struct _GtkSpinnerAccessiblePrivate GtkSpinnerAccessiblePrivate;

typedef struct _GtkSpinnerClass GtkSpinnerClass;

struct _GtkSpinnerClass {
  GtkWidgetClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkSpinnerPrivate GtkSpinnerPrivate;

typedef struct _GtkStackAccessibleClass GtkStackAccessibleClass;

struct _GtkStackAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkStackClass GtkStackClass;

struct _GtkStackClass {
  GtkContainerClass parent_class;
};

typedef struct _GtkStackSidebarClass GtkStackSidebarClass;

struct _GtkStackSidebarClass {
  GtkBinClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkStackSidebarPrivate GtkStackSidebarPrivate;

typedef struct _GtkStackSwitcherClass GtkStackSwitcherClass;

struct _GtkStackSwitcherClass {
  GtkBoxClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkStackTransitionType {
  GTK_STACK_TRANSITION_TYPE_NONE = 0,
  GTK_STACK_TRANSITION_TYPE_CROSSFADE = 1,
  GTK_STACK_TRANSITION_TYPE_SLIDE_RIGHT = 2,
  GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT = 3,
  GTK_STACK_TRANSITION_TYPE_SLIDE_UP = 4,
  GTK_STACK_TRANSITION_TYPE_SLIDE_DOWN = 5,
  GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT_RIGHT = 6,
  GTK_STACK_TRANSITION_TYPE_SLIDE_UP_DOWN = 7,
  GTK_STACK_TRANSITION_TYPE_OVER_UP = 8,
  GTK_STACK_TRANSITION_TYPE_OVER_DOWN = 9,
  GTK_STACK_TRANSITION_TYPE_OVER_LEFT = 10,
  GTK_STACK_TRANSITION_TYPE_OVER_RIGHT = 11,
  GTK_STACK_TRANSITION_TYPE_UNDER_UP = 12,
  GTK_STACK_TRANSITION_TYPE_UNDER_DOWN = 13,
  GTK_STACK_TRANSITION_TYPE_UNDER_LEFT = 14,
  GTK_STACK_TRANSITION_TYPE_UNDER_RIGHT = 15,
  GTK_STACK_TRANSITION_TYPE_OVER_UP_DOWN = 16,
  GTK_STACK_TRANSITION_TYPE_OVER_DOWN_UP = 17,
  GTK_STACK_TRANSITION_TYPE_OVER_LEFT_RIGHT = 18,
  GTK_STACK_TRANSITION_TYPE_OVER_RIGHT_LEFT = 19
};

enum GtkStateFlags {
  GTK_STATE_FLAG_NORMAL = 0,
  GTK_STATE_FLAG_ACTIVE = 1,
  GTK_STATE_FLAG_PRELIGHT = 2,
  GTK_STATE_FLAG_SELECTED = 4,
  GTK_STATE_FLAG_INSENSITIVE = 8,
  GTK_STATE_FLAG_INCONSISTENT = 16,
  GTK_STATE_FLAG_FOCUSED = 32,
  GTK_STATE_FLAG_BACKDROP = 64,
  GTK_STATE_FLAG_DIR_LTR = 128,
  GTK_STATE_FLAG_DIR_RTL = 256,
  GTK_STATE_FLAG_LINK = 512,
  GTK_STATE_FLAG_VISITED = 1024,
  GTK_STATE_FLAG_CHECKED = 2048,
  GTK_STATE_FLAG_DROP_ACTIVE = 4096
};

enum GtkStateType {
  GTK_STATE_NORMAL = 0,
  GTK_STATE_ACTIVE = 1,
  GTK_STATE_PRELIGHT = 2,
  GTK_STATE_SELECTED = 3,
  GTK_STATE_INSENSITIVE = 4,
  GTK_STATE_INCONSISTENT = 5,
  GTK_STATE_FOCUSED = 6
};

typedef struct _GtkStatusIconClass GtkStatusIconClass;

struct _GtkStatusIconClass {
  GObjectClass parent_class;
  void (*activate)(GtkStatusIcon *status_icon);
  void (*popup_menu)(GtkStatusIcon *status_icon, guint button,
                     guint32 activate_time);
  gboolean (*size_changed)(GtkStatusIcon *status_icon, gint size);
  gboolean (*button_press_event)(GtkStatusIcon *status_icon,
                                 GdkEventButton *event);
  gboolean (*button_release_event)(GtkStatusIcon *status_icon,
                                   GdkEventButton *event);
  gboolean (*scroll_event)(GtkStatusIcon *status_icon, GdkEventScroll *event);
  gboolean (*query_tooltip)(GtkStatusIcon *status_icon, gint x, gint y,
                            gboolean keyboard_mode, GtkTooltip *tooltip);
  void *__gtk_reserved1;
  void *__gtk_reserved2;
  void *__gtk_reserved3;
  void *__gtk_reserved4;
};

typedef struct _GtkStatusIconPrivate GtkStatusIconPrivate;

typedef struct _GtkStatusbarAccessibleClass GtkStatusbarAccessibleClass;

struct _GtkStatusbarAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkStatusbarAccessiblePrivate GtkStatusbarAccessiblePrivate;

typedef struct _GtkStatusbarClass GtkStatusbarClass;

struct _GtkStatusbarClass {
  GtkBoxClass parent_class;
  gpointer reserved;
  void (*text_pushed)(GtkStatusbar *statusbar, guint context_id,
                      const gchar *text);
  void (*text_popped)(GtkStatusbar *statusbar, guint context_id,
                      const gchar *text);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkStatusbarPrivate GtkStatusbarPrivate;

typedef struct _GtkStockItem GtkStockItem;

struct _GtkStockItem {
  gchar *stock_id;
  gchar *label;
  GdkModifierType modifier;
  guint keyval;
  gchar *translation_domain;
};
GtkStockItem *gtk_stock_item_copy(const GtkStockItem *item);
void gtk_stock_item_free(GtkStockItem *item);

typedef struct _GtkStyleClass GtkStyleClass;

struct _GtkStyleClass {
  GObjectClass parent_class;
  void (*realize)(GtkStyle *style);
  void (*unrealize)(GtkStyle *style);
  void (*copy)(GtkStyle *style, GtkStyle *src);
  GtkStyle *(*clone)(GtkStyle *style);
  void (*init_from_rc)(GtkStyle *style, GtkRcStyle *rc_style);
  void (*set_background)(GtkStyle *style, GdkWindow *window,
                         GtkStateType state_type);
  GdkPixbuf *(*render_icon)(GtkStyle *style, const GtkIconSource *source,
                            GtkTextDirection direction, GtkStateType state,
                            GtkIconSize size, GtkWidget *widget,
                            const gchar *detail);
  void (*draw_hline)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                     GtkWidget *widget, const gchar *detail, gint x1, gint x2,
                     gint y);
  void (*draw_vline)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                     GtkWidget *widget, const gchar *detail, gint y1_, gint y2_,
                     gint x);
  void (*draw_shadow)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                      GtkShadowType shadow_type, GtkWidget *widget,
                      const gchar *detail, gint x, gint y, gint width,
                      gint height);
  void (*draw_arrow)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                     GtkShadowType shadow_type, GtkWidget *widget,
                     const gchar *detail, GtkArrowType arrow_type,
                     gboolean fill, gint x, gint y, gint width, gint height);
  void (*draw_diamond)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                       GtkShadowType shadow_type, GtkWidget *widget,
                       const gchar *detail, gint x, gint y, gint width,
                       gint height);
  void (*draw_box)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                   GtkShadowType shadow_type, GtkWidget *widget,
                   const gchar *detail, gint x, gint y, gint width,
                   gint height);
  void (*draw_flat_box)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                        GtkShadowType shadow_type, GtkWidget *widget,
                        const gchar *detail, gint x, gint y, gint width,
                        gint height);
  void (*draw_check)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                     GtkShadowType shadow_type, GtkWidget *widget,
                     const gchar *detail, gint x, gint y, gint width,
                     gint height);
  void (*draw_option)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                      GtkShadowType shadow_type, GtkWidget *widget,
                      const gchar *detail, gint x, gint y, gint width,
                      gint height);
  void (*draw_tab)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                   GtkShadowType shadow_type, GtkWidget *widget,
                   const gchar *detail, gint x, gint y, gint width,
                   gint height);
  void (*draw_shadow_gap)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                          GtkShadowType shadow_type, GtkWidget *widget,
                          const gchar *detail, gint x, gint y, gint width,
                          gint height, GtkPositionType gap_side, gint gap_x,
                          gint gap_width);
  void (*draw_box_gap)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                       GtkShadowType shadow_type, GtkWidget *widget,
                       const gchar *detail, gint x, gint y, gint width,
                       gint height, GtkPositionType gap_side, gint gap_x,
                       gint gap_width);
  void (*draw_extension)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                         GtkShadowType shadow_type, GtkWidget *widget,
                         const gchar *detail, gint x, gint y, gint width,
                         gint height, GtkPositionType gap_side);
  void (*draw_focus)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                     GtkWidget *widget, const gchar *detail, gint x, gint y,
                     gint width, gint height);
  void (*draw_slider)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                      GtkShadowType shadow_type, GtkWidget *widget,
                      const gchar *detail, gint x, gint y, gint width,
                      gint height, GtkOrientation orientation);
  void (*draw_handle)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                      GtkShadowType shadow_type, GtkWidget *widget,
                      const gchar *detail, gint x, gint y, gint width,
                      gint height, GtkOrientation orientation);
  void (*draw_expander)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                        GtkWidget *widget, const gchar *detail, gint x, gint y,
                        GtkExpanderStyle expander_style);
  void (*draw_layout)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                      gboolean use_text, GtkWidget *widget, const gchar *detail,
                      gint x, gint y, PangoLayout *layout);
  void (*draw_resize_grip)(GtkStyle *style, cairo_t *cr,
                           GtkStateType state_type, GtkWidget *widget,
                           const gchar *detail, GdkWindowEdge edge, gint x,
                           gint y, gint width, gint height);
  void (*draw_spinner)(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                       GtkWidget *widget, const gchar *detail, guint step,
                       gint x, gint y, gint width, gint height);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
  void (*_gtk_reserved9)();
  void (*_gtk_reserved10)();
  void (*_gtk_reserved11)();
};

typedef struct _GtkStyleContextClass GtkStyleContextClass;

struct _GtkStyleContextClass {
  GObjectClass parent_class;
  void (*changed)(GtkStyleContext *context);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkStyleContextPrintFlags {
  GTK_STYLE_CONTEXT_PRINT_NONE = 0,
  GTK_STYLE_CONTEXT_PRINT_RECURSE = 1,
  GTK_STYLE_CONTEXT_PRINT_SHOW_STYLE = 2
};

typedef struct _GtkStyleContextPrivate GtkStyleContextPrivate;

typedef struct _GtkStylePropertiesClass GtkStylePropertiesClass;

struct _GtkStylePropertiesClass {
  GObjectClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkStylePropertiesPrivate GtkStylePropertiesPrivate;
gboolean (*StylePropertyParser)(const gchar *string, GValue *value);

typedef struct _GtkStyleProviderIface GtkStyleProviderIface;

struct _GtkStyleProviderIface {
  GTypeInterface g_iface;
  GtkStyleProperties *(*get_style)(GtkStyleProvider *provider,
                                   GtkWidgetPath *path);
  gboolean (*get_style_property)(GtkStyleProvider *provider,
                                 GtkWidgetPath *path, GtkStateFlags state,
                                 GParamSpec *pspec, GValue *value);
  GtkIconFactory *(*get_icon_factory)(GtkStyleProvider *provider,
                                      GtkWidgetPath *path);
};

typedef struct _GtkSwitchAccessibleClass GtkSwitchAccessibleClass;

struct _GtkSwitchAccessibleClass {
  GtkWidgetAccessibleClass parent_class;
};

typedef struct _GtkSwitchAccessiblePrivate GtkSwitchAccessiblePrivate;

typedef struct _GtkSwitchClass GtkSwitchClass;

struct _GtkSwitchClass {
  GtkWidgetClass parent_class;
  void (*activate)(GtkSwitch *sw);
  gboolean (*state_set)(GtkSwitch *sw, gboolean state);
  void (*_switch_padding_1)();
  void (*_switch_padding_2)();
  void (*_switch_padding_3)();
  void (*_switch_padding_4)();
  void (*_switch_padding_5)();
};

typedef struct _GtkSwitchPrivate GtkSwitchPrivate;

typedef struct _GtkSymbolicColor GtkSymbolicColor;
GtkSymbolicColor *gtk_symbolic_color_ref(GtkSymbolicColor *color);
gboolean gtk_symbolic_color_resolve(GtkSymbolicColor *color,
                                    GtkStyleProperties *props,
                                    GdkRGBA *resolved_color);
char *gtk_symbolic_color_to_string(GtkSymbolicColor *color);
void gtk_symbolic_color_unref(GtkSymbolicColor *color);

const gint GTK_TEXT_VIEW_PRIORITY_VALIDATE = 5;

const gint GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID = -1;

const gint GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID = -2;

typedef struct _GtkTableChild GtkTableChild;

struct _GtkTableChild {
  GtkWidget *widget;
  guint16 left_attach;
  guint16 right_attach;
  guint16 top_attach;
  guint16 bottom_attach;
  guint16 xpadding;
  guint16 ypadding;
  guint xexpand;
  guint yexpand;
  guint xshrink;
  guint yshrink;
  guint xfill;
  guint yfill;
};

typedef struct _GtkTableClass GtkTableClass;

struct _GtkTableClass {
  GtkContainerClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkTablePrivate GtkTablePrivate;

typedef struct _GtkTableRowCol GtkTableRowCol;

struct _GtkTableRowCol {
  guint16 requisition;
  guint16 allocation;
  guint16 spacing;
  guint need_expand;
  guint need_shrink;
  guint expand;
  guint shrink;
  guint empty;
};

typedef struct _GtkTargetEntry GtkTargetEntry;

struct _GtkTargetEntry {
  gchar *target;
  guint flags;
  guint info;
};
GtkTargetEntry *gtk_target_entry_copy(GtkTargetEntry *data);
void gtk_target_entry_free(GtkTargetEntry *data);

enum GtkTargetFlags {
  GTK_TARGET_SAME_APP = 1,
  GTK_TARGET_SAME_WIDGET = 2,
  GTK_TARGET_OTHER_APP = 4,
  GTK_TARGET_OTHER_WIDGET = 8
};

typedef struct _GtkTargetList GtkTargetList;
void gtk_target_list_add(GtkTargetList *list, GdkAtom target, guint flags,
                         guint info);
void gtk_target_list_add_image_targets(GtkTargetList *list, guint info,
                                       gboolean writable);
void gtk_target_list_add_rich_text_targets(GtkTargetList *list, guint info,
                                           gboolean deserializable,
                                           GtkTextBuffer *buffer);
void gtk_target_list_add_table(GtkTargetList *list, targets, guint ntargets);
void gtk_target_list_add_text_targets(GtkTargetList *list, guint info);
void gtk_target_list_add_uri_targets(GtkTargetList *list, guint info);
gboolean gtk_target_list_find(GtkTargetList *list, GdkAtom target, guint *info);
GtkTargetList *gtk_target_list_ref(GtkTargetList *list);
void gtk_target_list_remove(GtkTargetList *list, GdkAtom target);
void gtk_target_list_unref(GtkTargetList *list);

typedef struct _GtkTargetPair GtkTargetPair;

struct _GtkTargetPair {
  GdkAtom target;
  guint flags;
  guint info;
};

typedef struct _GtkTearoffMenuItemClass GtkTearoffMenuItemClass;

struct _GtkTearoffMenuItemClass {
  GtkMenuItemClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkTearoffMenuItemPrivate GtkTearoffMenuItemPrivate;

typedef struct _GtkTextAppearance GtkTextAppearance;

struct _GtkTextAppearance {
  GdkColor bg_color;
  GdkColor fg_color;
  gint rise;
  guint underline;
  guint strikethrough;
  guint draw_bg;
  guint inside_selection;
  guint is_text;
};

typedef struct _GtkTextAttributes GtkTextAttributes;

struct _GtkTextAttributes {
  guint refcount;
  GtkTextAppearance appearance;
  GtkJustification justification;
  GtkTextDirection direction;
  PangoFontDescription *font;
  gdouble font_scale;
  gint left_margin;
  gint right_margin;
  gint indent;
  gint pixels_above_lines;
  gint pixels_below_lines;
  gint pixels_inside_wrap;
  PangoTabArray *tabs;
  GtkWrapMode wrap_mode;
  PangoLanguage *language;
  GdkColor *pg_bg_color;
  guint invisible;
  guint bg_full_height;
  guint editable;
  guint no_fallback;
  GdkRGBA *pg_bg_rgba;
  gint letter_spacing;
};
GtkTextAttributes *gtk_text_attributes_copy(GtkTextAttributes *src);
void gtk_text_attributes_copy_values(GtkTextAttributes *src,
                                     GtkTextAttributes *dest);
GtkTextAttributes *gtk_text_attributes_ref(GtkTextAttributes *values);
void gtk_text_attributes_unref(GtkTextAttributes *values);

typedef struct _GtkTextBTree GtkTextBTree;

typedef struct _GtkTextBufferClass GtkTextBufferClass;

struct _GtkTextBufferClass {
  GObjectClass parent_class;
  void (*insert_text)(GtkTextBuffer *buffer, GtkTextIter *pos,
                      const gchar *new_text, gint new_text_length);
  void (*insert_pixbuf)(GtkTextBuffer *buffer, GtkTextIter *iter,
                        GdkPixbuf *pixbuf);
  void (*insert_child_anchor)(GtkTextBuffer *buffer, GtkTextIter *iter,
                              GtkTextChildAnchor *anchor);
  void (*delete_range)(GtkTextBuffer *buffer, GtkTextIter *start,
                       GtkTextIter *end);
  void (*changed)(GtkTextBuffer *buffer);
  void (*modified_changed)(GtkTextBuffer *buffer);
  void (*mark_set)(GtkTextBuffer *buffer, const GtkTextIter *location,
                   GtkTextMark *mark);
  void (*mark_deleted)(GtkTextBuffer *buffer, GtkTextMark *mark);
  void (*apply_tag)(GtkTextBuffer *buffer, GtkTextTag *tag,
                    const GtkTextIter *start, const GtkTextIter *end);
  void (*remove_tag)(GtkTextBuffer *buffer, GtkTextTag *tag,
                     const GtkTextIter *start, const GtkTextIter *end);
  void (*begin_user_action)(GtkTextBuffer *buffer);
  void (*end_user_action)(GtkTextBuffer *buffer);
  void (*paste_done)(GtkTextBuffer *buffer, GtkClipboard *clipboard);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};
gboolean (*TextBufferDeserializeFunc)(GtkTextBuffer *register_buffer,
                                      GtkTextBuffer *content_buffer,
                                      GtkTextIter *iter, data, gsize length,
                                      gboolean create_tags, gpointer user_data);

typedef struct _GtkTextBufferPrivate GtkTextBufferPrivate;
guint8 *(*TextBufferSerializeFunc)(GtkTextBuffer *register_buffer,
                                   GtkTextBuffer *content_buffer,
                                   const GtkTextIter *start,
                                   const GtkTextIter *end, gsize *length,
                                   gpointer user_data);

enum GtkTextBufferTargetInfo {
  GTK_TEXT_BUFFER_TARGET_INFO_BUFFER_CONTENTS = -1,
  GTK_TEXT_BUFFER_TARGET_INFO_RICH_TEXT = -2,
  GTK_TEXT_BUFFER_TARGET_INFO_TEXT = -3
};

typedef struct _GtkTextCellAccessibleClass GtkTextCellAccessibleClass;

struct _GtkTextCellAccessibleClass {
  GtkRendererCellAccessibleClass parent_class;
};

typedef struct _GtkTextCellAccessiblePrivate GtkTextCellAccessiblePrivate;
gboolean (*TextCharPredicate)(gunichar ch, gpointer user_data);

typedef struct _GtkTextChildAnchorClass GtkTextChildAnchorClass;

struct _GtkTextChildAnchorClass {
  GObjectClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkTextDirection {
  GTK_TEXT_DIR_NONE = 0,
  GTK_TEXT_DIR_LTR = 1,
  GTK_TEXT_DIR_RTL = 2
};

enum GtkTextExtendSelection {
  GTK_TEXT_EXTEND_SELECTION_WORD = 0,
  GTK_TEXT_EXTEND_SELECTION_LINE = 1
};

typedef struct _GtkTextIter GtkTextIter;

struct _GtkTextIter {
  gpointer dummy1;
  gpointer dummy2;
  gint dummy3;
  gint dummy4;
  gint dummy5;
  gint dummy6;
  gint dummy7;
  gint dummy8;
  gpointer dummy9;
  gpointer dummy10;
  gint dummy11;
  gint dummy12;
  gint dummy13;
  gpointer dummy14;
};
void gtk_text_iter_assign(GtkTextIter *iter, const GtkTextIter *other);
gboolean gtk_text_iter_backward_char(GtkTextIter *iter);
gboolean gtk_text_iter_backward_chars(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_backward_cursor_position(GtkTextIter *iter);
gboolean gtk_text_iter_backward_cursor_positions(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_backward_find_char(GtkTextIter *iter,
                                          GtkTextCharPredicate pred,
                                          gpointer user_data,
                                          const GtkTextIter *limit);
gboolean gtk_text_iter_backward_line(GtkTextIter *iter);
gboolean gtk_text_iter_backward_lines(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_backward_search(
    const GtkTextIter *iter, const gchar *str, GtkTextSearchFlags flags,
    GtkTextIter *match_start, GtkTextIter *match_end, const GtkTextIter *limit);
gboolean gtk_text_iter_backward_sentence_start(GtkTextIter *iter);
gboolean gtk_text_iter_backward_sentence_starts(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_backward_to_tag_toggle(GtkTextIter *iter,
                                              GtkTextTag *tag);
gboolean gtk_text_iter_backward_visible_cursor_position(GtkTextIter *iter);
gboolean gtk_text_iter_backward_visible_cursor_positions(GtkTextIter *iter,
                                                         gint count);
gboolean gtk_text_iter_backward_visible_line(GtkTextIter *iter);
gboolean gtk_text_iter_backward_visible_lines(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_backward_visible_word_start(GtkTextIter *iter);
gboolean gtk_text_iter_backward_visible_word_starts(GtkTextIter *iter,
                                                    gint count);
gboolean gtk_text_iter_backward_word_start(GtkTextIter *iter);
gboolean gtk_text_iter_backward_word_starts(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_begins_tag(const GtkTextIter *iter, GtkTextTag *tag);
gboolean gtk_text_iter_can_insert(const GtkTextIter *iter,
                                  gboolean default_editability);
gint gtk_text_iter_compare(const GtkTextIter *lhs, const GtkTextIter *rhs);
GtkTextIter *gtk_text_iter_copy(const GtkTextIter *iter);
gboolean gtk_text_iter_editable(const GtkTextIter *iter,
                                gboolean default_setting);
gboolean gtk_text_iter_ends_line(const GtkTextIter *iter);
gboolean gtk_text_iter_ends_sentence(const GtkTextIter *iter);
gboolean gtk_text_iter_ends_tag(const GtkTextIter *iter, GtkTextTag *tag);
gboolean gtk_text_iter_ends_word(const GtkTextIter *iter);
gboolean gtk_text_iter_equal(const GtkTextIter *lhs, const GtkTextIter *rhs);
gboolean gtk_text_iter_forward_char(GtkTextIter *iter);
gboolean gtk_text_iter_forward_chars(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_forward_cursor_position(GtkTextIter *iter);
gboolean gtk_text_iter_forward_cursor_positions(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_forward_find_char(GtkTextIter *iter,
                                         GtkTextCharPredicate pred,
                                         gpointer user_data,
                                         const GtkTextIter *limit);
gboolean gtk_text_iter_forward_line(GtkTextIter *iter);
gboolean gtk_text_iter_forward_lines(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_forward_search(const GtkTextIter *iter, const gchar *str,
                                      GtkTextSearchFlags flags,
                                      GtkTextIter *match_start,
                                      GtkTextIter *match_end,
                                      const GtkTextIter *limit);
gboolean gtk_text_iter_forward_sentence_end(GtkTextIter *iter);
gboolean gtk_text_iter_forward_sentence_ends(GtkTextIter *iter, gint count);
void gtk_text_iter_forward_to_end(GtkTextIter *iter);
gboolean gtk_text_iter_forward_to_line_end(GtkTextIter *iter);
gboolean gtk_text_iter_forward_to_tag_toggle(GtkTextIter *iter,
                                             GtkTextTag *tag);
gboolean gtk_text_iter_forward_visible_cursor_position(GtkTextIter *iter);
gboolean gtk_text_iter_forward_visible_cursor_positions(GtkTextIter *iter,
                                                        gint count);
gboolean gtk_text_iter_forward_visible_line(GtkTextIter *iter);
gboolean gtk_text_iter_forward_visible_lines(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_forward_visible_word_end(GtkTextIter *iter);
gboolean gtk_text_iter_forward_visible_word_ends(GtkTextIter *iter, gint count);
gboolean gtk_text_iter_forward_word_end(GtkTextIter *iter);
gboolean gtk_text_iter_forward_word_ends(GtkTextIter *iter, gint count);
void gtk_text_iter_free(GtkTextIter *iter);
gboolean gtk_text_iter_get_attributes(const GtkTextIter *iter,
                                      GtkTextAttributes *values);
GtkTextBuffer *gtk_text_iter_get_buffer(const GtkTextIter *iter);
gint gtk_text_iter_get_bytes_in_line(const GtkTextIter *iter);
gunichar gtk_text_iter_get_char(const GtkTextIter *iter);
gint gtk_text_iter_get_chars_in_line(const GtkTextIter *iter);
GtkTextChildAnchor *gtk_text_iter_get_child_anchor(const GtkTextIter *iter);
PangoLanguage *gtk_text_iter_get_language(const GtkTextIter *iter);
gint gtk_text_iter_get_line(const GtkTextIter *iter);
gint gtk_text_iter_get_line_index(const GtkTextIter *iter);
gint gtk_text_iter_get_line_offset(const GtkTextIter *iter);
GSList *gtk_text_iter_get_marks(const GtkTextIter *iter);
gint gtk_text_iter_get_offset(const GtkTextIter *iter);
GdkPixbuf *gtk_text_iter_get_pixbuf(const GtkTextIter *iter);
gchar *gtk_text_iter_get_slice(const GtkTextIter *start,
                               const GtkTextIter *end);
GSList *gtk_text_iter_get_tags(const GtkTextIter *iter);
gchar *gtk_text_iter_get_text(const GtkTextIter *start, const GtkTextIter *end);
GSList *gtk_text_iter_get_toggled_tags(const GtkTextIter *iter,
                                       gboolean toggled_on);
gint gtk_text_iter_get_visible_line_index(const GtkTextIter *iter);
gint gtk_text_iter_get_visible_line_offset(const GtkTextIter *iter);
gchar *gtk_text_iter_get_visible_slice(const GtkTextIter *start,
                                       const GtkTextIter *end);
gchar *gtk_text_iter_get_visible_text(const GtkTextIter *start,
                                      const GtkTextIter *end);
gboolean gtk_text_iter_has_tag(const GtkTextIter *iter, GtkTextTag *tag);
gboolean gtk_text_iter_in_range(const GtkTextIter *iter,
                                const GtkTextIter *start,
                                const GtkTextIter *end);
gboolean gtk_text_iter_inside_sentence(const GtkTextIter *iter);
gboolean gtk_text_iter_inside_word(const GtkTextIter *iter);
gboolean gtk_text_iter_is_cursor_position(const GtkTextIter *iter);
gboolean gtk_text_iter_is_end(const GtkTextIter *iter);
gboolean gtk_text_iter_is_start(const GtkTextIter *iter);
void gtk_text_iter_order(GtkTextIter *first, GtkTextIter *second);
void gtk_text_iter_set_line(GtkTextIter *iter, gint line_number);
void gtk_text_iter_set_line_index(GtkTextIter *iter, gint byte_on_line);
void gtk_text_iter_set_line_offset(GtkTextIter *iter, gint char_on_line);
void gtk_text_iter_set_offset(GtkTextIter *iter, gint char_offset);
void gtk_text_iter_set_visible_line_index(GtkTextIter *iter, gint byte_on_line);
void gtk_text_iter_set_visible_line_offset(GtkTextIter *iter,
                                           gint char_on_line);
gboolean gtk_text_iter_starts_line(const GtkTextIter *iter);
gboolean gtk_text_iter_starts_sentence(const GtkTextIter *iter);
gboolean gtk_text_iter_starts_tag(const GtkTextIter *iter, GtkTextTag *tag);
gboolean gtk_text_iter_starts_word(const GtkTextIter *iter);
gboolean gtk_text_iter_toggles_tag(const GtkTextIter *iter, GtkTextTag *tag);

typedef struct _GtkTextMarkClass GtkTextMarkClass;

struct _GtkTextMarkClass {
  GObjectClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkTextSearchFlags {
  GTK_TEXT_SEARCH_VISIBLE_ONLY = 1,
  GTK_TEXT_SEARCH_TEXT_ONLY = 2,
  GTK_TEXT_SEARCH_CASE_INSENSITIVE = 4
};

typedef struct _GtkTextTagClass GtkTextTagClass;

struct _GtkTextTagClass {
  GObjectClass parent_class;
  gboolean (*event)(GtkTextTag *tag, GObject *event_object, GdkEvent *event,
                    const GtkTextIter *iter);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkTextTagPrivate GtkTextTagPrivate;

typedef struct _GtkTextTagTableClass GtkTextTagTableClass;

struct _GtkTextTagTableClass {
  GObjectClass parent_class;
  void (*tag_changed)(GtkTextTagTable *table, GtkTextTag *tag,
                      gboolean size_changed);
  void (*tag_added)(GtkTextTagTable *table, GtkTextTag *tag);
  void (*tag_removed)(GtkTextTagTable *table, GtkTextTag *tag);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};
void (*TextTagTableForeach)(GtkTextTag *tag, gpointer data);

typedef struct _GtkTextTagTablePrivate GtkTextTagTablePrivate;

typedef struct _GtkTextViewAccessibleClass GtkTextViewAccessibleClass;

struct _GtkTextViewAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkTextViewAccessiblePrivate GtkTextViewAccessiblePrivate;

typedef struct _GtkTextViewClass GtkTextViewClass;

struct _GtkTextViewClass {
  GtkContainerClass parent_class;
  void (*populate_popup)(GtkTextView *text_view, GtkWidget *popup);
  void (*move_cursor)(GtkTextView *text_view, GtkMovementStep step, gint count,
                      gboolean extend_selection);
  void (*set_anchor)(GtkTextView *text_view);
  void (*insert_at_cursor)(GtkTextView *text_view, const gchar *str);
  void (*delete_from_cursor)(GtkTextView *text_view, GtkDeleteType type,
                             gint count);
  void (*backspace)(GtkTextView *text_view);
  void (*cut_clipboard)(GtkTextView *text_view);
  void (*copy_clipboard)(GtkTextView *text_view);
  void (*paste_clipboard)(GtkTextView *text_view);
  void (*toggle_overwrite)(GtkTextView *text_view);
  GtkTextBuffer *(*create_buffer)(GtkTextView *text_view);
  void (*draw_layer)(GtkTextView *text_view, GtkTextViewLayer layer,
                     cairo_t *cr);
  gboolean (*extend_selection)(GtkTextView *text_view,
                               GtkTextExtendSelection granularity,
                               const GtkTextIter *location, GtkTextIter *start,
                               GtkTextIter *end);
  void (*insert_emoji)(GtkTextView *text_view);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkTextViewLayer {
  GTK_TEXT_VIEW_LAYER_BELOW = 0,
  GTK_TEXT_VIEW_LAYER_ABOVE = 1,
  GTK_TEXT_VIEW_LAYER_BELOW_TEXT = 2,
  GTK_TEXT_VIEW_LAYER_ABOVE_TEXT = 3
};

typedef struct _GtkTextViewPrivate GtkTextViewPrivate;

enum GtkTextWindowType {
  GTK_TEXT_WINDOW_PRIVATE = 0,
  GTK_TEXT_WINDOW_WIDGET = 1,
  GTK_TEXT_WINDOW_TEXT = 2,
  GTK_TEXT_WINDOW_LEFT = 3,
  GTK_TEXT_WINDOW_RIGHT = 4,
  GTK_TEXT_WINDOW_TOP = 5,
  GTK_TEXT_WINDOW_BOTTOM = 6
};

typedef struct _GtkThemeEngine GtkThemeEngine;

typedef struct _GtkThemingEngineClass GtkThemingEngineClass;

struct _GtkThemingEngineClass {
  GObjectClass parent_class;
  void (*render_line)(GtkThemingEngine *engine, cairo_t *cr, gdouble x0,
                      gdouble y0, gdouble x1, gdouble y1);
  void (*render_background)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                            gdouble y, gdouble width, gdouble height);
  void (*render_frame)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                       gdouble y, gdouble width, gdouble height);
  void (*render_frame_gap)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                           gdouble y, gdouble width, gdouble height,
                           GtkPositionType gap_side, gdouble xy0_gap,
                           gdouble xy1_gap);
  void (*render_extension)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                           gdouble y, gdouble width, gdouble height,
                           GtkPositionType gap_side);
  void (*render_check)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                       gdouble y, gdouble width, gdouble height);
  void (*render_option)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                        gdouble y, gdouble width, gdouble height);
  void (*render_arrow)(GtkThemingEngine *engine, cairo_t *cr, gdouble angle,
                       gdouble x, gdouble y, gdouble size);
  void (*render_expander)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                          gdouble y, gdouble width, gdouble height);
  void (*render_focus)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                       gdouble y, gdouble width, gdouble height);
  void (*render_layout)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                        gdouble y, PangoLayout *layout);
  void (*render_slider)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                        gdouble y, gdouble width, gdouble height,
                        GtkOrientation orientation);
  void (*render_handle)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                        gdouble y, gdouble width, gdouble height);
  void (*render_activity)(GtkThemingEngine *engine, cairo_t *cr, gdouble x,
                          gdouble y, gdouble width, gdouble height);
  GdkPixbuf *(*render_icon_pixbuf)(GtkThemingEngine *engine,
                                   const GtkIconSource *source,
                                   GtkIconSize size);
  void (*render_icon)(GtkThemingEngine *engine, cairo_t *cr, GdkPixbuf *pixbuf,
                      gdouble x, gdouble y);
  void (*render_icon_surface)(GtkThemingEngine *engine, cairo_t *cr,
                              cairo_surface_t *surface, gdouble x, gdouble y);
  gpointer padding[14];
};

typedef struct _GtkThemingEnginePrivate GtkThemingEnginePrivate;
gboolean (*TickCallback)(GtkWidget *widget, GdkFrameClock *frame_clock,
                         gpointer user_data);

typedef struct _GtkToggleActionClass GtkToggleActionClass;

struct _GtkToggleActionClass {
  GtkActionClass parent_class;
  void (*toggled)(GtkToggleAction *action);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkToggleActionEntry GtkToggleActionEntry;

struct _GtkToggleActionEntry {
  const gchar *name;
  const gchar *stock_id;
  const gchar *label;
  const gchar *accelerator;
  const gchar *tooltip;
  GCallback callback;
  gboolean is_active;
};

typedef struct _GtkToggleActionPrivate GtkToggleActionPrivate;

typedef struct _GtkToggleButtonAccessibleClass GtkToggleButtonAccessibleClass;

struct _GtkToggleButtonAccessibleClass {
  GtkButtonAccessibleClass parent_class;
};

typedef struct _GtkToggleButtonAccessiblePrivate
    GtkToggleButtonAccessiblePrivate;

typedef struct _GtkToggleButtonClass GtkToggleButtonClass;

struct _GtkToggleButtonClass {
  GtkButtonClass parent_class;
  void (*toggled)(GtkToggleButton *toggle_button);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkToggleButtonPrivate GtkToggleButtonPrivate;

typedef struct _GtkToggleToolButtonClass GtkToggleToolButtonClass;

struct _GtkToggleToolButtonClass {
  GtkToolButtonClass parent_class;
  void (*toggled)(GtkToggleToolButton *button);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkToggleToolButtonPrivate GtkToggleToolButtonPrivate;

typedef struct _GtkToolButtonClass GtkToolButtonClass;

struct _GtkToolButtonClass {
  GtkToolItemClass parent_class;
  GType button_type;
  void (*clicked)(GtkToolButton *tool_item);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkToolButtonPrivate GtkToolButtonPrivate;

typedef struct _GtkToolItemClass GtkToolItemClass;

struct _GtkToolItemClass {
  GtkBinClass parent_class;
  gboolean (*create_menu_proxy)(GtkToolItem *tool_item);
  void (*toolbar_reconfigured)(GtkToolItem *tool_item);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkToolItemGroupClass GtkToolItemGroupClass;

struct _GtkToolItemGroupClass {
  GtkContainerClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkToolItemGroupPrivate GtkToolItemGroupPrivate;

typedef struct _GtkToolItemPrivate GtkToolItemPrivate;

typedef struct _GtkToolPaletteClass GtkToolPaletteClass;

struct _GtkToolPaletteClass {
  GtkContainerClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkToolPaletteDragTargets {
  GTK_TOOL_PALETTE_DRAG_ITEMS = 1,
  GTK_TOOL_PALETTE_DRAG_GROUPS = 2
};

typedef struct _GtkToolPalettePrivate GtkToolPalettePrivate;

typedef struct _GtkToolShellIface GtkToolShellIface;

struct _GtkToolShellIface {
  GTypeInterface g_iface;
  GtkIconSize (*get_icon_size)(GtkToolShell *shell);
  GtkOrientation (*get_orientation)(GtkToolShell *shell);
  GtkToolbarStyle (*get_style)(GtkToolShell *shell);
  GtkReliefStyle (*get_relief_style)(GtkToolShell *shell);
  void (*rebuild_menu)(GtkToolShell *shell);
  GtkOrientation (*get_text_orientation)(GtkToolShell *shell);
  gfloat (*get_text_alignment)(GtkToolShell *shell);
  PangoEllipsizeMode (*get_ellipsize_mode)(GtkToolShell *shell);
  GtkSizeGroup *(*get_text_size_group)(GtkToolShell *shell);
};

typedef struct _GtkToolbarClass GtkToolbarClass;

struct _GtkToolbarClass {
  GtkContainerClass parent_class;
  void (*orientation_changed)(GtkToolbar *toolbar, GtkOrientation orientation);
  void (*style_changed)(GtkToolbar *toolbar, GtkToolbarStyle style);
  gboolean (*popup_context_menu)(GtkToolbar *toolbar, gint x, gint y,
                                 gint button_number);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkToolbarPrivate GtkToolbarPrivate;

enum GtkToolbarSpaceStyle {
  GTK_TOOLBAR_SPACE_EMPTY = 0,
  GTK_TOOLBAR_SPACE_LINE = 1
};

enum GtkToolbarStyle {
  GTK_TOOLBAR_ICONS = 0,
  GTK_TOOLBAR_TEXT = 1,
  GTK_TOOLBAR_BOTH = 2,
  GTK_TOOLBAR_BOTH_HORIZ = 3
};

typedef struct _GtkToplevelAccessibleClass GtkToplevelAccessibleClass;

struct _GtkToplevelAccessibleClass {
  AtkObjectClass parent_class;
};

typedef struct _GtkToplevelAccessiblePrivate GtkToplevelAccessiblePrivate;
gchar *(*TranslateFunc)(const gchar *path, gpointer func_data);
void (*TreeCellDataFunc)(GtkTreeViewColumn *tree_column, GtkCellRenderer *cell,
                         GtkTreeModel *tree_model, GtkTreeIter *iter,
                         gpointer data);
void (*TreeDestroyCountFunc)(GtkTreeView *tree_view, GtkTreePath *path,
                             gint children, gpointer user_data);

typedef struct _GtkTreeDragDestIface GtkTreeDragDestIface;

struct _GtkTreeDragDestIface {
  GTypeInterface g_iface;
  gboolean (*drag_data_received)(GtkTreeDragDest *drag_dest, GtkTreePath *dest,
                                 GtkSelectionData *selection_data);
  gboolean (*row_drop_possible)(GtkTreeDragDest *drag_dest,
                                GtkTreePath *dest_path,
                                GtkSelectionData *selection_data);
};

typedef struct _GtkTreeDragSourceIface GtkTreeDragSourceIface;

struct _GtkTreeDragSourceIface {
  GTypeInterface g_iface;
  gboolean (*row_draggable)(GtkTreeDragSource *drag_source, GtkTreePath *path);
  gboolean (*drag_data_get)(GtkTreeDragSource *drag_source, GtkTreePath *path,
                            GtkSelectionData *selection_data);
  gboolean (*drag_data_delete)(GtkTreeDragSource *drag_source,
                               GtkTreePath *path);
};

typedef struct _GtkTreeIter GtkTreeIter;

struct _GtkTreeIter {
  gint stamp;
  gpointer user_data;
  gpointer user_data2;
  gpointer user_data3;
};
GtkTreeIter *gtk_tree_iter_copy(GtkTreeIter *iter);
void gtk_tree_iter_free(GtkTreeIter *iter);
gint (*TreeIterCompareFunc)(GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b,
                            gpointer user_data);

typedef struct _GtkTreeModelFilterClass GtkTreeModelFilterClass;

struct _GtkTreeModelFilterClass {
  GObjectClass parent_class;
  gboolean (*visible)(GtkTreeModelFilter *self, GtkTreeModel *child_model,
                      GtkTreeIter *iter);
  void (*modify)(GtkTreeModelFilter *self, GtkTreeModel *child_model,
                 GtkTreeIter *iter, GValue *value, gint column);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};
void (*TreeModelFilterModifyFunc)(GtkTreeModel *model, GtkTreeIter *iter,
                                  GValue *value, gint column, gpointer data);

typedef struct _GtkTreeModelFilterPrivate GtkTreeModelFilterPrivate;
gboolean (*TreeModelFilterVisibleFunc)(GtkTreeModel *model, GtkTreeIter *iter,
                                       gpointer data);

enum GtkTreeModelFlags {
  GTK_TREE_MODEL_ITERS_PERSIST = 1,
  GTK_TREE_MODEL_LIST_ONLY = 2
};
gboolean (*TreeModelForeachFunc)(GtkTreeModel *model, GtkTreePath *path,
                                 GtkTreeIter *iter, gpointer data);

typedef struct _GtkTreeModelIface GtkTreeModelIface;

struct _GtkTreeModelIface {
  GTypeInterface g_iface;
  void (*row_changed)(GtkTreeModel *tree_model, GtkTreePath *path,
                      GtkTreeIter *iter);
  void (*row_inserted)(GtkTreeModel *tree_model, GtkTreePath *path,
                       GtkTreeIter *iter);
  void (*row_has_child_toggled)(GtkTreeModel *tree_model, GtkTreePath *path,
                                GtkTreeIter *iter);
  void (*row_deleted)(GtkTreeModel *tree_model, GtkTreePath *path);
  void (*rows_reordered)(GtkTreeModel *tree_model, GtkTreePath *path,
                         GtkTreeIter *iter, gint *new_order);
  GtkTreeModelFlags (*get_flags)(GtkTreeModel *tree_model);
  gint (*get_n_columns)(GtkTreeModel *tree_model);
  GType (*get_column_type)(GtkTreeModel *tree_model, gint index_);
  gboolean (*get_iter)(GtkTreeModel *tree_model, GtkTreeIter *iter,
                       GtkTreePath *path);
  GtkTreePath *(*get_path)(GtkTreeModel *tree_model, GtkTreeIter *iter);
  void (*get_value)(GtkTreeModel *tree_model, GtkTreeIter *iter, gint column,
                    GValue *value);
  gboolean (*iter_next)(GtkTreeModel *tree_model, GtkTreeIter *iter);
  gboolean (*iter_previous)(GtkTreeModel *tree_model, GtkTreeIter *iter);
  gboolean (*iter_children)(GtkTreeModel *tree_model, GtkTreeIter *iter,
                            GtkTreeIter *parent);
  gboolean (*iter_has_child)(GtkTreeModel *tree_model, GtkTreeIter *iter);
  gint (*iter_n_children)(GtkTreeModel *tree_model, GtkTreeIter *iter);
  gboolean (*iter_nth_child)(GtkTreeModel *tree_model, GtkTreeIter *iter,
                             GtkTreeIter *parent, gint n);
  gboolean (*iter_parent)(GtkTreeModel *tree_model, GtkTreeIter *iter,
                          GtkTreeIter *child);
  void (*ref_node)(GtkTreeModel *tree_model, GtkTreeIter *iter);
  void (*unref_node)(GtkTreeModel *tree_model, GtkTreeIter *iter);
};

typedef struct _GtkTreeModelSortClass GtkTreeModelSortClass;

struct _GtkTreeModelSortClass {
  GObjectClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkTreeModelSortPrivate GtkTreeModelSortPrivate;

typedef struct _GtkTreePath GtkTreePath;
void gtk_tree_path_append_index(GtkTreePath *path, gint index_);
gint gtk_tree_path_compare(const GtkTreePath *a, const GtkTreePath *b);
GtkTreePath *gtk_tree_path_copy(const GtkTreePath *path);
void gtk_tree_path_down(GtkTreePath *path);
void gtk_tree_path_free(GtkTreePath *path);
gint gtk_tree_path_get_depth(GtkTreePath *path);
gint *gtk_tree_path_get_indices(GtkTreePath *path);
gtk_tree_path_get_indices_with_depth(GtkTreePath *path, gint *depth);
gboolean gtk_tree_path_is_ancestor(GtkTreePath *path, GtkTreePath *descendant);
gboolean gtk_tree_path_is_descendant(GtkTreePath *path, GtkTreePath *ancestor);
void gtk_tree_path_next(GtkTreePath *path);
void gtk_tree_path_prepend_index(GtkTreePath *path, gint index_);
gboolean gtk_tree_path_prev(GtkTreePath *path);
gchar *gtk_tree_path_to_string(GtkTreePath *path);
gboolean gtk_tree_path_up(GtkTreePath *path);

typedef struct _GtkTreeRowReference GtkTreeRowReference;
GtkTreeRowReference *
gtk_tree_row_reference_copy(GtkTreeRowReference *reference);
void gtk_tree_row_reference_free(GtkTreeRowReference *reference);
GtkTreeModel *gtk_tree_row_reference_get_model(GtkTreeRowReference *reference);
GtkTreePath *gtk_tree_row_reference_get_path(GtkTreeRowReference *reference);
gboolean gtk_tree_row_reference_valid(GtkTreeRowReference *reference);

typedef struct _GtkTreeSelectionClass GtkTreeSelectionClass;

struct _GtkTreeSelectionClass {
  GObjectClass parent_class;
  void (*changed)(GtkTreeSelection *selection);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};
void (*TreeSelectionForeachFunc)(GtkTreeModel *model, GtkTreePath *path,
                                 GtkTreeIter *iter, gpointer data);
gboolean (*TreeSelectionFunc)(GtkTreeSelection *selection, GtkTreeModel *model,
                              GtkTreePath *path,
                              gboolean path_currently_selected, gpointer data);

typedef struct _GtkTreeSelectionPrivate GtkTreeSelectionPrivate;

typedef struct _GtkTreeSortableIface GtkTreeSortableIface;

struct _GtkTreeSortableIface {
  GTypeInterface g_iface;
  void (*sort_column_changed)(GtkTreeSortable *sortable);
  gboolean (*get_sort_column_id)(GtkTreeSortable *sortable,
                                 gint *sort_column_id, GtkSortType *order);
  void (*set_sort_column_id)(GtkTreeSortable *sortable, gint sort_column_id,
                             GtkSortType order);
  void (*set_sort_func)(GtkTreeSortable *sortable, gint sort_column_id,
                        GtkTreeIterCompareFunc sort_func, gpointer user_data,
                        GDestroyNotify destroy);
  void (*set_default_sort_func)(GtkTreeSortable *sortable,
                                GtkTreeIterCompareFunc sort_func,
                                gpointer user_data, GDestroyNotify destroy);
  gboolean (*has_default_sort_func)(GtkTreeSortable *sortable);
};

typedef struct _GtkTreeStoreClass GtkTreeStoreClass;

struct _GtkTreeStoreClass {
  GObjectClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkTreeStorePrivate GtkTreeStorePrivate;

typedef struct _GtkTreeViewAccessibleClass GtkTreeViewAccessibleClass;

struct _GtkTreeViewAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkTreeViewAccessiblePrivate GtkTreeViewAccessiblePrivate;

typedef struct _GtkTreeViewClass GtkTreeViewClass;

struct _GtkTreeViewClass {
  GtkContainerClass parent_class;
  void (*row_activated)(GtkTreeView *tree_view, GtkTreePath *path,
                        GtkTreeViewColumn *column);
  gboolean (*test_expand_row)(GtkTreeView *tree_view, GtkTreeIter *iter,
                              GtkTreePath *path);
  gboolean (*test_collapse_row)(GtkTreeView *tree_view, GtkTreeIter *iter,
                                GtkTreePath *path);
  void (*row_expanded)(GtkTreeView *tree_view, GtkTreeIter *iter,
                       GtkTreePath *path);
  void (*row_collapsed)(GtkTreeView *tree_view, GtkTreeIter *iter,
                        GtkTreePath *path);
  void (*columns_changed)(GtkTreeView *tree_view);
  void (*cursor_changed)(GtkTreeView *tree_view);
  gboolean (*move_cursor)(GtkTreeView *tree_view, GtkMovementStep step,
                          gint count);
  gboolean (*select_all)(GtkTreeView *tree_view);
  gboolean (*unselect_all)(GtkTreeView *tree_view);
  gboolean (*select_cursor_row)(GtkTreeView *tree_view, gboolean start_editing);
  gboolean (*toggle_cursor_row)(GtkTreeView *tree_view);
  gboolean (*expand_collapse_cursor_row)(GtkTreeView *tree_view,
                                         gboolean logical, gboolean expand,
                                         gboolean open_all);
  gboolean (*select_cursor_parent)(GtkTreeView *tree_view);
  gboolean (*start_interactive_search)(GtkTreeView *tree_view);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
  void (*_gtk_reserved5)();
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
  void (*_gtk_reserved8)();
};

typedef struct _GtkTreeViewColumnClass GtkTreeViewColumnClass;

struct _GtkTreeViewColumnClass {
  GInitiallyUnownedClass parent_class;
  void (*clicked)(GtkTreeViewColumn *tree_column);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};
gboolean (*TreeViewColumnDropFunc)(GtkTreeView *tree_view,
                                   GtkTreeViewColumn *column,
                                   GtkTreeViewColumn *prev_column,
                                   GtkTreeViewColumn *next_column,
                                   gpointer data);

typedef struct _GtkTreeViewColumnPrivate GtkTreeViewColumnPrivate;

enum GtkTreeViewColumnSizing {
  GTK_TREE_VIEW_COLUMN_GROW_ONLY = 0,
  GTK_TREE_VIEW_COLUMN_AUTOSIZE = 1,
  GTK_TREE_VIEW_COLUMN_FIXED = 2
};

enum GtkTreeViewDropPosition {
  GTK_TREE_VIEW_DROP_BEFORE = 0,
  GTK_TREE_VIEW_DROP_AFTER = 1,
  GTK_TREE_VIEW_DROP_INTO_OR_BEFORE = 2,
  GTK_TREE_VIEW_DROP_INTO_OR_AFTER = 3
};

enum GtkTreeViewGridLines {
  GTK_TREE_VIEW_GRID_LINES_NONE = 0,
  GTK_TREE_VIEW_GRID_LINES_HORIZONTAL = 1,
  GTK_TREE_VIEW_GRID_LINES_VERTICAL = 2,
  GTK_TREE_VIEW_GRID_LINES_BOTH = 3
};
void (*TreeViewMappingFunc)(GtkTreeView *tree_view, GtkTreePath *path,
                            gpointer user_data);

typedef struct _GtkTreeViewPrivate GtkTreeViewPrivate;
gboolean (*TreeViewRowSeparatorFunc)(GtkTreeModel *model, GtkTreeIter *iter,
                                     gpointer data);
gboolean (*TreeViewSearchEqualFunc)(GtkTreeModel *model, gint column,
                                    const gchar *key, GtkTreeIter *iter,
                                    gpointer search_data);
void (*TreeViewSearchPositionFunc)(GtkTreeView *tree_view,
                                   GtkWidget *search_dialog,
                                   gpointer user_data);

typedef struct _GtkUIManagerClass GtkUIManagerClass;

struct _GtkUIManagerClass {
  GObjectClass parent_class;
  void (*add_widget)(GtkUIManager *manager, GtkWidget *widget);
  void (*actions_changed)(GtkUIManager *manager);
  void (*connect_proxy)(GtkUIManager *manager, GtkAction *action,
                        GtkWidget *proxy);
  void (*disconnect_proxy)(GtkUIManager *manager, GtkAction *action,
                           GtkWidget *proxy);
  void (*pre_activate)(GtkUIManager *manager, GtkAction *action);
  void (*post_activate)(GtkUIManager *manager, GtkAction *action);
  GtkWidget *(*get_widget)(GtkUIManager *manager, const gchar *path);
  GtkAction *(*get_action)(GtkUIManager *manager, const gchar *path);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

enum GtkUIManagerItemType {
  GTK_UI_MANAGER_AUTO = 0,
  GTK_UI_MANAGER_MENUBAR = 1,
  GTK_UI_MANAGER_MENU = 2,
  GTK_UI_MANAGER_TOOLBAR = 4,
  GTK_UI_MANAGER_PLACEHOLDER = 8,
  GTK_UI_MANAGER_POPUP = 16,
  GTK_UI_MANAGER_MENUITEM = 32,
  GTK_UI_MANAGER_TOOLITEM = 64,
  GTK_UI_MANAGER_SEPARATOR = 128,
  GTK_UI_MANAGER_ACCELERATOR = 256,
  GTK_UI_MANAGER_POPUP_WITH_ACCELS = 512
};

typedef struct _GtkUIManagerPrivate GtkUIManagerPrivate;

enum GtkUnit {
  GTK_UNIT_NONE = 0,
  GTK_UNIT_POINTS = 1,
  GTK_UNIT_INCH = 2,
  GTK_UNIT_MM = 3
};

typedef struct _GtkVBoxClass GtkVBoxClass;

struct _GtkVBoxClass {
  GtkBoxClass parent_class;
};

typedef struct _GtkVButtonBoxClass GtkVButtonBoxClass;

struct _GtkVButtonBoxClass {
  GtkButtonBoxClass parent_class;
};

typedef struct _GtkVPanedClass GtkVPanedClass;

struct _GtkVPanedClass {
  GtkPanedClass parent_class;
};

typedef struct _GtkVScaleClass GtkVScaleClass;

struct _GtkVScaleClass {
  GtkScaleClass parent_class;
};

typedef struct _GtkVScrollbarClass GtkVScrollbarClass;

struct _GtkVScrollbarClass {
  GtkScrollbarClass parent_class;
};

typedef struct _GtkVSeparatorClass GtkVSeparatorClass;

struct _GtkVSeparatorClass {
  GtkSeparatorClass parent_class;
};

typedef struct _GtkViewportClass GtkViewportClass;

struct _GtkViewportClass {
  GtkBinClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkViewportPrivate GtkViewportPrivate;

typedef struct _GtkVolumeButtonClass GtkVolumeButtonClass;

struct _GtkVolumeButtonClass {
  GtkScaleButtonClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkWidgetAccessibleClass GtkWidgetAccessibleClass;

struct _GtkWidgetAccessibleClass {
  GtkAccessibleClass parent_class;
  void (*notify_gtk)(GObject *object, GParamSpec *pspec);
};

typedef struct _GtkWidgetAccessiblePrivate GtkWidgetAccessiblePrivate;

typedef struct _GtkWidgetClass GtkWidgetClass;

struct _GtkWidgetClass {
  GInitiallyUnownedClass parent_class;
  guint activate_signal;
  void (*dispatch_child_properties_changed)(GtkWidget *widget, guint n_pspecs,
                                            GParamSpec **pspecs);
  void (*destroy)(GtkWidget *widget);
  void (*show)(GtkWidget *widget);
  void (*show_all)(GtkWidget *widget);
  void (*hide)(GtkWidget *widget);
  void (*map)(GtkWidget *widget);
  void (*unmap)(GtkWidget *widget);
  void (*realize)(GtkWidget *widget);
  void (*unrealize)(GtkWidget *widget);
  void (*size_allocate)(GtkWidget *widget, GtkAllocation *allocation);
  void (*state_changed)(GtkWidget *widget, GtkStateType previous_state);
  void (*state_flags_changed)(GtkWidget *widget,
                              GtkStateFlags previous_state_flags);
  void (*parent_set)(GtkWidget *widget, GtkWidget *previous_parent);
  void (*hierarchy_changed)(GtkWidget *widget, GtkWidget *previous_toplevel);
  void (*style_set)(GtkWidget *widget, GtkStyle *previous_style);
  void (*direction_changed)(GtkWidget *widget,
                            GtkTextDirection previous_direction);
  void (*grab_notify)(GtkWidget *widget, gboolean was_grabbed);
  void (*child_notify)(GtkWidget *widget, GParamSpec *child_property);
  gboolean (*draw)(GtkWidget *widget, cairo_t *cr);
  GtkSizeRequestMode (*get_request_mode)(GtkWidget *widget);
  void (*get_preferred_height)(GtkWidget *widget, gint *minimum_height,
                               gint *natural_height);
  void (*get_preferred_width_for_height)(GtkWidget *widget, gint height,
                                         gint *minimum_width,
                                         gint *natural_width);
  void (*get_preferred_width)(GtkWidget *widget, gint *minimum_width,
                              gint *natural_width);
  void (*get_preferred_height_for_width)(GtkWidget *widget, gint width,
                                         gint *minimum_height,
                                         gint *natural_height);
  gboolean (*mnemonic_activate)(GtkWidget *widget, gboolean group_cycling);
  void (*grab_focus)(GtkWidget *widget);
  gboolean (*focus)(GtkWidget *widget, GtkDirectionType direction);
  void (*move_focus)(GtkWidget *widget, GtkDirectionType direction);
  gboolean (*keynav_failed)(GtkWidget *widget, GtkDirectionType direction);
  gboolean (*event)(GtkWidget *widget, GdkEvent *event);
  gboolean (*button_press_event)(GtkWidget *widget, GdkEventButton *event);
  gboolean (*button_release_event)(GtkWidget *widget, GdkEventButton *event);
  gboolean (*scroll_event)(GtkWidget *widget, GdkEventScroll *event);
  gboolean (*motion_notify_event)(GtkWidget *widget, GdkEventMotion *event);
  gboolean (*delete_event)(GtkWidget *widget, GdkEventAny *event);
  gboolean (*destroy_event)(GtkWidget *widget, GdkEventAny *event);
  gboolean (*key_press_event)(GtkWidget *widget, GdkEventKey *event);
  gboolean (*key_release_event)(GtkWidget *widget, GdkEventKey *event);
  gboolean (*enter_notify_event)(GtkWidget *widget, GdkEventCrossing *event);
  gboolean (*leave_notify_event)(GtkWidget *widget, GdkEventCrossing *event);
  gboolean (*configure_event)(GtkWidget *widget, GdkEventConfigure *event);
  gboolean (*focus_in_event)(GtkWidget *widget, GdkEventFocus *event);
  gboolean (*focus_out_event)(GtkWidget *widget, GdkEventFocus *event);
  gboolean (*map_event)(GtkWidget *widget, GdkEventAny *event);
  gboolean (*unmap_event)(GtkWidget *widget, GdkEventAny *event);
  gboolean (*property_notify_event)(GtkWidget *widget, GdkEventProperty *event);
  gboolean (*selection_clear_event)(GtkWidget *widget,
                                    GdkEventSelection *event);
  gboolean (*selection_request_event)(GtkWidget *widget,
                                      GdkEventSelection *event);
  gboolean (*selection_notify_event)(GtkWidget *widget,
                                     GdkEventSelection *event);
  gboolean (*proximity_in_event)(GtkWidget *widget, GdkEventProximity *event);
  gboolean (*proximity_out_event)(GtkWidget *widget, GdkEventProximity *event);
  gboolean (*visibility_notify_event)(GtkWidget *widget,
                                      GdkEventVisibility *event);
  gboolean (*window_state_event)(GtkWidget *widget, GdkEventWindowState *event);
  gboolean (*damage_event)(GtkWidget *widget, GdkEventExpose *event);
  gboolean (*grab_broken_event)(GtkWidget *widget, GdkEventGrabBroken *event);
  void (*selection_get)(GtkWidget *widget, GtkSelectionData *selection_data,
                        guint info, guint time_);
  void (*selection_received)(GtkWidget *widget,
                             GtkSelectionData *selection_data, guint time_);
  void (*drag_begin)(GtkWidget *widget, GdkDragContext *context);
  void (*drag_end)(GtkWidget *widget, GdkDragContext *context);
  void (*drag_data_get)(GtkWidget *widget, GdkDragContext *context,
                        GtkSelectionData *selection_data, guint info,
                        guint time_);
  void (*drag_data_delete)(GtkWidget *widget, GdkDragContext *context);
  void (*drag_leave)(GtkWidget *widget, GdkDragContext *context, guint time_);
  gboolean (*drag_motion)(GtkWidget *widget, GdkDragContext *context, gint x,
                          gint y, guint time_);
  gboolean (*drag_drop)(GtkWidget *widget, GdkDragContext *context, gint x,
                        gint y, guint time_);
  void (*drag_data_received)(GtkWidget *widget, GdkDragContext *context, gint x,
                             gint y, GtkSelectionData *selection_data,
                             guint info, guint time_);
  gboolean (*drag_failed)(GtkWidget *widget, GdkDragContext *context,
                          GtkDragResult result);
  gboolean (*popup_menu)(GtkWidget *widget);
  gboolean (*show_help)(GtkWidget *widget, GtkWidgetHelpType help_type);
  AtkObject *(*get_accessible)(GtkWidget *widget);
  void (*screen_changed)(GtkWidget *widget, GdkScreen *previous_screen);
  gboolean (*can_activate_accel)(GtkWidget *widget, guint signal_id);
  void (*composited_changed)(GtkWidget *widget);
  gboolean (*query_tooltip)(GtkWidget *widget, gint x, gint y,
                            gboolean keyboard_tooltip, GtkTooltip *tooltip);
  void (*compute_expand)(GtkWidget *widget, gboolean *hexpand_p,
                         gboolean *vexpand_p);
  void (*adjust_size_request)(GtkWidget *widget, GtkOrientation orientation,
                              gint *minimum_size, gint *natural_size);
  void (*adjust_size_allocation)(GtkWidget *widget, GtkOrientation orientation,
                                 gint *minimum_size, gint *natural_size,
                                 gint *allocated_pos, gint *allocated_size);
  void (*style_updated)(GtkWidget *widget);
  gboolean (*touch_event)(GtkWidget *widget, GdkEventTouch *event);
  void (*get_preferred_height_and_baseline_for_width)(
      GtkWidget *widget, gint width, gint *minimum_height, gint *natural_height,
      gint *minimum_baseline, gint *natural_baseline);
  void (*adjust_baseline_request)(GtkWidget *widget, gint *minimum_baseline,
                                  gint *natural_baseline);
  void (*adjust_baseline_allocation)(GtkWidget *widget, gint *baseline);
  void (*queue_draw_region)(GtkWidget *widget, const cairo_region_t *region);
  GtkWidgetClassPrivate *priv;
  void (*_gtk_reserved6)();
  void (*_gtk_reserved7)();
};
void gtk_widget_class_bind_template_callback_full(GtkWidgetClass *widget_class,
                                                  const gchar *callback_name,
                                                  GCallback callback_symbol);
void gtk_widget_class_bind_template_child_full(GtkWidgetClass *widget_class,
                                               const gchar *name,
                                               gboolean internal_child,
                                               gssize struct_offset);
GParamSpec *gtk_widget_class_find_style_property(GtkWidgetClass *klass,
                                                 const gchar *property_name);
const char *gtk_widget_class_get_css_name(GtkWidgetClass *widget_class);
void gtk_widget_class_install_style_property(GtkWidgetClass *klass,
                                             GParamSpec *pspec);
void gtk_widget_class_install_style_property_parser(GtkWidgetClass *klass,
                                                    GParamSpec *pspec,
                                                    GtkRcPropertyParser parser);
gtk_widget_class_list_style_properties(GtkWidgetClass *klass,
                                       guint *n_properties);
void gtk_widget_class_set_accessible_role(GtkWidgetClass *widget_class,
                                          AtkRole role);
void gtk_widget_class_set_accessible_type(GtkWidgetClass *widget_class,
                                          GType type);
void gtk_widget_class_set_connect_func(GtkWidgetClass *widget_class,
                                       GtkBuilderConnectFunc connect_func,
                                       gpointer connect_data,
                                       GDestroyNotify connect_data_destroy);
void gtk_widget_class_set_css_name(GtkWidgetClass *widget_class,
                                   const char *name);
void gtk_widget_class_set_template(GtkWidgetClass *widget_class,
                                   GBytes *template_bytes);
void gtk_widget_class_set_template_from_resource(GtkWidgetClass *widget_class,
                                                 const gchar *resource_name);

typedef struct _GtkWidgetClassPrivate GtkWidgetClassPrivate;

enum GtkWidgetHelpType {
  GTK_WIDGET_HELP_TOOLTIP = 0,
  GTK_WIDGET_HELP_WHATS_THIS = 1
};

typedef struct _GtkWidgetPath GtkWidgetPath;
gint gtk_widget_path_append_for_widget(GtkWidgetPath *path, GtkWidget *widget);
gint gtk_widget_path_append_type(GtkWidgetPath *path, GType type);
gint gtk_widget_path_append_with_siblings(GtkWidgetPath *path,
                                          GtkWidgetPath *siblings,
                                          guint sibling_index);
GtkWidgetPath *gtk_widget_path_copy(const GtkWidgetPath *path);
void gtk_widget_path_free(GtkWidgetPath *path);
GType gtk_widget_path_get_object_type(const GtkWidgetPath *path);
gboolean gtk_widget_path_has_parent(const GtkWidgetPath *path, GType type);
gboolean gtk_widget_path_is_type(const GtkWidgetPath *path, GType type);
void gtk_widget_path_iter_add_class(GtkWidgetPath *path, gint pos,
                                    const gchar *name);
void gtk_widget_path_iter_add_region(GtkWidgetPath *path, gint pos,
                                     const gchar *name, GtkRegionFlags flags);
void gtk_widget_path_iter_clear_classes(GtkWidgetPath *path, gint pos);
void gtk_widget_path_iter_clear_regions(GtkWidgetPath *path, gint pos);
const gchar *gtk_widget_path_iter_get_name(const GtkWidgetPath *path, gint pos);
const char *gtk_widget_path_iter_get_object_name(const GtkWidgetPath *path,
                                                 gint pos);
GType gtk_widget_path_iter_get_object_type(const GtkWidgetPath *path, gint pos);
guint gtk_widget_path_iter_get_sibling_index(const GtkWidgetPath *path,
                                             gint pos);
const GtkWidgetPath *
gtk_widget_path_iter_get_siblings(const GtkWidgetPath *path, gint pos);
GtkStateFlags gtk_widget_path_iter_get_state(const GtkWidgetPath *path,
                                             gint pos);
gboolean gtk_widget_path_iter_has_class(const GtkWidgetPath *path, gint pos,
                                        const gchar *name);
gboolean gtk_widget_path_iter_has_name(const GtkWidgetPath *path, gint pos,
                                       const gchar *name);
gboolean gtk_widget_path_iter_has_qclass(const GtkWidgetPath *path, gint pos,
                                         GQuark qname);
gboolean gtk_widget_path_iter_has_qname(const GtkWidgetPath *path, gint pos,
                                        GQuark qname);
gboolean gtk_widget_path_iter_has_qregion(const GtkWidgetPath *path, gint pos,
                                          GQuark qname, GtkRegionFlags *flags);
gboolean gtk_widget_path_iter_has_region(const GtkWidgetPath *path, gint pos,
                                         const gchar *name,
                                         GtkRegionFlags *flags);
GSList *gtk_widget_path_iter_list_classes(const GtkWidgetPath *path, gint pos);
GSList *gtk_widget_path_iter_list_regions(const GtkWidgetPath *path, gint pos);
void gtk_widget_path_iter_remove_class(GtkWidgetPath *path, gint pos,
                                       const gchar *name);
void gtk_widget_path_iter_remove_region(GtkWidgetPath *path, gint pos,
                                        const gchar *name);
void gtk_widget_path_iter_set_name(GtkWidgetPath *path, gint pos,
                                   const gchar *name);
void gtk_widget_path_iter_set_object_name(GtkWidgetPath *path, gint pos,
                                          const char *name);
void gtk_widget_path_iter_set_object_type(GtkWidgetPath *path, gint pos,
                                          GType type);
void gtk_widget_path_iter_set_state(GtkWidgetPath *path, gint pos,
                                    GtkStateFlags state);
gint gtk_widget_path_length(const GtkWidgetPath *path);
void gtk_widget_path_prepend_type(GtkWidgetPath *path, GType type);
GtkWidgetPath *gtk_widget_path_ref(GtkWidgetPath *path);
char *gtk_widget_path_to_string(const GtkWidgetPath *path);
void gtk_widget_path_unref(GtkWidgetPath *path);

typedef struct _GtkWidgetPrivate GtkWidgetPrivate;

typedef struct _GtkWindowAccessibleClass GtkWindowAccessibleClass;

struct _GtkWindowAccessibleClass {
  GtkContainerAccessibleClass parent_class;
};

typedef struct _GtkWindowAccessiblePrivate GtkWindowAccessiblePrivate;

typedef struct _GtkWindowClass GtkWindowClass;

struct _GtkWindowClass {
  GtkBinClass parent_class;
  void (*set_focus)(GtkWindow *window, GtkWidget *focus);
  void (*activate_focus)(GtkWindow *window);
  void (*activate_default)(GtkWindow *window);
  void (*keys_changed)(GtkWindow *window);
  gboolean (*enable_debugging)(GtkWindow *window, gboolean toggle);
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
};

typedef struct _GtkWindowGeometryInfo GtkWindowGeometryInfo;

typedef struct _GtkWindowGroupClass GtkWindowGroupClass;

struct _GtkWindowGroupClass {
  GObjectClass parent_class;
  void (*_gtk_reserved1)();
  void (*_gtk_reserved2)();
  void (*_gtk_reserved3)();
  void (*_gtk_reserved4)();
};

typedef struct _GtkWindowGroupPrivate GtkWindowGroupPrivate;

enum GtkWindowPosition {
  GTK_WIN_POS_NONE = 0,
  GTK_WIN_POS_CENTER = 1,
  GTK_WIN_POS_MOUSE = 2,
  GTK_WIN_POS_CENTER_ALWAYS = 3,
  GTK_WIN_POS_CENTER_ON_PARENT = 4
};

typedef struct _GtkWindowPrivate GtkWindowPrivate;

enum GtkWindowType { GTK_WINDOW_TOPLEVEL = 0, GTK_WINDOW_POPUP = 1 };

enum GtkWrapMode {
  GTK_WRAP_NONE = 0,
  GTK_WRAP_CHAR = 1,
  GTK_WRAP_WORD = 2,
  GTK_WRAP_WORD_CHAR = 3
};
gboolean gtk_accel_groups_activate(GObject *object, guint accel_key,
                                   GdkModifierType accel_mods);
GSList *gtk_accel_groups_from_object(GObject *object);
GdkModifierType gtk_accelerator_get_default_mod_mask(

);
gchar *gtk_accelerator_get_label(guint accelerator_key,
                                 GdkModifierType accelerator_mods);
gchar *gtk_accelerator_get_label_with_keycode(GdkDisplay *display,
                                              guint accelerator_key,
                                              guint keycode,
                                              GdkModifierType accelerator_mods);
gchar *gtk_accelerator_name(guint accelerator_key,
                            GdkModifierType accelerator_mods);
gchar *gtk_accelerator_name_with_keycode(GdkDisplay *display,
                                         guint accelerator_key, guint keycode,
                                         GdkModifierType accelerator_mods);
void gtk_accelerator_parse(const gchar *accelerator, guint *accelerator_key,
                           GdkModifierType *accelerator_mods);
void gtk_accelerator_parse_with_keycode(const gchar *accelerator,
                                        guint *accelerator_key,
                                        accelerator_codes,
                                        GdkModifierType *accelerator_mods);
void gtk_accelerator_set_default_mod_mask(GdkModifierType default_mod_mask);
gboolean gtk_accelerator_valid(guint keyval, GdkModifierType modifiers);
gboolean gtk_alternative_dialog_button_order(GdkScreen *screen);
GTokenType gtk_binding_entry_add_signal_from_string(GtkBindingSet *binding_set,
                                                    const gchar *signal_desc);
void gtk_binding_entry_add_signall(GtkBindingSet *binding_set, guint keyval,
                                   GdkModifierType modifiers,
                                   const gchar *signal_name,
                                   GSList *binding_args);
void gtk_binding_entry_remove(GtkBindingSet *binding_set, guint keyval,
                              GdkModifierType modifiers);
void gtk_binding_entry_skip(GtkBindingSet *binding_set, guint keyval,
                            GdkModifierType modifiers);
GtkBindingSet *gtk_binding_set_by_class(gpointer object_class);
GtkBindingSet *gtk_binding_set_find(const gchar *set_name);
GtkBindingSet *gtk_binding_set_new(const gchar *set_name);
gboolean gtk_bindings_activate(GObject *object, guint keyval,
                               GdkModifierType modifiers);
gboolean gtk_bindings_activate_event(GObject *object, GdkEventKey *event);
GQuark gtk_builder_error_quark(

);
gboolean gtk_cairo_should_draw_window(cairo_t *cr, GdkWindow *window);
void gtk_cairo_transform_to_window(cairo_t *cr, GtkWidget *widget,
                                   GdkWindow *window);
const gchar *gtk_check_version(guint required_major, guint required_minor,
                               guint required_micro);
GQuark gtk_css_provider_error_quark(

);
void gtk_device_grab_add(GtkWidget *widget, GdkDevice *device,
                         gboolean block_others);
void gtk_device_grab_remove(GtkWidget *widget, GdkDevice *device);
void gtk_disable_setlocale(

);
gint gtk_distribute_natural_allocation(gint extra_space,
                                       guint n_requested_sizes,
                                       GtkRequestedSize *sizes);
void gtk_drag_cancel(GdkDragContext *context);
void gtk_drag_finish(GdkDragContext *context, gboolean success, gboolean del,
                     guint32 time_);
GtkWidget *gtk_drag_get_source_widget(GdkDragContext *context);
void gtk_drag_set_icon_default(GdkDragContext *context);
void gtk_drag_set_icon_gicon(GdkDragContext *context, GIcon *icon, gint hot_x,
                             gint hot_y);
void gtk_drag_set_icon_name(GdkDragContext *context, const gchar *icon_name,
                            gint hot_x, gint hot_y);
void gtk_drag_set_icon_pixbuf(GdkDragContext *context, GdkPixbuf *pixbuf,
                              gint hot_x, gint hot_y);
void gtk_drag_set_icon_stock(GdkDragContext *context, const gchar *stock_id,
                             gint hot_x, gint hot_y);
void gtk_drag_set_icon_surface(GdkDragContext *context,
                               cairo_surface_t *surface);
void gtk_drag_set_icon_widget(GdkDragContext *context, GtkWidget *widget,
                              gint hot_x, gint hot_y);
void gtk_draw_insertion_cursor(GtkWidget *widget, cairo_t *cr,
                               const GdkRectangle *location,
                               gboolean is_primary, GtkTextDirection direction,
                               gboolean draw_arrow);
gboolean gtk_events_pending(

);
gboolean gtk_false(

);
GQuark gtk_file_chooser_error_quark(

);
guint gtk_get_binary_age(

);
GdkEvent *gtk_get_current_event(

);
GdkDevice *gtk_get_current_event_device(

);
gboolean gtk_get_current_event_state(GdkModifierType *state);
guint32 gtk_get_current_event_time(

);
guint gtk_get_debug_flags(

);
PangoLanguage *gtk_get_default_language(

);
GtkWidget *gtk_get_event_widget(GdkEvent *event);
guint gtk_get_interface_age(

);
GtkTextDirection gtk_get_locale_direction(

);
guint gtk_get_major_version(

);
guint gtk_get_micro_version(

);
guint gtk_get_minor_version(

);
GOptionGroup *gtk_get_option_group(gboolean open_default_display);
GtkWidget *gtk_grab_get_current(

);
GtkIconSize gtk_icon_size_from_name(const gchar *name);
const gchar *gtk_icon_size_get_name(GtkIconSize size);
gboolean gtk_icon_size_lookup(GtkIconSize size, gint *width, gint *height);
gboolean gtk_icon_size_lookup_for_settings(GtkSettings *settings,
                                           GtkIconSize size, gint *width,
                                           gint *height);
GtkIconSize gtk_icon_size_register(const gchar *name, gint width, gint height);
void gtk_icon_size_register_alias(const gchar *alias, GtkIconSize target);
GQuark gtk_icon_theme_error_quark(

);
void gtk_init(int *argc, argv);
gboolean gtk_init_check(int *argc, argv);
gboolean gtk_init_with_args(gint *argc, argv, const gchar *parameter_string,
                            entries, const gchar *translation_domain);
guint gtk_key_snooper_install(GtkKeySnoopFunc snooper, gpointer func_data);
void gtk_key_snooper_remove(guint snooper_handler_id);
void gtk_main(

);
void gtk_main_do_event(GdkEvent *event);
gboolean gtk_main_iteration(

);
gboolean gtk_main_iteration_do(gboolean blocking);
guint gtk_main_level(

);
void gtk_main_quit(

);
void gtk_paint_arrow(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                     GtkShadowType shadow_type, GtkWidget *widget,
                     const gchar *detail, GtkArrowType arrow_type,
                     gboolean fill, gint x, gint y, gint width, gint height);
void gtk_paint_box(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                   GtkShadowType shadow_type, GtkWidget *widget,
                   const gchar *detail, gint x, gint y, gint width,
                   gint height);
void gtk_paint_box_gap(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                       GtkShadowType shadow_type, GtkWidget *widget,
                       const gchar *detail, gint x, gint y, gint width,
                       gint height, GtkPositionType gap_side, gint gap_x,
                       gint gap_width);
void gtk_paint_check(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                     GtkShadowType shadow_type, GtkWidget *widget,
                     const gchar *detail, gint x, gint y, gint width,
                     gint height);
void gtk_paint_diamond(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                       GtkShadowType shadow_type, GtkWidget *widget,
                       const gchar *detail, gint x, gint y, gint width,
                       gint height);
void gtk_paint_expander(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                        GtkWidget *widget, const gchar *detail, gint x, gint y,
                        GtkExpanderStyle expander_style);
void gtk_paint_extension(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                         GtkShadowType shadow_type, GtkWidget *widget,
                         const gchar *detail, gint x, gint y, gint width,
                         gint height, GtkPositionType gap_side);
void gtk_paint_flat_box(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                        GtkShadowType shadow_type, GtkWidget *widget,
                        const gchar *detail, gint x, gint y, gint width,
                        gint height);
void gtk_paint_focus(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                     GtkWidget *widget, const gchar *detail, gint x, gint y,
                     gint width, gint height);
void gtk_paint_handle(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                      GtkShadowType shadow_type, GtkWidget *widget,
                      const gchar *detail, gint x, gint y, gint width,
                      gint height, GtkOrientation orientation);
void gtk_paint_hline(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                     GtkWidget *widget, const gchar *detail, gint x1, gint x2,
                     gint y);
void gtk_paint_layout(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                      gboolean use_text, GtkWidget *widget, const gchar *detail,
                      gint x, gint y, PangoLayout *layout);
void gtk_paint_option(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                      GtkShadowType shadow_type, GtkWidget *widget,
                      const gchar *detail, gint x, gint y, gint width,
                      gint height);
void gtk_paint_resize_grip(GtkStyle *style, cairo_t *cr,
                           GtkStateType state_type, GtkWidget *widget,
                           const gchar *detail, GdkWindowEdge edge, gint x,
                           gint y, gint width, gint height);
void gtk_paint_shadow(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                      GtkShadowType shadow_type, GtkWidget *widget,
                      const gchar *detail, gint x, gint y, gint width,
                      gint height);
void gtk_paint_shadow_gap(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                          GtkShadowType shadow_type, GtkWidget *widget,
                          const gchar *detail, gint x, gint y, gint width,
                          gint height, GtkPositionType gap_side, gint gap_x,
                          gint gap_width);
void gtk_paint_slider(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                      GtkShadowType shadow_type, GtkWidget *widget,
                      const gchar *detail, gint x, gint y, gint width,
                      gint height, GtkOrientation orientation);
void gtk_paint_spinner(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                       GtkWidget *widget, const gchar *detail, guint step,
                       gint x, gint y, gint width, gint height);
void gtk_paint_tab(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                   GtkShadowType shadow_type, GtkWidget *widget,
                   const gchar *detail, gint x, gint y, gint width,
                   gint height);
void gtk_paint_vline(GtkStyle *style, cairo_t *cr, GtkStateType state_type,
                     GtkWidget *widget, const gchar *detail, gint y1_, gint y2_,
                     gint x);
const gchar *gtk_paper_size_get_default(

);
GList *gtk_paper_size_get_paper_sizes(gboolean include_custom);
gboolean gtk_parse_args(int *argc, argv);
GQuark gtk_print_error_quark(

);
GtkPageSetup *gtk_print_run_page_setup_dialog(GtkWindow *parent,
                                              GtkPageSetup *page_setup,
                                              GtkPrintSettings *settings);
void gtk_print_run_page_setup_dialog_async(GtkWindow *parent,
                                           GtkPageSetup *page_setup,
                                           GtkPrintSettings *settings,
                                           GtkPageSetupDoneFunc done_cb,
                                           gpointer data);
void gtk_propagate_event(GtkWidget *widget, GdkEvent *event);
void gtk_rc_add_default_file(const gchar *filename);
gchar *gtk_rc_find_module_in_path(const gchar *module_file);
gchar *gtk_rc_find_pixmap_in_path(GtkSettings *settings, GScanner *scanner,
                                  const gchar *pixmap_file);
gtk_rc_get_default_files(

);
gchar *gtk_rc_get_im_module_file(

);
gchar *gtk_rc_get_im_module_path(

);
gchar *gtk_rc_get_module_dir(

);
GtkStyle *gtk_rc_get_style(GtkWidget *widget);
GtkStyle *gtk_rc_get_style_by_paths(GtkSettings *settings,
                                    const char *widget_path,
                                    const char *class_path, GType type);
gchar *gtk_rc_get_theme_dir(

);
void gtk_rc_parse(const gchar *filename);
guint gtk_rc_parse_color(GScanner *scanner, GdkColor *color);
guint gtk_rc_parse_color_full(GScanner *scanner, GtkRcStyle *style,
                              GdkColor *color);
guint gtk_rc_parse_priority(GScanner *scanner, GtkPathPriorityType *priority);
guint gtk_rc_parse_state(GScanner *scanner, GtkStateType *state);
void gtk_rc_parse_string(const gchar *rc_string);
gboolean gtk_rc_property_parse_border(const GParamSpec *pspec,
                                      const GString *gstring,
                                      GValue *property_value);
gboolean gtk_rc_property_parse_color(const GParamSpec *pspec,
                                     const GString *gstring,
                                     GValue *property_value);
gboolean gtk_rc_property_parse_enum(const GParamSpec *pspec,
                                    const GString *gstring,
                                    GValue *property_value);
gboolean gtk_rc_property_parse_flags(const GParamSpec *pspec,
                                     const GString *gstring,
                                     GValue *property_value);
gboolean gtk_rc_property_parse_requisition(const GParamSpec *pspec,
                                           const GString *gstring,
                                           GValue *property_value);
gboolean gtk_rc_reparse_all(

);
gboolean gtk_rc_reparse_all_for_settings(GtkSettings *settings,
                                         gboolean force_load);
void gtk_rc_reset_styles(GtkSettings *settings);
GScanner *gtk_rc_scanner_new(

);
void gtk_rc_set_default_files(filenames);
GQuark gtk_recent_chooser_error_quark(

);
GQuark gtk_recent_manager_error_quark(

);
void gtk_render_activity(GtkStyleContext *context, cairo_t *cr, gdouble x,
                         gdouble y, gdouble width, gdouble height);
void gtk_render_arrow(GtkStyleContext *context, cairo_t *cr, gdouble angle,
                      gdouble x, gdouble y, gdouble size);
void gtk_render_background(GtkStyleContext *context, cairo_t *cr, gdouble x,
                           gdouble y, gdouble width, gdouble height);
void gtk_render_background_get_clip(GtkStyleContext *context, gdouble x,
                                    gdouble y, gdouble width, gdouble height,
                                    GdkRectangle *out_clip);
void gtk_render_check(GtkStyleContext *context, cairo_t *cr, gdouble x,
                      gdouble y, gdouble width, gdouble height);
void gtk_render_expander(GtkStyleContext *context, cairo_t *cr, gdouble x,
                         gdouble y, gdouble width, gdouble height);
void gtk_render_extension(GtkStyleContext *context, cairo_t *cr, gdouble x,
                          gdouble y, gdouble width, gdouble height,
                          GtkPositionType gap_side);
void gtk_render_focus(GtkStyleContext *context, cairo_t *cr, gdouble x,
                      gdouble y, gdouble width, gdouble height);
void gtk_render_frame(GtkStyleContext *context, cairo_t *cr, gdouble x,
                      gdouble y, gdouble width, gdouble height);
void gtk_render_frame_gap(GtkStyleContext *context, cairo_t *cr, gdouble x,
                          gdouble y, gdouble width, gdouble height,
                          GtkPositionType gap_side, gdouble xy0_gap,
                          gdouble xy1_gap);
void gtk_render_handle(GtkStyleContext *context, cairo_t *cr, gdouble x,
                       gdouble y, gdouble width, gdouble height);
void gtk_render_icon(GtkStyleContext *context, cairo_t *cr, GdkPixbuf *pixbuf,
                     gdouble x, gdouble y);
GdkPixbuf *gtk_render_icon_pixbuf(GtkStyleContext *context,
                                  const GtkIconSource *source,
                                  GtkIconSize size);
void gtk_render_icon_surface(GtkStyleContext *context, cairo_t *cr,
                             cairo_surface_t *surface, gdouble x, gdouble y);
void gtk_render_insertion_cursor(GtkStyleContext *context, cairo_t *cr,
                                 gdouble x, gdouble y, PangoLayout *layout,
                                 int index, PangoDirection direction);
void gtk_render_layout(GtkStyleContext *context, cairo_t *cr, gdouble x,
                       gdouble y, PangoLayout *layout);
void gtk_render_line(GtkStyleContext *context, cairo_t *cr, gdouble x0,
                     gdouble y0, gdouble x1, gdouble y1);
void gtk_render_option(GtkStyleContext *context, cairo_t *cr, gdouble x,
                       gdouble y, gdouble width, gdouble height);
void gtk_render_slider(GtkStyleContext *context, cairo_t *cr, gdouble x,
                       gdouble y, gdouble width, gdouble height,
                       GtkOrientation orientation);
void gtk_rgb_to_hsv(gdouble r, gdouble g, gdouble b, gdouble *h, gdouble *s,
                    gdouble *v);
void gtk_selection_add_target(GtkWidget *widget, GdkAtom selection,
                              GdkAtom target, guint info);
void gtk_selection_add_targets(GtkWidget *widget, GdkAtom selection, targets,
                               guint ntargets);
void gtk_selection_clear_targets(GtkWidget *widget, GdkAtom selection);
gboolean gtk_selection_convert(GtkWidget *widget, GdkAtom selection,
                               GdkAtom target, guint32 time_);
gboolean gtk_selection_owner_set(GtkWidget *widget, GdkAtom selection,
                                 guint32 time_);
gboolean gtk_selection_owner_set_for_display(GdkDisplay *display,
                                             GtkWidget *widget,
                                             GdkAtom selection, guint32 time_);
void gtk_selection_remove_all(GtkWidget *widget);
void gtk_set_debug_flags(guint flags);
void gtk_show_about_dialog(GtkWindow *parent, const gchar *first_property_name,
                           ...);
gboolean gtk_show_uri(GdkScreen *screen, const gchar *uri, guint32 timestamp);
gboolean gtk_show_uri_on_window(GtkWindow *parent, const char *uri,
                                guint32 timestamp);
void gtk_stock_add(items, guint n_items);
void gtk_stock_add_static(items, guint n_items);
GSList *gtk_stock_list_ids(

);
gboolean gtk_stock_lookup(const gchar *stock_id, GtkStockItem *item);
void gtk_stock_set_translate_func(const gchar *domain, GtkTranslateFunc func,
                                  gpointer data, GDestroyNotify notify);
void gtk_target_table_free(targets, gint n_targets);
gtk_target_table_new_from_list(GtkTargetList *list, gint *n_targets);
gboolean gtk_targets_include_image(targets, gint n_targets, gboolean writable);
gboolean gtk_targets_include_rich_text(targets, gint n_targets,
                                       GtkTextBuffer *buffer);
gboolean gtk_targets_include_text(targets, gint n_targets);
gboolean gtk_targets_include_uri(targets, gint n_targets);
GtkWidget *gtk_test_create_simple_window(const gchar *window_title,
                                         const gchar *dialog_text);
GtkWidget *gtk_test_create_widget(GType widget_type,
                                  const gchar *first_property_name, ...);
GtkWidget *gtk_test_display_button_window(const gchar *window_title,
                                          const gchar *dialog_text, ...);
GtkWidget *gtk_test_find_label(GtkWidget *widget, const gchar *label_pattern);
GtkWidget *gtk_test_find_sibling(GtkWidget *base_widget, GType widget_type);
GtkWidget *gtk_test_find_widget(GtkWidget *widget, const gchar *label_pattern,
                                GType widget_type);
void gtk_test_init(int *argcp, argvp, ...);
gtk_test_list_all_types(guint *n_types);
void gtk_test_register_all_types(

);
double gtk_test_slider_get_value(GtkWidget *widget);
void gtk_test_slider_set_perc(GtkWidget *widget, double percentage);
gboolean gtk_test_spin_button_click(GtkSpinButton *spinner, guint button,
                                    gboolean upwards);
gchar *gtk_test_text_get(GtkWidget *widget);
void gtk_test_text_set(GtkWidget *widget, const gchar *string);
gboolean gtk_test_widget_click(GtkWidget *widget, guint button,
                               GdkModifierType modifiers);
gboolean gtk_test_widget_send_key(GtkWidget *widget, guint keyval,
                                  GdkModifierType modifiers);
void gtk_test_widget_wait_for_draw(GtkWidget *widget);
gboolean gtk_tree_get_row_drag_data(GtkSelectionData *selection_data,
                                    GtkTreeModel **tree_model,
                                    GtkTreePath **path);
void gtk_tree_row_reference_deleted(GObject *proxy, GtkTreePath *path);
void gtk_tree_row_reference_inserted(GObject *proxy, GtkTreePath *path);
void gtk_tree_row_reference_reordered(GObject *proxy, GtkTreePath *path,
                                      GtkTreeIter *iter, new_order);
gboolean gtk_tree_set_row_drag_data(GtkSelectionData *selection_data,
                                    GtkTreeModel *tree_model,
                                    GtkTreePath *path);
gboolean gtk_true(

);
