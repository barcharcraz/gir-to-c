<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform"
            xmlns:gir="http://www.gtk.org/introspection/core/1.0"
            xmlns:c="http://www.gtk.org/introspection/c/1.0"
            xmlns:glib="http://www.gtk.org/introspection/glib/1.0">
    <output method="text" />

    <key name="gtype-struct-for" match="gir:record"
         use="@glib:is-gtype-struct-for"/>
    <key name="class-name" match="gir:class"
         use="@name" />
    <key name="parent-name" match="gir:class"
         use="@parent"/>
    <!-- Generate structs -->
    <template
            match="gir:record|gir:class|gir:interface[@c:type]"
            mode="forward-declare">
        typedef struct _<value-of select="@c:type"/><text> </text>
        <value-of select="@c:type"/>;
    </template>

    <template name="c-struct-declare">
        <!--
        we don't want to generate a real struct if the record
        only has methods
         -->
        <if test="gir:field">
            struct _<value-of select="@c:type"/> {
            <apply-templates select="gir:field" />
            };
        </if>
        <apply-templates select="gir:method" />
    </template>

    <template name="c-class-declare">
        <apply-templates select="key('gtype-struct-for', @glib:type-struct)"
                         mode="parent-recurse"/>
        <call-template name="c-struct-declare"/>

    </template>

    <!--
    This kicks off class struct definition. It matches stuff that doesn't
    have a parent class in this file (so it'll match stuff with the parent "Gobject.GObject
    when using a gir file that isn't describing gobject itself
    -->
    <template match="gir:class[not(key('class-name', @parent))]">
        <call-template name="c-class-declare"/>
        <apply-templates select="key('parent-name', @name)"
                         mode="parent-recurse" />
    </template>
    <!--
    The mode parent-recurse matches all classes but only in
    mode parent recurse. it's designed to generate classes in order
    of inheritance.
    -->
    <template match="gir:class" mode="parent-recurse">
        <call-template name="c-class-declare"/>
        <apply-templates select="key('parent-name', @name)"
                         mode="parent-recurse"/>
    </template>

    <template match="gir:record" mode="parent-recurse">
        <call-template name="c-struct-declare"/>
    </template>
    <!--
    generate all the non-class record types (plain old structs)
    -->
    <template match="gir:record[not(@glib:is-gtype-struct-for)]">
        <call-template name="c-struct-declare"/>
    </template>
    <template match="gir:union">
        typedef union _<value-of select="@c:type"/><text> </text>
        <value-of select="@c:type"/>;
        union _<value-of select="@c:type"/> {
        <apply-templates select="gir:field"/>
        };
    </template>
    <!--
    Generate fields for structs and unions
    -->
    <template match="gir:field">
        <value-of select="gir:type/@c:type"/><text> </text>
        <value-of select="@name" />;
    </template>
    <template match="gir:field[gir:array]">
        <value-of select="gir:array/gir:type/@c:type"/><text> </text>
        <value-of select="@name"/>[<value-of select="gir:array/@fixed-size"/>];
    </template>
    <template match="gir:field[gir:callback]">
        <apply-templates select="gir:callback"/>
    </template>


</stylesheet>