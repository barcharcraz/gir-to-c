<?xml version="1.0" encoding="UTF-8"?>
<stylesheet version="1.0"
            xmlns:gir="http://www.gtk.org/introspection/core/1.0"
            xmlns="http://www.w3.org/1999/XSL/Transform"
            xmlns:c="http://www.gtk.org/introspection/c/1.0"
            xmlns:glib="http://www.gtk.org/introspection/glib/1.0"
            xmlns:xd="http://www.pnp-software.com/XSLTdoc">
    
    <import href="c-types.xsl" />
    <include href="c-record.xsl" />
    <output method="text"
            encoding="utf-8"
            indent="yes"/>
    
    <template match="gir:include">
        <variable name="include_file">
            <value-of select="document(concat(@name, '-', @version, '.gir'))//c:include/@name" />
        </variable>
        <if test="$include_file!=''">
            #include &lt;<value-of select="$include_file"/>&gt;
        </if>
    </template>

    <template match="gir:namespace">
        <apply-templates mode="forward-declare" />
        <apply-templates mode="typedefs"/>
        <apply-templates />
    </template>
    <template match = "gir:repository">
  		<apply-templates/>
    </template>
    <template match="gir:alias">
    	typedef <value-of select="gir:type/@c:type" /><text> </text>
    		<value-of select="@c:type" />;
    </template>

    <!-- Generate C constants -->
    <template match="gir:constant">
    	const <value-of select="gir:type/@c:type" /><text> </text>
    		<value-of select="@c:type"/> = <value-of select="@value"/>;
    </template>
    <template match="gir:constant[gir:type/@c:type = 'gchar*']">
    	const <value-of select="gir:type/@c:type" /> <text> </text>
    		<value-of select="@c:type"/> = "<value-of select="@value" />";
    </template>

    <template match="gir:parameters">
        <for-each select="gir:parameter|gir:instance-parameter">
            <value-of select="gir:type/@c:type"/><text> </text>
            <value-of select="@name" />
            <if test="position() != last()">,</if>
        </for-each>
    </template>
    
    <!--
    callback nodes with the c:type are actually typedefs,
    honestly they really should be aliases but w/e
    -->
    <xd:doc>
        <xd:short>Generate typedefs for callbacks</xd:short>
    </xd:doc>
    <template match="gir:callback[@c:type]" mode="typedefs">
        <value-of select="gir:return-value/gir:type/@c:type"/>
        (*<value-of select="@c:type"/>)
        (<apply-templates select="gir:parameters"/>);
    </template>
    <xd:doc>
    	<xd:short>Generate callback types in non-typedef contexts</xd:short>
    	<xd:detail>This is used in parameter lists</xd:detail>
    </xd:doc>
    <template match="gir:callback">
        <value-of select="gir:return-value/gir:type/@c:type"/>
        (*<value-of select="@name"/>)
        (<apply-templates select="gir:parameters"/>);
    </template>



    <xd:doc>
    	<xd:short>Generate enums</xd:short>
    </xd:doc>
    <template match="gir:enumeration|gir:bitfield">
        enum <value-of select="@c:type"/> {
        <for-each select="gir:member">
            <value-of select="@c:identifier"/>
            <text> = </text>
            <value-of select="@value" />
            <if test="position() != last()">,</if>
        </for-each>
        };
    </template>

    <template match="gir:function|gir:method">
        <value-of select="gir:return-value/gir:type/@c:type" />
        <text> </text>
        <value-of select="@c:identifier" />
        (<apply-templates select="gir:parameters" />);
    </template>
    <strip-space elements="*"/>
    <template match="*">
        <message terminate="no">
            Error: Unmatched Template <value-of select="name()" />
             With value <value-of select="@name"/>
        </message>
    </template>
    <template match="*" mode="forward-declare"/>
    <template match="*" mode="typedefs"/>
    <template match="/">
        #pragma once
        <apply-imports/>
        <apply-templates />
    </template>
	<template match="text()|@*" />
</stylesheet>