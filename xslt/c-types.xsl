<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform">
    <output method="text"
            encoding="utf-8"
            indent="yes" />
    <template match="/">
        <text>
            typedef char   gchar;
            typedef short  gshort;
            typedef long   glong;
            typedef int    gint;
            typedef gint   gboolean;

            typedef unsigned char   guchar;
            typedef unsigned short  gushort;
            typedef unsigned long   gulong;
            typedef unsigned int    guint;

            typedef float   gfloat;
            typedef double  gdouble;

            typedef void* gpointer;

        </text>
    </template>

</stylesheet>
